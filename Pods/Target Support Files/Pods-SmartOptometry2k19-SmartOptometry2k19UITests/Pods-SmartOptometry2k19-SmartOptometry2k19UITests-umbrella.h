#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif


FOUNDATION_EXPORT double Pods_SmartOptometry2k19_SmartOptometry2k19UITestsVersionNumber;
FOUNDATION_EXPORT const unsigned char Pods_SmartOptometry2k19_SmartOptometry2k19UITestsVersionString[];

