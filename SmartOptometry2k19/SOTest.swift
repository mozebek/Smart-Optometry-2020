//
//  SOTest.swift
//  SmartOptometry2k19
//
//  Created by Matic Ozebek on 15/05/2019.
//  Copyright © 2019 Matic Ozebek. All rights reserved.
//

import UIKit

class SOTest: NSObject {
    
    var TestName: String = ""
    var TestColor: UIColor = UIColor.black
    var TestImageName: String = ""
    var TestVCName: String = ""
    var TestNumber: Int = Int()
    var TestLink: String = ""
    
    init(testName: String, testColor: UIColor, testImageName: String, testVCName: String, testNumber: Int, testLink: String) {
        self.TestName = testName
        self.TestColor = testColor
        self.TestImageName = testImageName
        self.TestVCName = testVCName
        self.TestNumber = testNumber
        self.TestLink = testLink
        super.init()
    }

}


