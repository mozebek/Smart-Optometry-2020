//
//  ProfileViewController.swift
//  SmartOptometry2k19
//
//  Created by Matic Ozebek on 07/11/2019.
//  Copyright © 2019 Matic Ozebek. All rights reserved.
//

import UIKit
import Alamofire

class ProfileViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var UpdateButton: UIButton!
    @IBOutlet weak var Name: UITextField!
    @IBOutlet weak var EmailAddress: UITextField!
    @IBOutlet weak var NameHolder: UIView!
    @IBOutlet weak var EmailHolder: UIView!
    @IBOutlet weak var CheckmarkPlaceholder: UIView!
    @IBOutlet weak var ErrorLabel: UILabel!
    @IBOutlet weak var termsLabel: UIButton!
    @IBOutlet weak var UpdateLabel: UILabel!
    
    var terms: Bool = true;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Helper.app.addGradientToView(view: UpdateButton,
                                     color1: UIColor.init(displayP3Red: 25/255, green: 154/255, blue: 143/255, alpha: 1),
                                     color2: UIColor.init(displayP3Red: 35/255, green: 188/255, blue: 121/255, alpha: 1))
    
        UpdateButton.layer.cornerRadius = 5
        UpdateButton.clipsToBounds = true;
        NameHolder.layer.cornerRadius = 5
        NameHolder.clipsToBounds = true;
        EmailHolder.layer.cornerRadius = 5
        EmailHolder.clipsToBounds = true;
        CheckmarkPlaceholder.layer.cornerRadius = 3
        CheckmarkPlaceholder.clipsToBounds = true;
        ErrorLabel.alpha = 0;

        self.Name.delegate = self
        self.EmailAddress.delegate = self
        // Do any additional setup after loading the view.
        
        UpdateButton.setTitle("update".localized, for: .normal)
        termsLabel.setTitle("terms_agree".localized, for: .normal)
        UpdateLabel.text = "profile_update".localized
        Name.placeholder = "name".localized
        EmailAddress.placeholder = "email".localized
        
        // Set orientation to portrait
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
    }
    
    override func viewWillLayoutSubviews() {
        Helper.app.addGradientToView(view: UpdateButton,
                                     color1: UIColor.init(displayP3Red: 25/255, green: 154/255, blue: 143/255, alpha: 1),
                                     color2: UIColor.init(displayP3Red: 35/255, green: 188/255, blue: 121/255, alpha: 1))
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    @IBAction func TermsButton(_ sender: Any) {
        if(terms){
            CheckmarkPlaceholder.backgroundColor = UIColor.lightGray
            terms = false
        }
        else{
            CheckmarkPlaceholder.backgroundColor = UIColor.init(red: 25/255, green: 154/255, blue: 143/255, alpha: 1)
            terms = true
        }
        
    }
    
    // Store user to sendy
    @IBAction func UpdateProfile(_ sender: Any) {
        
        let email: String = EmailAddress.text!
        let name: String = Name.text!
        var SendyStatus: String = ""
        
        let parameters: [String: String] = [
            "list": "PXr7j9IYFJn0anqDXcQ3vA",
            "name": name,
            "email": email,
            "gdpr": "true",
            "boolean" : "true",
        ]
        
        
        
        AF.request("https://sendy.smart-optometry.com/subscribe", method: .post, parameters: parameters).validate().responseString { response in
            
            
            switch response.result {
            case .success(let value):
                SendyStatus = value
            case .failure( _):
                print("No internet or smomething!")
                self.ErrorLabel.alpha = 1;
                self.ErrorLabel.text = "check_connection".localized
                break
            }
            
            if(self.terms == true){
                
            if(SendyStatus == "Already subscribed."){
                self.UpdateButton.isEnabled = false
                UserDefaults.standard.set(true, forKey: "Subscribed")
                self.dismiss(animated: true, completion: nil)
            }
            else if(SendyStatus == "1"){
                self.UpdateButton.isEnabled = false
                UserDefaults.standard.set(true, forKey: "Subscribed")
                self.dismiss(animated: true, completion: nil)
            }
            else if(SendyStatus == "Invalid email address."){
                self.ErrorLabel.alpha = 1;
                self.ErrorLabel.text = "invalid_email".localized
            }
            else if(SendyStatus == "Some fields are missing."){
                self.ErrorLabel.alpha = 1;
                self.ErrorLabel.text = "email_missing".localized
            }
            }
            else{
                self.ErrorLabel.alpha = 1;
                self.ErrorLabel.text = "you_must_aggree".localized
            }
        }
        
    }
    
    @IBAction func ShowTerms(_ sender: Any) {
        if let url = URL(string: "https://www.amblyoplay.com/tou/") {
            UIApplication.shared.open(url, options: [:])
        }
    }
    
    @IBAction func ExitProfileUpdate(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // Allow rotation
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var shouldAutorotate: Bool {
        return true
    }



}

