//
//  DesaturationRedViewController.swift
//  SmartOptometry2k19
//
//  Created by Matic Ozebek on 21/08/2019.
//  Copyright © 2019 Matic Ozebek. All rights reserved.
//

import UIKit

class DesaturationRedViewController: UIViewController {
    


    @IBOutlet weak var MiddleDot: UIButton!
    @IBOutlet weak var CircleButton1: UIButton!
    @IBOutlet weak var CircleButton2: UIButton!
    @IBOutlet weak var CircleButton3: UIButton!
    @IBOutlet weak var CircleButton4: UIButton!
    
    var timer: Timer = Timer()
    
    var redColor1: Int = 130
    var redColor2: Int = 130
    var redColor3: Int = 130
    var redColor4: Int = 130
    var cn = 1
    
    var c1state = true;
    var c2state = true;
    var c3state = true;
    var c4state = true;
    
    @IBOutlet weak var PercentageLabel1: UILabel!
    @IBOutlet weak var LTLabel1: UILabel!
    @IBOutlet weak var UpLabel1: UILabel!
    
    @IBOutlet weak var PercentageLabel2: UILabel!
    @IBOutlet weak var LTLabel2: UILabel!
    @IBOutlet weak var UpLabel2: UILabel!
    
    @IBOutlet weak var PercentageLabel3: UILabel!
    @IBOutlet weak var LTLabel3: UILabel!
    @IBOutlet weak var LoLabel1: UILabel!
    
    @IBOutlet weak var PercentageLabel4: UILabel!
    @IBOutlet weak var LTLabel4: UILabel!
    @IBOutlet weak var LoLabel2: UILabel!
    
    @IBOutlet weak var LeftEyeButton: UIButton!
    @IBOutlet weak var RightEyeButton: UIButton!
    
    // Brightness
    var br: CGFloat = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Save current brightnes
        br = UIScreen.main.brightness
        // Check for auto brightness
        if(Helper.app.CheckForBrightness()){
            UIScreen.main.brightness = CGFloat(1)
        }
        

        // Rotate screen to landscape
        let value = UIInterfaceOrientation.landscapeRight.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        
        // Hide labels
        UpLabel1.alpha = 0
        PercentageLabel1.alpha = 0
        LTLabel1.alpha = 0
        
        UpLabel2.alpha = 0
        PercentageLabel2.alpha = 0
        LTLabel2.alpha = 0
        
        LoLabel1.alpha = 0
        PercentageLabel3.alpha = 0
        LTLabel3.alpha = 0
        
        LoLabel2.alpha = 0
        PercentageLabel4.alpha = 0
        LTLabel4.alpha = 0
        
        // Hide left and right eye buttons
        LeftEyeButton.alpha = 0
        RightEyeButton.alpha = 0
        
        // make circles rounded
        MiddleDot.layer.cornerRadius = MiddleDot.frame.height/2
        CircleButton1.layer.cornerRadius = CircleButton1.frame.height/2
        CircleButton2.layer.cornerRadius = CircleButton2.frame.height/2
        CircleButton3.layer.cornerRadius = CircleButton3.frame.height/2
        CircleButton4.layer.cornerRadius = CircleButton4.frame.height/2
        
        ResetTest()
        
        // Set translates
        UpLabel1.text = "upper".localized
        UpLabel2.text = "upper".localized
        LoLabel1.text = "lower".localized
        LoLabel2.text = "lower".localized
        LeftEyeButton.setTitle("left_eye".localized, for: .normal)
        RightEyeButton.setTitle("right_eye".localized, for: .normal)

    }
    
    
    @objc func ColorChange(){
        
        // Change first circle
        if(cn == 1 ){
            
            if(c1state == false && redColor1 <= 255){
                redColor1 = redColor1+1
            }
            else if (c1state == true && redColor1 > 102){
                redColor1 = redColor1-1
            }
            
            if(redColor1>254){
                redColor1 = 255
            }
           CircleButton1.backgroundColor = UIColor.init(red: CGFloat(redColor1)/255, green: 0, blue: 0, alpha: 1)
        }
        
        // Change second circle
        if(cn == 2 ){
            
            if(c2state == false && redColor2 <= 255){
                redColor2 = redColor2+1
            }
            else if (c2state == true && redColor2 > 102){
                redColor2 = redColor2-1
            }
            
            if(redColor2>254){
                redColor2 = 255
            }
            CircleButton2.backgroundColor = UIColor.init(red: CGFloat(redColor2)/255, green: 0, blue: 0, alpha: 1)
        }
        
        // Change third circle
        if(cn == 3 ){
            
            if(c3state == false && redColor3 <= 255){
                redColor3 = redColor3+1
            }
            else if (c3state == true && redColor3 > 102){
                redColor3 = redColor3-1
            }
            
            if(redColor3>254){
                redColor3 = 255
            }
            CircleButton3.backgroundColor = UIColor.init(red: CGFloat(redColor3)/255, green: 0, blue: 0, alpha: 1)
        }
        
        // Change fourth circle
        if(cn == 4){
            
            if(c4state == false && redColor4 <= 255){
                redColor4 = redColor4+1
            }
            else if (c4state == true && redColor4 > 102){
                redColor4 = redColor4-1
            }
            
            if(redColor4>254){
                redColor4 = 255
            }
            CircleButton4.backgroundColor = UIColor.init(red: CGFloat(redColor4)/255, green: 0, blue: 0, alpha: 1)
        }
        

 
    }
    
    func ResetTest(){
        
        //Reset color values
        redColor1 = 130
        redColor2 = 130
        redColor3 = 130
        redColor4 = 130
        
        // Set color to the darkest
        CircleButton1.backgroundColor = UIColor.init(red: 130/255, green: 0, blue: 0, alpha: 1)
        CircleButton2.backgroundColor = UIColor.init(red: 130/255, green: 0, blue: 0, alpha: 1)
        CircleButton3.backgroundColor = UIColor.init(red: 130/255, green: 0, blue: 0, alpha: 1)
        CircleButton4.backgroundColor = UIColor.init(red: 130/255, green: 0, blue: 0, alpha: 1)
        
        
        UIView.animate(withDuration: 0.3, animations: {
            self.LeftEyeButton.alpha = 0
            self.RightEyeButton.alpha = 0
            
            self.UpLabel1.alpha = 0
            self.PercentageLabel1.alpha = 0
            self.LTLabel1.alpha = 0
            
            self.UpLabel2.alpha = 0
            self.PercentageLabel2.alpha = 0
            self.LTLabel2.alpha = 0
            
            self.LoLabel1.alpha = 0
            self.PercentageLabel3.alpha = 0
            self.LTLabel3.alpha = 0
            
            self.LoLabel2.alpha = 0
            self.PercentageLabel4.alpha = 0
            self.LTLabel4.alpha = 0
            
            self.LeftEyeButton.setTitleColor(UIColor.white, for: .normal)
            self.RightEyeButton.setTitleColor(UIColor.white, for: .normal)
            
        })
        
        // Reset states
        c1state = true;
        c2state = true;
        c3state = true;
        c4state = true;
        
        
        
    }
    
    
    // Allow rotation
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .landscapeRight
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    // Buttons actions
    @IBAction func Circle1Action(_ sender: Any) {
        cn=1
        timer.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(ColorChange), userInfo: nil, repeats: true)
        if(c1state == false){
            c1state = true
        }
        else {
            c1state = false;
        }
    }
    
    @IBAction func Circle2Action(_ sender: Any) {
        cn=2
        timer.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(ColorChange), userInfo: nil, repeats: true)
        if(c2state == false){
            c2state = true
        }
        else {
            c2state = false;
        }
    }
    
    @IBAction func Circle3Action(_ sender: Any) {
        cn=3
        timer.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(ColorChange), userInfo: nil, repeats: true)
        if(c3state == false){
            c3state = true
        }
        else {
            c3state = false;
        }
    }
    
    @IBAction func Circle4Action(_ sender: Any) {
        cn=4
        timer.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(ColorChange), userInfo: nil, repeats: true)
        if(c4state == false){
            c4state = true
        }
        else {
            c4state = false;
        }
    }
    
    
    @IBAction func LeftEyeAction(_ sender: Any) {
        
        LTLabel1.text = "temporal".localized
        LTLabel3.text = "temporal".localized
        LTLabel2.text = "nasal".localized
        LTLabel4.text = "nasal".localized
        
        LeftEyeButton.setTitleColor(UIColor.red, for: .normal)
        RightEyeButton.setTitleColor(UIColor.white, for: .normal)
        
        SetResults()
        
    }
    
    @IBAction func RightEyeAction(_ sender: Any) {
        LTLabel1.text = "nasal".localized
        LTLabel3.text = "nasal".localized
        LTLabel2.text = "temporal".localized
        LTLabel4.text = "temporal".localized
        
        LeftEyeButton.setTitleColor(UIColor.white, for: .normal)
        RightEyeButton.setTitleColor(UIColor.red, for: .normal)
        
        SetResults()
    }
    
    func SetResults(){
        
        let smallest = min(redColor1,redColor2,redColor3,redColor4)
        let calc = ((smallest-102)*100)/153
        
        // Set text percentage label
        PercentageLabel1.text  = String(abs((redColor1-102)*100)/153-calc) + "%"
        PercentageLabel2.text  = String(abs((redColor2-102)*100)/153-calc) + "%"
        PercentageLabel3.text  = String(abs((redColor3-102)*100)/153-calc) + "%"
        PercentageLabel4.text  = String(abs((redColor4-102)*100)/153-calc) + "%"
        
        // Present results that are not the smallest
        if(redColor1 != smallest){
            LTLabel1.alpha = 1
            UpLabel1.alpha = 1
            PercentageLabel1.alpha = 1
        }
        
        if(redColor2 != smallest){
            LTLabel2.alpha = 1
            UpLabel2.alpha = 1
            PercentageLabel2.alpha = 1
        }
        
        if(redColor3 != smallest){
            LTLabel3.alpha = 1
            LoLabel1.alpha = 1
            PercentageLabel3.alpha = 1
        }
        
        if(redColor4 != smallest){
            LTLabel4.alpha = 1
            LoLabel2.alpha = 1
            PercentageLabel4.alpha = 1
        }
        
    }
    

    
    @IBAction func ShowResultsAction(_ sender: Any) {
        UIView.animate(withDuration: 0.3, animations: {
            self.LeftEyeButton.alpha = 1
            self.RightEyeButton.alpha = 1
        })
    }
    
    @IBAction func Circle1ActionStop(_ sender: Any) {
        timer.invalidate()
    }
    
    // Exit the test
    @IBAction func ExitTestAction(_ sender: Any) {
        UIScreen.main.brightness = br
        NotificationCenter.default.post(name: Notification.Name(rawValue: "MainMenuDefaultCheck"), object: nil)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func RepeatTestAction(_ sender: Any) {
        ResetTest()
    }
    
    // Hide top bar
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
}
