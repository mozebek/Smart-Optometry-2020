//
//  AmslerGridViewController.swift
//  SmartOptometry2k19
//
//  Created by Matic Ozebek on 23/05/2019.
//  Copyright © 2019 Matic Ozebek. All rights reserved.
//

import UIKit

class AmslerGridViewController: UIViewController {

    @IBOutlet weak var AmslerView: UIView!
    @IBOutlet weak var CenterDot: UIView!
    @IBOutlet weak var tempImageView: UIImageView!
    @IBOutlet weak var ThicknesSlider: UISlider!
    @IBOutlet weak var PencilColor1: UIButton!
    @IBOutlet weak var PencilColor2: UIButton!
    @IBOutlet weak var ScreenshotView: UIView!
    @IBOutlet weak var ThicknessLabel: UILabel!
    @IBOutlet weak var ColorLabel: UILabel!
    @IBOutlet weak var GridColorLabel: UILabel!
    
    var lastPoint = CGPoint.zero
    var pencilColor = UIColor.black
    var brushWidth: CGFloat = 10.0
    var opacity: CGFloat = 1.0
    var swiped = false
    var SCAnimationView: UIView = UIView()
    
    // Brightness
    var br: CGFloat = 1
    
    enum GridType {
        case GridType1
        case GridType2
        case GridType3
        case GridType4
        case GridType5
        case GridType6
    }
    
    // Set first grid color on load
    var CurrentGrid = GridType.GridType1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Save current brightnes
        br = UIScreen.main.brightness
        
        if(Helper.app.CheckForBrightness()){
            UIScreen.main.brightness = CGFloat(1)
        }
        
        // Set orientation to portrait
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        
        // Set transaltes
        ColorLabel.text = "color".localized
        GridColorLabel.text = "grid_color".localized
        ThicknessLabel.text = "thickness".localized
        
        //Create screenshot view for effect
        let screenSize: CGRect = UIScreen.main.bounds
        SCAnimationView = UIView(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height))
        SCAnimationView.backgroundColor = UIColor.white;
        self.view.addSubview(SCAnimationView)
        SCAnimationView.isHidden = true
        self.view.bringSubviewToFront(SCAnimationView)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        CreateAmslerGrid(gridType: CurrentGrid)
    }
    
    func CreateAmslerGrid(gridType: GridType){
        
        // Get colors for selected color combination
        var colors = getGridColors(gridType: gridType)
        // Colorize background
        AmslerView.backgroundColor = colors[1]
        
        // Set middle dot
        Helper.app.SetCircle(CView: CenterDot, Color: colors[0]);
        Helper.app.SetBorder(CView: AmslerView, borderWidth: 1, borderColor:colors[0])
        
        let spacing = self.AmslerView.frame.width/20;
        print(spacing)
        
        
        for index in 1...20 {
            addLine(fromPoint: CGPoint(x: 0, y: spacing * CGFloat(index)), toPoint: CGPoint(x: self.AmslerView.frame.size.width, y: spacing * CGFloat(index)), color: colors[0])
        }
        
        for index in 1...20 {
            addLine(fromPoint: CGPoint(x: spacing * CGFloat(index), y:0 ), toPoint: CGPoint(x: spacing * CGFloat(index), y: self.AmslerView.frame.size.width), color: colors[0])
        }
        
    }
    
    // Drawing the grid
    func addLine(fromPoint start: CGPoint, toPoint end:CGPoint, color:UIColor) {
        let line = CAShapeLayer()
        let linePath = UIBezierPath()
        linePath.move(to: start)
        linePath.addLine(to: end)
        line.path = linePath.cgPath
        line.strokeColor = color.cgColor
        line.lineWidth = 1
        line.lineJoin = CAShapeLayerLineJoin.round
        self.AmslerView.layer.addSublayer(line)
    }
    
    func removeGrid(){
        for v in AmslerView.subviews{
            v.removeFromSuperview()
        }
    }

    
    //////////////////////////// Touch drawing  ----->
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else {
            return
        }
        swiped = false
        lastPoint = touch.location(in: tempImageView)
    }
    
    func drawLine(from fromPoint: CGPoint, to toPoint: CGPoint) {
        // 1
        UIGraphicsBeginImageContext(tempImageView.frame.size)
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }
        tempImageView.image?.draw(in: tempImageView.bounds)
        
        // 2
        context.move(to: fromPoint)
        context.addLine(to: toPoint)
        
        // 3
        context.setLineCap(.round)
        context.setBlendMode(.normal)
        context.setLineWidth(CGFloat(ThicknesSlider!.value))
        context.setStrokeColor(pencilColor.cgColor)
        
        // 4
        context.strokePath()
        
        // 5
        tempImageView.image = UIGraphicsGetImageFromCurrentImageContext()
        tempImageView.alpha = opacity
        UIGraphicsEndImageContext()
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else {
            return
        }
        
        // 6
        swiped = true
        let currentPoint = touch.location(in: tempImageView)
        drawLine(from: lastPoint, to: currentPoint)
        
        // 7
        lastPoint = currentPoint
    }
    
    ///////////////////////////////////////////////////////////
    
    // Get color combination from enum
    
    func  getGridColors(gridType: GridType) -> [UIColor]{
        var colors: [UIColor] = []
        var netColor: UIColor = UIColor.white
        var backgroundColor: UIColor = UIColor.white
        var pencil1 : UIColor = UIColor.white
        var pencil2: UIColor = UIColor.white
        
        
        if(gridType == GridType.GridType1){
            netColor = UIColor.black
            backgroundColor = UIColor.white
            pencil1 = UIColor.white
            pencil2 = UIColor.black
            PencilColor1.backgroundColor = UIColor.white
            PencilColor2.backgroundColor = UIColor.black
            pencilColor = UIColor.black
        }
        else if(gridType == GridType.GridType2){
            netColor = UIColor.white
            backgroundColor = UIColor.black
            pencil1 = UIColor.white
            pencil2 = UIColor.black
            PencilColor1.backgroundColor = UIColor.white
            PencilColor2.backgroundColor = UIColor.black
            pencilColor = UIColor.white
        }
        else if(gridType == GridType.GridType3){
            netColor = UIColor.black
            backgroundColor = UIColor.red
            pencil1 = UIColor.red
            pencil2 = UIColor.black
            PencilColor1.backgroundColor = UIColor.red
            PencilColor2.backgroundColor = UIColor.black
            pencilColor = UIColor.black
        }
        else if(gridType == GridType.GridType4){
            netColor = UIColor.red
            backgroundColor = UIColor.black
            pencil1 = UIColor.red
            pencil2 = UIColor.black
            PencilColor1.backgroundColor = UIColor.red
            PencilColor2.backgroundColor = UIColor.black
            pencilColor = UIColor.red
        }
        else if(gridType == GridType.GridType5){
            netColor = UIColor.blue
            backgroundColor = UIColor.yellow
            pencil1 = UIColor.yellow
            pencil2 = UIColor.blue
            PencilColor1.backgroundColor = UIColor.yellow
            PencilColor2.backgroundColor = UIColor.blue
            pencilColor = UIColor.blue
        }
        else if(gridType == GridType.GridType6){
            netColor = UIColor.yellow
            backgroundColor = UIColor.blue
            pencil1 = UIColor.yellow
            pencil2 = UIColor.blue
            PencilColor1.backgroundColor = UIColor.yellow
            PencilColor2.backgroundColor = UIColor.blue
            pencilColor = UIColor.yellow
        }
        
        colors.append(netColor)
        colors.append(backgroundColor)
        colors.append(pencil1)
        colors.append(pencil2)
        
        return colors
    }
    
    // Eraser
    func EraseDrawing(){
        tempImageView.image = nil
    }
    
    // Hides status bar once test opened
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    /////////////////// Screenshoot ------>
    
    open func takeScreenshot(_ shouldSave: Bool = true) {
        var screenshotImage :UIImage?
        let layer = ScreenshotView.layer
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(layer.frame.size, false, scale);
        guard let context = UIGraphicsGetCurrentContext() else { return }
        layer.render(in:context)
        screenshotImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        if let image = screenshotImage, shouldSave {
            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
        }
        
        // Animate screenshoot
        SCAnimationView.isHidden = false
        UIView.animate(withDuration: 0.2, animations: {
            self.SCAnimationView.alpha = 1;
        },completion: {(finished:Bool) in
            UIView.animate(withDuration: 0.2, animations: {
                self.SCAnimationView.alpha = 0
            },completion: {(finished:Bool) in
               self.SCAnimationView.isHidden = true
                })
        })
    }
    
    //////////////////////////////////////////////////////
    
    // -----------------> Button Actions
    

  
    @IBAction func Erase(_ sender: Any) {
        EraseDrawing()
    }
    
    @IBAction func TakeScreenShoot(_ sender: Any) {
        
        takeScreenshot()
    }
    
    @IBAction func SelectColor1(_ sender: Any) {
        var colors = getGridColors(gridType: CurrentGrid)
        pencilColor = colors[2]
    }
    
    @IBAction func SelectColor2(_ sender: Any) {
        var colors = getGridColors(gridType: CurrentGrid)
        pencilColor = colors[3]
    }
    @IBAction func SelectGrid1(_ sender: Any) {
        removeGrid()
        EraseDrawing()
        CurrentGrid = GridType.GridType1
        CreateAmslerGrid(gridType: CurrentGrid)
    }
    
    @IBAction func SelectGrid2(_ sender: Any) {
        removeGrid()
        EraseDrawing()
        CurrentGrid = GridType.GridType2
        CreateAmslerGrid(gridType: CurrentGrid)
    }
    
    @IBAction func SelectGrid3(_ sender: Any) {
        removeGrid()
        EraseDrawing()
        CurrentGrid = GridType.GridType3
        CreateAmslerGrid(gridType: CurrentGrid)
    }
    
    @IBAction func SelectGrid4(_ sender: Any) {
        removeGrid()
        EraseDrawing()
        CurrentGrid = GridType.GridType4
        CreateAmslerGrid(gridType: CurrentGrid)
    }
    @IBAction func SelectGrid5(_ sender: Any) {
        removeGrid()
        EraseDrawing()
        CurrentGrid = GridType.GridType5
        CreateAmslerGrid(gridType: CurrentGrid)
    }
    
    @IBAction func SelectGrid6(_ sender: Any) {
        removeGrid()
        EraseDrawing()
        CurrentGrid = GridType.GridType6
        CreateAmslerGrid(gridType: CurrentGrid)
    }
    
    
    // Exit the test
    @IBAction func ExitTestAction(_ sender: Any) {
        UIScreen.main.brightness = br
        NotificationCenter.default.post(name: Notification.Name(rawValue: "MainMenuDefaultCheck"), object: nil)
        self.dismiss(animated: true, completion: nil)
    }
    
    // Allow rotation
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var shouldAutorotate: Bool {
        return true
    }

}
