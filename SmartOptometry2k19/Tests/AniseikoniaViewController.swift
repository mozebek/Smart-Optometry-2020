//
//  AniseikoniaViewController.swift
//  SmartOptometry2k19
//
//  Created by Matic Ozebek on 20/08/2019.
//  Copyright © 2019 Matic Ozebek. All rights reserved.
//

import UIKit

class AniseikoniaViewController: UIViewController {

    @IBOutlet weak var Bracket1: UIView!
    @IBOutlet weak var Bracket2: UIView!
    @IBOutlet weak var BracketsView: UIView!
    @IBOutlet weak var MiddleDotView: UIView!
    @IBOutlet weak var MeridianLabel: UILabel!
    @IBOutlet weak var ResultLabel: UILabel!
    @IBOutlet weak var DoneButton: UIButton!
    
    let border1: CALayer = CALayer()
    let border2: CALayer = CALayer()
    var Bracket1OrigPos: CGPoint = CGPoint()
    
    @IBOutlet weak var SizeSlider: UISlider!
    var rotation: Bool = false;
    
    // Brightness
    var br: CGFloat = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Save current brightnes
        br = UIScreen.main.brightness
        // Check for auto brightness
        if(Helper.app.CheckForBrightness()){
            UIScreen.main.brightness = CGFloat(1)
        }
        
        
        DoneButton.setTitle(NSLocalizedString("done", comment: "").uppercased(), for: .normal)
        
        // Rotate screen to landscape
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        
        // Set middle view to become dot
        MiddleDotView.layer.cornerRadius = MiddleDotView.frame.height/2
        
        // Hide results labels
        self.ResultLabel.alpha = 0
        self.MeridianLabel.alpha = 0
        
        Bracket1OrigPos = Bracket1.center
        // Do any additional setup after loading the view.
        
        Bracket1.clipsToBounds = true
        Bracket2.clipsToBounds = true
        
        
    
        
        border1.borderColor = UIColor.green.cgColor
        border1.borderWidth = 12;
        border1.frame = CGRect(x: 0, y: 0, width: Bracket1.frame.width + 12, height: Bracket1.frame.height)
        Bracket1.layer.addSublayer(border1)
        
        border2.borderColor = UIColor.red.cgColor
        border2.borderWidth = 12;
        border2.frame = CGRect(x: -12, y: 0, width: Bracket2.frame.width + 12, height: Bracket2.frame.height)
        Bracket2.layer.addSublayer(border2)
    }
    
    @IBAction func SizeSliderChanged(_ sender: UISlider) {
       
        UIView.animate(withDuration: 0.2, animations: {
            self.ResultLabel.alpha = 0
            self.MeridianLabel.alpha = 0
        })
        
        Bracket1.frame = CGRect(x: Bracket1OrigPos.x, y: Bracket1OrigPos.y, width: Bracket1.frame.width, height: CGFloat(sender.value))
        Bracket1.center = Bracket1OrigPos
        
        CATransaction.setValue(kCFBooleanTrue, forKey:kCATransactionDisableActions)
        border1.frame = CGRect(x: 0, y: 0, width: Bracket1.frame.width + 12, height: CGFloat(sender.value) )
        CATransaction.commit()
        
        print(sender.value)
        
    }
    
    // Buttons actions
    
    // Exit the test
    @IBAction func ExitTestAction(_ sender: Any) {
        UIScreen.main.brightness = br
        NotificationCenter.default.post(name: Notification.Name(rawValue: "MainMenuDefaultCheck"), object: nil)
        self.dismiss(animated: true, completion: nil)
    }
    
    // Rotate the brackets
    @IBAction func RoateBracketsAction(_ sender: Any) {
        
        if(!rotation){
            UIView.animate(withDuration: 0.5, animations: {
                self.BracketsView.transform = CGAffineTransform(rotationAngle: (.pi * -1) / 2)
            }, completion: {(finished:Bool) in
                self.SetBracketsToDefault()
            })
            rotation = true;
        }
        else{
            UIView.animate(withDuration: 0.5, animations: {
                self.BracketsView.transform = CGAffineTransform(rotationAngle: 0)
            }, completion: {(finished:Bool) in
                self.SetBracketsToDefault()
            })
            rotation = false;
        }
        
        UIView.animate(withDuration: 0.2, animations: {
            self.ResultLabel.alpha = 0
            self.MeridianLabel.alpha = 0
        })
        
        
        
    }
    
    func SetBracketsToDefault(){
       UIView.animate(withDuration: 0.17, animations: {
            self.Bracket1.frame = CGRect(x: self.Bracket1OrigPos.x, y: self.Bracket1OrigPos.y, width: self.Bracket1.frame.width, height: self.BracketsView.frame.height)
            self.Bracket1.center = self.Bracket1OrigPos
        
            self.SizeSlider.setValue(((self.SizeSlider.maximumValue - self.SizeSlider.minimumValue) / 2) + self.SizeSlider.minimumValue, animated:true)
        
            //CATransaction.setValue(kCFBooleanTrue, forKey:kCATransactionDisableActions)
            self.border1.frame = CGRect(x: 0, y: 0, width: self.Bracket1.frame.width + 12, height: self.BracketsView.frame.height)
            //CATransaction.commit()
        })
        
       

    }
    
    @IBAction func ShowResult(_ sender: Any) {
        
        // Animate showing results
        UIView.animate(withDuration: 0.2, animations: {
            self.ResultLabel.alpha = 1
            self.MeridianLabel.alpha = 1
        })
        
        // Set meridian text
        if(!rotation){
            MeridianLabel.text = NSLocalizedString("horizontal_meridian_upper_case", comment: "")
        }
        else{
            MeridianLabel.text = NSLocalizedString("vertical_meridian_upper_case", comment: "")
        }
        
        // Caculate the difference percetage
        let percentage = (SizeSlider.value*100)/153-100
        
        if(percentage >= -1 && percentage <= 1){
            ResultLabel.text = NSLocalizedString("both_eyes_same", comment: "")
        }
        else if(percentage < 1){
            let localizedString = NSLocalizedString("right_eye_has", comment: "")
            let stringWithParams = String(format: localizedString, String(format: "%.1f",abs(percentage)) + "%")
            ResultLabel.text = stringWithParams
        }
        else if(percentage > 1){
            let localizedString = NSLocalizedString("left_eye_has", comment: "")
            let stringWithParams = String(format: localizedString, String(format: "%.1f",abs(percentage)) + "%")
            ResultLabel.text = stringWithParams
        }
    }
    
    // Allow rotation
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    // Hide top bar
    override var prefersStatusBarHidden: Bool {
        return true
    }
}
