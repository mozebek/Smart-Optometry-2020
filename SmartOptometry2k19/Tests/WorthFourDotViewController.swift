//
//  WorthFourDotViewController.swift
//  SmartOptometry2k19
//
//  Created by Matic Ozebek on 21/05/2019.
//  Copyright © 2019 Matic Ozebek. All rights reserved.
//

import UIKit

class WorthFourDotViewController: UIViewController {

    @IBOutlet weak var TopCircle: UIView!
    @IBOutlet weak var BottomCircle: UIView!
    @IBOutlet weak var LeftCircle: UIView!
    @IBOutlet weak var RightCirlce: UIView!
    
    @IBOutlet weak var GreenButton: UIButton!
    @IBOutlet weak var WhiteButton: UIButton!
    @IBOutlet weak var RedButton: UIButton!
    @IBOutlet weak var ResultLabel: UILabel!
    
    @IBOutlet weak var ExitButton: UIButton!
    @IBOutlet weak var ColorButton: UIButton!
    @IBOutlet weak var InfoButton: UIButton!
    
    // Brightness
    var br: CGFloat = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Save current brightnes
        br = UIScreen.main.brightness
        // Check for auto brightness
        if(Helper.app.CheckForBrightness()){
            UIScreen.main.brightness = CGFloat(1)
        }

    }
    
    // Set up view on init
    override func viewDidAppear(_ animated: Bool){

        // Hide result label
        ResultLabel.alpha = 0;
        
        // Set middle circles
        SetCircle(CView: TopCircle, Color: UIColor.red)
        SetCircle(CView: BottomCircle, Color: UIColor.white)
        SetCircle(CView: LeftCircle, Color: UIColor.green)
        SetCircle(CView: RightCirlce, Color: UIColor.green)
        SetBorder(CView: BottomCircle);
        
        // Set buttons
        SetCircle(CView: GreenButton, Color: UIColor.green)
        SetCircle(CView: WhiteButton, Color: UIColor.white)
        SetCircle(CView: RedButton, Color: UIColor.red)
        SetBorder(CView: WhiteButton);
    }
    
    // Show result after user clicks on button
    func Showlabel(labelText: String){
        ResultLabel.alpha = 1;
        ResultLabel.text = labelText.localized
        UIView.animate(withDuration: 3, animations: {
            self.ResultLabel.alpha = 0
        })
    }
    
    //Method to make circle shape view
    func SetCircle(CView: UIView, Color: UIColor){
        CView.layer.cornerRadius = CView.frame.size.height/2;
        CView.layer.masksToBounds = true;
        CView.backgroundColor = Color;
        
    }
    
    // Add border to view
    func SetBorder(CView: UIView){
        CView.layer.borderColor = UIColor.black.cgColor
        CView.layer.borderWidth = 3
    }
    
    // Hides status bar once test opened
    override var prefersStatusBarHidden: Bool {
        return true
    }

    // -----------------> Button Actions
    @IBAction func GreenCicleAction(_ sender: Any) {
        Showlabel(labelText: "left_dominant")
    }
    
    @IBAction func WhiteCircleAction(_ sender: Any) {
        Showlabel(labelText: "no_dominant")
    }
    
    @IBAction func RedCircleAction(_ sender: Any) {
        Showlabel(labelText: "right_dominant")
    }
    
    @IBAction func ExitTestAction(_ sender: Any) {
        UIScreen.main.brightness = br
        NotificationCenter.default.post(name: Notification.Name(rawValue: "MainMenuDefaultCheck"), object: nil)
        self.dismiss(animated: true, completion: nil)
    }
    
    // Invert colors for test
    @IBAction func ColorizeBackground(_ sender: Any) {
        
        if(self.view.backgroundColor == UIColor.white){
            self.view.backgroundColor = UIColor.black
            ExitButton.tintColor = UIColor.white
            InfoButton.tintColor = UIColor.white
            ColorButton.tintColor = UIColor.white
            ResultLabel.textColor = UIColor.white
        }
        else{
            self.view.backgroundColor = UIColor.white
            ExitButton.tintColor = UIColor.black
            InfoButton.tintColor = UIColor.black
            ColorButton.tintColor = UIColor.black
            ResultLabel.textColor = UIColor.black
        }
            
    }
    
    // Allow rotation
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var shouldAutorotate: Bool {
        UIScreen.main.brightness = br;
        return true
    }
    
}

