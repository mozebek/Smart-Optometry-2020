//
//  FlourescinLightViewController.swift
//  SmartOptometry2k19
//
//  Created by Matic Ozebek on 23/05/2019.
//  Copyright © 2019 Matic Ozebek. All rights reserved.
//

import UIKit

class FlourescinLightViewController: UIViewController {

    // Brightness
    var br: CGFloat = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Save current brightnes
        br = UIScreen.main.brightness

        // Set orientation to portrait
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        
        // Brightness to full
        UIScreen.main.brightness = CGFloat(1)
        
        // Do any additional setup after loading the view.
    }
    
    
    
    // -----------------> Button Actions
    
    // Exit the test
    @IBAction func ExitTestAction(_ sender: Any) {
        UIScreen.main.brightness = br
        NotificationCenter.default.post(name: Notification.Name(rawValue: "MainMenuDefaultCheck"), object: nil)
        self.dismiss(animated: true, completion: nil)
    }
    
    // Allow rotation
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    // Hides status bar once test opened
    override var prefersStatusBarHidden: Bool {
        return true
    }


}
