//
//  AccommodationViewController.swift
//  SmartOptometry2k19
//
//  Created by Matic Ozebek on 19/08/2019.
//  Copyright © 2019 Matic Ozebek. All rights reserved.
//

import UIKit

class AccommodationViewController: UIViewController {

    @IBOutlet weak var TimerLabel: UILabel!
    @IBOutlet weak var Stopwatch: UIView!
    @IBOutlet weak var WordLabel: UILabel!
    @IBOutlet weak var ResultsView: UIView!
    @IBOutlet weak var FirstPartResultLabel: UILabel!
    @IBOutlet weak var SecondPartResultLabel: UILabel!
    @IBOutlet weak var ResultsInterpretationView: UIView!
    @IBOutlet weak var ResultsHeaderView: UIView!
    @IBOutlet weak var ResultsLabel: UILabel!
    @IBOutlet weak var Result1Label: UILabel!
    @IBOutlet weak var Result2Label: UILabel!
    
    
    var seconds = 60
    var timer = Timer()
    var isTimerRunning = false
    var IsPerforming: Bool = false
    var cp = CircularProgressView()
    var WordsArray:[String] = ["bread","people","after","beacause","person","large","think","child","great","different","number", "public","problem", "other", "hand", "leave", "point","fact", "young", "history",  "world", "family", "health", "meat", "year", "music","power","internet", "control", "science", "product", "activity", "language", "player", "week", "quality", "oven", "moon", "ability", "movie", "direction", "camera", "paper", "mouth", "strategy", "analysis", "army", "goal", "fishing", "university", "college", "article", "income", "user", "marriage", "teacher", "road", "moment","paint", "wood", "office", "student", "event", "politics", "driver", "flight", "river", "member", "woman", "blood", "health", "advice", "effort", "reality", "skill", "city", "heart", "photo", "director", "alcohol", "union", "banana", "apple", "orange",  "drama", "golf", "tennis", "football", "virus", "sample", "editor", "guitar", "basket", "lesson","hair", "police"]
    
    var firstCounter: Int = 0
    var secondCounter: Int = 0
    
    // Brightness
    var br: CGFloat = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Save current brightnes
        br = UIScreen.main.brightness
        // Check for auto brightness
        if(Helper.app.CheckForBrightness()){
            UIScreen.main.brightness = CGFloat(1)
        }

        // Set orientation to portrait
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        
        // Swipe gesture registration
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.handleGesture(gesture:)))
        swipeLeft.direction = .left
        self.view.addGestureRecognizer(swipeLeft)
        
      
        
        // Hide results view
        ResultsView.alpha = 0
        
        // Set label
        self.WordLabel.text = "acc_line1".localized
        ResultsLabel.text = "results".localized
        Result1Label.text = "first_thirty".localized
        Result2Label.text = "second_thirty".localized

    }
    
    override func viewDidAppear(_ animated: Bool) {
        // Set results interpretation rounded corners
        ResultsInterpretationView.layer.cornerRadius = 30
        Helper.app.addGradientToView(view: ResultsHeaderView,
                                     color1: UIColor.init(displayP3Red: 25/255, green: 154/255, blue: 143/255, alpha: 1),
                                     color2: UIColor.init(displayP3Red: 35/255, green: 188/255, blue: 121/255, alpha: 1))        // Set results interpretation rounded corners
        
        // Add new progress view
        addProgressView()
    }
    
    // Init new progress view
    func addProgressView(){
        
        // Get safe area size
        var topSafeAreaHeight: CGFloat = 0
        
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.windows[0]
            let safeFrame = window.safeAreaLayoutGuide.layoutFrame
            topSafeAreaHeight = safeFrame.minY
        }
        
        // Progress view init
        cp = CircularProgressView(frame: CGRect(x: 0.0, y: 0.0, width: Stopwatch.frame.width, height: Stopwatch.frame.height))
        cp.trackColor = UIColor(red: 25.0/255.0, green: 154.0/255.0, blue: 143.0/255.0, alpha: 0.2)
        cp.progressColor = UIColor(red: 25.0/255.0, green: 154.0/255.0, blue: 143.0/255.0, alpha: 1.0)
        cp.tag = 101
        self.view.addSubview(cp)
        //self.view.bringSubviewToFront(cp)
        cp.center = CGPoint(x: Stopwatch.center.x, y: Stopwatch.center.y+topSafeAreaHeight)
        //TimerLabel.center = cp.center
    }
    
    // Animate circeled progress bar
    @objc func animateProgress() {
        let cP = self.view.viewWithTag(101) as! CircularProgressView
        cP.setProgressWithAnimation(duration: 60.0, value: 1.0)
    }

    // Start animation and timer for the test
    func runTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(updateTimer)), userInfo: nil, repeats: true)
        self.perform(#selector(animateProgress), with: nil, afterDelay: 0.0)
    }
    
    // Updating timer
    @objc func updateTimer() {
        if(seconds > 0){
            seconds -= 1
        }
        else{
            EndTest()
        }
        TimerLabel.text = "\(seconds)"
    }
    
    // Swipe left geasture handle
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        if gesture.direction == UISwipeGestureRecognizer.Direction.left {
            if(IsPerforming == false){
                runTimer()
                IsPerforming = true
                ChangeWord(word: "")
            }
            else{
                ChangeWord(word: "")
                if(seconds >= 30){
                    firstCounter = firstCounter+1
                }
                else{
                    secondCounter = secondCounter+1
                }
                print(firstCounter)
                print(secondCounter)
            }
            
        }
    }
    
    // Changing the main word in the label
    func ChangeWord(word: String){
        UIView.animate(withDuration: 0.1, animations: {
            self.WordLabel.transform = CGAffineTransform(translationX: -160, y: 0)
            self.WordLabel.alpha = 0
        },completion: {(finished:Bool) in
            if(word == ""){
                self.WordLabel.text = self.WordsArray.getRandomFromArray().localized
            }
            else{
                self.WordLabel.text = word.localized
            }
           // self.WordLabel.center = CGPoint(x: self.WordLabel.center.x + 160, y: self.WordLabel.center.y)
            self.WordLabel.transform = CGAffineTransform(translationX: +160, y: 0)
            UIView.animate(withDuration: 0.1, animations: {
                self.WordLabel.transform = CGAffineTransform(translationX: 0, y: 0)
                self.WordLabel.alpha = 1
            })
        })
    }
    
    // After end show results
    func EndTest(){
        timer.invalidate()
        cp.removeFromSuperview()
        FirstPartResultLabel.text = String(firstCounter)
        SecondPartResultLabel.text = String(secondCounter)
        UIView.animate(withDuration: 0.2, animations: {
            self.ResultsView.alpha = 1
        })
        
    }
    
    // Allow rotation
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    // Hide top bar
    override var prefersStatusBarHidden: Bool {
        return true
    }
    // Buttons actions
    
    // Exit the test
    @IBAction func ExitTestAction(_ sender: Any) {
        UIScreen.main.brightness = br
        NotificationCenter.default.post(name: Notification.Name(rawValue: "MainMenuDefaultCheck"), object: nil)
        self.dismiss(animated: true, completion: nil)
    }
    
    // Repeat test
    @IBAction func RepeatTestAction(_ sender: Any) {
        
        if(cp.superview === self.view){
            cp.removeFromSuperview()
        }
        
        firstCounter = 0
        secondCounter = 0
        timer.invalidate()
        addProgressView()
        IsPerforming = false
        seconds = 60
        TimerLabel.text = "60"
        ChangeWord(word: "acc_line1")
        
        UIView.animate(withDuration: 0.2, animations: {
            self.ResultsView.alpha = 0
        })
    }

}

extension Array {
    func getRandomFromArray() -> Element {
        let index = Int(arc4random_uniform(UInt32(self.count)))
        return self[index]
    }
}

