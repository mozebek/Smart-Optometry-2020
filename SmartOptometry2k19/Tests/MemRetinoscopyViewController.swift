//
//  MemRetinoscopyViewController.swift
//  SmartOptometry2k19
//
//  Created by Matic Ozebek on 21/08/2019.
//  Copyright © 2019 Matic Ozebek. All rights reserved.
//

import UIKit
import AVKit


class MemRetinoscopyViewController: UIViewController {

     @IBOutlet weak var viewPlay: UIView!
    
    var playerLayer = AVPlayerLayer()
    
    // Brightness
    var br: CGFloat = 1
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Rotate screen to landscape
        let value = UIInterfaceOrientation.landscapeRight.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        
        // Save current brightnes
        br = UIScreen.main.brightness
        // Check for auto brightness
        if(Helper.app.CheckForBrightness()){
            UIScreen.main.brightness = CGFloat(1)
        }
        
      
    }
    
    // Set position of layer after rotation of screen
    override func viewWillLayoutSubviews() {
        playerLayer.position = CGPoint(x: viewPlay.center.x, y:viewPlay.center.y)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Set up and loop the video
        let path = Bundle.main.path(forResource: "lalinea5", ofType: "mp4.3gp")
        let videoURL = NSURL(fileURLWithPath: path!)
        let player = AVPlayer(url: videoURL as URL)
        
        playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = CGRect(x: viewPlay.center.x , y: viewPlay.center.y, width: self.view.frame.height/3, height: self.viewPlay.frame.height)
        playerLayer.position = CGPoint(x: playerLayer.position.x-playerLayer.frame.width/2, y: playerLayer.position.y-playerLayer.frame.height/2)
        playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        viewPlay.layer.addSublayer(playerLayer)
        
        player.play();
        loopVideo(videoPlayer: player)
        
    }

    // Loop the video
    func loopVideo(videoPlayer: AVPlayer) {
        
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: videoPlayer.currentItem, queue: .main) { _ in
            videoPlayer.seek(to: CMTime.zero)
            videoPlayer.play()
            }
    }
    
    
    
    // Allow rotation
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .landscapeRight
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    // Buttons actions
    
    // Exit the test
    @IBAction func ExitTestAction(_ sender: Any) {
        UIScreen.main.brightness = br
        NotificationCenter.default.post(name: Notification.Name(rawValue: "MainMenuDefaultCheck"), object: nil)
        self.dismiss(animated: true, completion: nil)
    }
    
    
}
