//
//  DuochromeViewController.swift
//  SmartOptometry2k19
//
//  Created by Matic Ozebek on 30/05/2019.
//  Copyright © 2019 Matic Ozebek. All rights reserved.
//

import UIKit

class DuochromeViewController: UIViewController {

    // Brightness
    var br: CGFloat = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Save current brightnes
        br = UIScreen.main.brightness
        // Check for auto brightness
        if(Helper.app.CheckForBrightness()){
            UIScreen.main.brightness = CGFloat(1)
        }
        

        // Rotate screen to landscape
        let value = UIInterfaceOrientation.landscapeRight.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
    }
    
    // Allow rotation
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .landscapeRight
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    // -----------------> Button Actions
    
    // Exit the test
    @IBAction func ExitTestAction(_ sender: Any) {
        UIScreen.main.brightness = br
        NotificationCenter.default.post(name: Notification.Name(rawValue: "MainMenuDefaultCheck"), object: nil)
        self.dismiss(animated: true, completion: nil)
    }

    // Hides status bar once test opened
    override var prefersStatusBarHidden: Bool {
        return true
    }
}
