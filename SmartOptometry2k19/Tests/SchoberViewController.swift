//
//  SchoberViewController.swift
//  SmartOptometry2k19
//
//  Created by Matic Ozebek on 22/05/2019.
//  Copyright © 2019 Matic Ozebek. All rights reserved.
//

import UIKit

class SchoberViewController: UIViewController {

    @IBOutlet weak var CircleView: UIView!
    @IBOutlet weak var CrossView: UIView!
    @IBOutlet weak var XValueLabel: UILabel!
    @IBOutlet weak var XHeadingLabel: UILabel!
    @IBOutlet weak var LeftEyeHeadingLabel: UILabel!
    @IBOutlet weak var RightEyeHedingLabel: UILabel!
    @IBOutlet weak var LeftEyeResult: UILabel!
    @IBOutlet weak var RightEyeValue: UILabel!
    @IBOutlet weak var Headings: UIView!
    @IBOutlet weak var Results: UIView!
    @IBOutlet weak var Toolbar: UIView!
    
    // Toolbar
    @IBOutlet weak var ExitButton: UIButton!
    @IBOutlet weak var ColorButton: UIButton!
    @IBOutlet weak var InfoButton: UIButton!
    
    var x: CGFloat = 0;
    var y: CGFloat = 0;
    
    // Brightness
    var br: CGFloat = 1
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Save current brightnes
        br = UIScreen.main.brightness
        // Check for auto brightness
        if(Helper.app.CheckForBrightness()){
            UIScreen.main.brightness = CGFloat(1)
        }

        // Set orientation to portrait
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        
        LeftEyeHeadingLabel.text = "left_eye".localized
        RightEyeHedingLabel.text = "right_eye".localized
        
        // Make circle view and add border
        Helper.app.SetCircle(CView: CircleView, Color: UIColor.clear)
        Helper.app.SetBorder(CView: CircleView, borderWidth: 6, borderColor: UIColor.green)
        // Set cross to center of the screen
        CrossView.center = CGPoint(x: self.view.center.x, y: self.view.center.y)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        // Set cross to center of the screen
        CrossView.center = CGPoint(x: self.view.center.x, y: self.view.center.y)
        // Make circle view and add border
        Helper.app.SetCircle(CView: CircleView, Color: UIColor.clear)
    }
    
    // Get cross location for start moving
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first!
        let location = touch.location(in: self.view)
        
        // Center cross inside view
        x = CrossView.center.x - location.x;
        y = CrossView.center.y - location.y
        
        // Hide toolbar and results when start moving
        UIView.animate(withDuration: 0.2, animations: {
            self.Headings.alpha = 0
            self.Results.alpha = 0
            self.Toolbar.alpha = 0
        })
        
    }
    
    // Move cross across the screen
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first!
        let location = touch.location(in: self.view)
        
        CrossView.center = CGPoint(x: location.x + CGFloat(x), y: location.y + CGFloat(y))
        
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        // Show toolbar and results
        UIView.animate(withDuration: 0.2, animations: {
            self.Headings.alpha = 1
            self.Results.alpha = 1
            self.Toolbar.alpha = 1
        })
        
        // Get cross position and center location
        let crossLocation =  CrossView.center
        let defaultLocation = CGPoint(x: self.view.center.x, y: self.view.center.y)
        
        // Calculate values
        var xDist = (defaultLocation.x - crossLocation.x);
        xDist = xDist * 0.04625;
        
        var yDist = (defaultLocation.y - crossLocation.y);
        yDist = yDist * 0.04625;
        
        //print(xDist, "    ", yDist)
        
        // Print x result
        XValueLabel.text = String(format: "%.1f", abs(xDist))
        if(xDist < 0.1 && xDist > -0.1){
            XHeadingLabel.text = "center".localized
            XValueLabel.text = "0.0"
        }
        else if(xDist > 0.1){
            XHeadingLabel.text = "nasal".localized
        }
        else if(xDist < 0.1){
            XHeadingLabel.text = "temporal".localized
        }
        
        // Print y result
        if( yDist < 0.1 && yDist > -0.1){
            RightEyeValue.text = "center".localized
            LeftEyeResult.text =  "center".localized
        }
        else if(yDist > 0.1){
            var localizedString = "up".localized
            RightEyeValue.text = String(format: localizedString, String(format: "%.1f",abs(yDist)))
            
            localizedString = "down".localized
            LeftEyeResult.text = String(format: localizedString, String(format: "%.1f",abs(yDist)))
        }
        else if(yDist < 0.1){
            var localizedString = "down".localized
            RightEyeValue.text = String(format: localizedString, String(format: "%.1f",abs(yDist)))
            
            localizedString = "up".localized
            LeftEyeResult.text = String(format: localizedString, String(format: "%.1f",abs(yDist)))
        }
        
    }
    
    // Hides status bar once test opened
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    // -----------------> Button Actions
    
    
    
    // Invert colors for test
    @IBAction func ColorizeBackground(_ sender: Any) {
        
        if(self.view.backgroundColor == UIColor.white){
            self.view.backgroundColor = UIColor.black
            ExitButton.tintColor = UIColor.white
            InfoButton.tintColor = UIColor.white
            ColorButton.tintColor = UIColor.white
            RightEyeValue.textColor = UIColor.white
            LeftEyeResult.textColor = UIColor.white
            LeftEyeHeadingLabel.textColor = UIColor.white
            RightEyeHedingLabel.textColor = UIColor.white
            XValueLabel.textColor = UIColor.white
            XHeadingLabel.textColor = UIColor.white
        }
        else{
            self.view.backgroundColor = UIColor.white
            ExitButton.tintColor = UIColor.black
            InfoButton.tintColor = UIColor.black
            ColorButton.tintColor = UIColor.black
            RightEyeValue.textColor = UIColor.black
            LeftEyeResult.textColor = UIColor.black
            LeftEyeHeadingLabel.textColor = UIColor.black
            RightEyeHedingLabel.textColor = UIColor.black
            XValueLabel.textColor = UIColor.black
            XHeadingLabel.textColor = UIColor.black
        }
        
    }
    
    // Exit the test
    @IBAction func ExitTestAction(_ sender: Any) {
        UIScreen.main.brightness = br
        NotificationCenter.default.post(name: Notification.Name(rawValue: "MainMenuDefaultCheck"), object: nil)
        self.dismiss(animated: true, completion: nil)
    }

    // Allow rotation
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var shouldAutorotate: Bool {
        return true
    }


}
