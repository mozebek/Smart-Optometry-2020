//
//  OknStripesViewController.swift
//  SmartOptometry2k19
//
//  Created by Matic Ozebek on 30/05/2019.
//  Copyright © 2019 Matic Ozebek. All rights reserved.
//

import UIKit

class OknStripesViewController: UIViewController {

    // Views array
    var views = [UIView]();
    
    // Inital speed
    var moveSpeed: [Double] = [0.2,0.4,0.6,0.8]
    var moveSpeedIndex = 1
    
    // Outlets
    @IBOutlet weak var MiddleDot: UIView!
    @IBOutlet weak var Toolbar: UIStackView!
    
    // Brightness
    var br: CGFloat = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Save current brightnes
        br = UIScreen.main.brightness
        // Check for auto brightness
        if(Helper.app.CheckForBrightness()){
            UIScreen.main.brightness = CGFloat(1)
        }
        
        let screenSize: CGRect = UIScreen.main.bounds
        
        // Create virews
        for index in 0...6 {
            let myView = UIView(frame: CGRect(x: 0, y: CGFloat(index) * screenSize.height/6, width: screenSize.width, height: screenSize.height/12))
            myView.backgroundColor = UIColor.red
            self.view.addSubview(myView)
            views.append(myView)
        }
        
        // Regidter swipe gestures for color change and dot
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.handleGesture(gesture:)))
        swipeLeft.direction = .left
        self.view.addGestureRecognizer(swipeLeft)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.handleGesture(gesture:)))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(self.handleGesture(gesture:)))
        swipeUp.direction = .up
        self.view.addGestureRecognizer(swipeUp)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.handleGesture(gesture:)))
        swipeDown.direction = .down
        self.view.addGestureRecognizer(swipeDown)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.view.addGestureRecognizer(tap)
        
        // Start movement
        StartTimer()
        
        // Midle dot setup
        Helper.app.SetCircle(CView: MiddleDot, Color: UIColor.black)
        MiddleDot.alpha = 0
        self.view.bringSubviewToFront(MiddleDot)
        
        //Bring toolbar to front
        self.view.bringSubviewToFront(Toolbar)
        
    }
    
    
    // Handle swipe geastures for color change and remove or add dot in the middle
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        // Change color
        if gesture.direction == UISwipeGestureRecognizer.Direction.right || gesture.direction == UISwipeGestureRecognizer.Direction.left  {
            for index in 0...views.count-1 {
                if(views[index].backgroundColor == UIColor.red){
                   views[index].backgroundColor = UIColor.black
                    Helper.app.SetCircle(CView: MiddleDot, Color: UIColor.red)
                }
                else{
                    views[index].backgroundColor = UIColor.red;
                    Helper.app.SetCircle(CView: MiddleDot, Color: UIColor.black)
                }
                
            }
        }
        // Show/hide dot
        else if gesture.direction == UISwipeGestureRecognizer.Direction.up || gesture.direction == UISwipeGestureRecognizer.Direction.down {
            if(MiddleDot.alpha == 0){
                MiddleDot.alpha = 1
            }
            else{
                MiddleDot.alpha = 0
            }
        }
    }
    
    // change speed on tap
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        if(moveSpeedIndex < moveSpeed.count-1){
          moveSpeedIndex += 1
        }
        else{
            moveSpeedIndex = 0
        }
       
    }
    
    // Start the timer
    var timer: Timer?
    func StartTimer() {
        timer = Timer.scheduledTimer(timeInterval: 0.002,
                                     target: self,
                                     selector: #selector(eventWith(timer:)),
                                     userInfo: [ "foo" : "bar" ],
                                     repeats: true)
    }
    
    // Timer that moves views
    @objc func eventWith(timer: Timer!) {
        // Move the views
        views[0].center = CGPoint(x: views[0].center.x, y: views[0].center.y + CGFloat(moveSpeed[moveSpeedIndex]))
        views[1].center = CGPoint(x: views[1].center.x, y: views[1].center.y + CGFloat(moveSpeed[moveSpeedIndex]))
        views[2].center = CGPoint(x: views[2].center.x, y: views[2].center.y + CGFloat(moveSpeed[moveSpeedIndex]))
        views[3].center = CGPoint(x: views[3].center.x, y: views[3].center.y + CGFloat(moveSpeed[moveSpeedIndex]))
        views[4].center = CGPoint(x: views[4].center.x, y: views[4].center.y + CGFloat(moveSpeed[moveSpeedIndex]))
        views[5].center = CGPoint(x: views[5].center.x, y: views[5].center.y + CGFloat(moveSpeed[moveSpeedIndex]))
        views[6].center = CGPoint(x: views[5].center.x, y: views[6].center.y + CGFloat(moveSpeed[moveSpeedIndex]))
        
        // Set them to inital position when over screen
        if(views[0].center.y > self.view.frame.size.height + views[0].frame.size.height * 1.5){
            views[0].center = CGPoint(x: views[0].center.x, y: 0 - self.view.frame.size.height/24)
        }
        if(views[1].center.y > self.view.frame.size.height + views[0].frame.size.height * 1.5){
            views[1].center = CGPoint(x: views[1].center.x, y: 0 - self.view.frame.size.height/24)
        }
        if(views[2].center.y > self.view.frame.size.height + views[0].frame.size.height * 1.5){
            views[2].center = CGPoint(x: views[2].center.x, y: 0 - self.view.frame.size.height/24)
        }
        if(views[3].center.y > self.view.frame.size.height + views[0].frame.size.height * 1.5){
            views[3].center = CGPoint(x: views[3].center.x, y: 0 - self.view.frame.size.height/24)
        }
        if(views[4].center.y > self.view.frame.size.height + views[0].frame.size.height * 1.5){
            views[4].center = CGPoint(x: views[4].center.x, y: 0 - self.view.frame.size.height/24)
        }
        if(views[5].center.y > self.view.frame.size.height + views[0].frame.size.height * 1.5){
            views[5].center = CGPoint(x: views[5].center.x, y: 0 - self.view.frame.size.height/24)
        }
        if(views[6].center.y > self.view.frame.size.height + views[0].frame.size.height * 1.5){
            views[6].center = CGPoint(x: views[6].center.x, y: 0 - self.view.frame.size.height/24)
        }
    }
    

    
    // Allow just portraint orientation inside main menu
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    // Hides status bar once test opened
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    // Exit test
    @IBAction func ExitTestAction(_ sender: Any) {
        UIScreen.main.brightness = br
        NotificationCenter.default.post(name: Notification.Name(rawValue: "MainMenuDefaultCheck"), object: nil)
        self.dismiss(animated: true, completion: nil)
    }

}
