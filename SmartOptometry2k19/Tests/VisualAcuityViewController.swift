//
//  VisualAcuityViewController.swift
//  SmartOptometry2k19
//
//  Created by Matic Ozebek on 24/07/2019.
//  Copyright © 2019 Matic Ozebek. All rights reserved.
//

import UIKit

class VisualAcuityViewController: UIViewController {

    
    // Outlets
    @IBOutlet weak var VALabel: UILabel!
    @IBOutlet weak var SizePageControl: UIPageControl!
    @IBOutlet weak var TypePageControl: UIPageControl!
    @IBOutlet weak var SizeIndicatorLabel: UILabel!
    
    // Sizes of C characters array
    var CSizes:[CGFloat] = [64,34,30,24,17,12,11,9,8]
    var ESizes:[CGFloat] = [53,27,24,18,13,10,9,7,6]
    var ASizes:[CGFloat] = [69,36,32,25,19,15,12,9,7]
    
    // Localized strings
    var VAStatements:[String] = ["statmentVA1" ,"statmentVA2","statmentVA3","statmentVA4","statmentVA5","statmentVA6","statmentVA7","statmentVA8","statmentVA9"]
    
    var VASizeIndicator:[String] = ["0.1","0.2","0.25","0.32","0.4","0.5","0.63","0.8","1.0"]
    
    // Size and type of letters
    var VATypeNum = 0;
    var VASyzeNum = 0;
    
    var VASyzeVal: CGFloat = 64;
    
    // Brightness
    var br: CGFloat = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Save current brightnes
        br = UIScreen.main.brightness
        // Check for auto brightness
        if(Helper.app.CheckForBrightness()){
            UIScreen.main.brightness = CGFloat(1)
        }

        
        // Rotate type pager for 90 degrees
        TypePageControl.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi/2))
        
        // Set size label
        SizeIndicatorLabel.text = "0.1"
        
        // Rotate screen to landscape
        let value = UIInterfaceOrientation.landscapeRight.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        
        // Set the defaults
        VALabel.font = UIFont(name:"Opto", size: 20.0)
        VALabel.text = ""
        FillLabelWithContent(fontSize: 50)
        
        // Swipe geastures registration
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.handleGesture(gesture:)))
        swipeLeft.direction = .left
        self.view.addGestureRecognizer(swipeLeft)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.handleGesture(gesture:)))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(self.handleGesture(gesture:)))
        swipeUp.direction = .up
        self.view.addGestureRecognizer(swipeUp)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.handleGesture(gesture:)))
        swipeDown.direction = .down
        self.view.addGestureRecognizer(swipeDown)
        
        
    }
    
    // Method to fill label with C characters
    func FillLabelWithContent(fontSize: CGFloat){
        
        
        VALabel.text = ""
        var VALetters  = "mnop"
        
        // Determine which letters to show C or E
        if(VATypeNum == 0){
            VALetters = "abcdefgh"
        }
        else if(VATypeNum == 1){
            VALetters = "ijkl"
        }
        
        // Show C or E letters or show statement
        if(VATypeNum == 1 || VATypeNum == 0){
            VALabel.font = UIFont(name:"Opto", size: fontSize)
            for index in 0...4{
                
                VALabel.text = VALabel.text! + randomString(length: 1, letters: VALetters)
                if(index<4){
                    VALabel.text = VALabel.text! + " "
                }
                
            }
        }
        else{
            VALabel.font = UIFont.systemFont(ofSize: fontSize)
            let vast = VAStatements[VASyzeNum]
            VALabel.text = vast.localized
        }
        
    }
    
    // Animate label chage
    func ChangeLabel(){
        UIView.animate(withDuration: 0.2, animations: {
            self.VALabel.alpha = 0;
        },completion: {(finished:Bool) in
            self.FillLabelWithContent(fontSize: self.VASyzeVal)
            UIView.animate(withDuration: 0.2, animations: {
                self.VALabel.alpha = 1
            })
        })
    }

    // Get random character
    func randomString(length: Int, letters: String) -> String {
        return String((0..<length).map{ _ in letters.randomElement()!})
    }
    
    
    // Swipe gestures to change character type and size
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        if gesture.direction == UISwipeGestureRecognizer.Direction.left {
            
            if(VASyzeNum < 8){
                VASyzeNum = VASyzeNum + 1
            }
            else{
                VASyzeNum = 0
            }
            // If C E or A
            if(VATypeNum == 0){
                VASyzeVal = CSizes[VASyzeNum]
            } else if(VATypeNum == 1){
                VASyzeVal = ESizes[VASyzeNum]
            }else if(VATypeNum == 2){
                VASyzeVal = ASizes[VASyzeNum]
            }
            SizePageControl.currentPage = VASyzeNum
            SizeIndicatorLabel.text = VASizeIndicator[VASyzeNum]
            ChangeLabel()
            
        }
        else if gesture.direction == UISwipeGestureRecognizer.Direction.right {
            if(VASyzeNum > 0){
                VASyzeNum = VASyzeNum - 1
            }
            else{
                VASyzeNum = 8
            }
            // If C E or A
            if(VATypeNum == 0){
                VASyzeVal = CSizes[VASyzeNum]
            } else if(VATypeNum == 1){
                VASyzeVal = ESizes[VASyzeNum]
            }else if(VATypeNum == 2){
                VASyzeVal = ASizes[VASyzeNum]
            }
            SizePageControl.currentPage = VASyzeNum
            SizeIndicatorLabel.text = VASizeIndicator[VASyzeNum]
            ChangeLabel()
        }
        else if gesture.direction == UISwipeGestureRecognizer.Direction.up {
            if(VATypeNum < 2){
               VATypeNum = VATypeNum + 1
            }
            else{
                VATypeNum = 0
            }
            TypePageControl.currentPage = VATypeNum
            ChangeLabel()
        }
        else if gesture.direction == UISwipeGestureRecognizer.Direction.down {
            if(VATypeNum > 0){
                VATypeNum = VATypeNum - 1
            }
            else{
                VATypeNum = 2
            }
            TypePageControl.currentPage = VATypeNum
            ChangeLabel()
        }
    }

    // Hides status bar once test opened
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    // Allow rotation
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .landscapeRight
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    // Exit the test
    @IBAction func ExitTestAction(_ sender: Any) {
        UIScreen.main.brightness = br
        NotificationCenter.default.post(name: Notification.Name(rawValue: "MainMenuDefaultCheck"), object: nil)
        self.dismiss(animated: true, completion: nil)
    }

}
