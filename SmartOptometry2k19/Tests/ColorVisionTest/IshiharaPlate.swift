//
//  IshiharaPlate.swift
//  SmartOptometry2k19
//
//  Created by Matic Ozebek on 29/05/2019.
//  Copyright © 2019 Matic Ozebek. All rights reserved.
//

import UIKit

public  enum ColorCondition {
    case Deutranopia
    case Portanopia
    case RedAndGreen
}


class IshiharaPlate: NSObject {

    var ImageName: String = ""
    var Result: Int = 0
    var Condition: ColorCondition = ColorCondition.RedAndGreen
    
    init(imageName: String, result: Int, condition: ColorCondition) {
        self.ImageName = imageName
        self.Result = result
        self.Condition = condition
        super.init()
    }
}
