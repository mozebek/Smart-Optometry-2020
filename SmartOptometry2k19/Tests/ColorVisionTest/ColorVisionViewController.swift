//
//  ColorVisionViewController.swift
//  SmartOptometry2k19
//
//  Created by Matic Ozebek on 28/05/2019.
//  Copyright © 2019 Matic Ozebek. All rights reserved.
//

import UIKit

class ColorVisionViewController: UIViewController {

    // Results view
    @IBOutlet weak var ResultsView: UIView!
    @IBOutlet weak var DeficeincyLabel: UILabel!
    @IBOutlet weak var ScoreLabel: UILabel!
    
    // Ok and erase
    @IBOutlet weak var OkButton: UIButton!
    @IBOutlet weak var EraseButton: UIButton!
    // Keyboard
    @IBOutlet weak var Number1: UIButton!
    @IBOutlet weak var Number2: UIButton!
    @IBOutlet weak var Number3: UIButton!
    @IBOutlet weak var Number4: UIButton!
    @IBOutlet weak var Number5: UIButton!
    @IBOutlet weak var Number6: UIButton!
    @IBOutlet weak var Number7: UIButton!
    @IBOutlet weak var Number8: UIButton!
    @IBOutlet weak var Number9: UIButton!
    @IBOutlet weak var Number0: UIButton!
    
    // Input label to present entered numbers
    @IBOutlet weak var InputLabel: UILabel!
    
    @IBOutlet weak var PlateImage: UIImageView!
    
    // Results view elements
    @IBOutlet weak var ResultsHeaderView: UIView!
    @IBOutlet weak var ResultsInterpretationView: UIView!
    @IBOutlet weak var WrongPlateImage: UIImageView!
    @IBOutlet weak var WrongPlateLabel: UILabel!
    @IBOutlet weak var LeftButton: UIButton!
    @IBOutlet weak var RightButton: UIButton!
    @IBOutlet weak var ResultsLabel: UILabel!
    @IBOutlet weak var ColorDeficLabel: UILabel!
    @IBOutlet weak var ScoreHeader: UILabel!
    
    // Array of isihara plates
    var IsiharaPlates = [IshiharaPlate]();
    var WrongAnswers = [IshiharaPlate]();
    var WrongAnswersEntered = [Int]();
    var currentStep = 0
    var correctAnswers = 0
    
    // Deficiency
    var protanopia = false
    var deutranopia = false
    var redAndGreen = false
    
    // Groups of images
    var firstGroup = 0
    var secondGroup = 0
    var thirdGroup = 0
    var fourthGroup = 0
    var fifthGroup = 0
    var sixthGroup = 0
    
    // Wrong answers counter
    var wrongCounter = 0
    
    // Brightness
    var br: CGFloat = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // translates
        ResultsLabel.text = "results".localized
        ScoreHeader.text = "score".localized
        ColorDeficLabel.text = "color_deficiency".localized
        

        // Save current brightnes
        br = UIScreen.main.brightness
        // Check for auto brightness
        if(Helper.app.CheckForBrightness()){
            UIScreen.main.brightness = CGFloat(1)
        }
        
        // Set orientation to portrait
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        // Setup test on init
        SetUpTest()
    }
    
    // Set up test
    func SetUpTest(){
        
        
        
        // Hide wrong answres
        WrongPlateImage.alpha = 0
        LeftButton.alpha = 0
        RightButton.alpha = 0
        WrongPlateLabel.alpha = 0
        
        // Hide results
        ResultsView.alpha = 0;
        
        // Set results interpretation rounded corners
        ResultsInterpretationView.layer.cornerRadius = 30
        Helper.app.addGradientToView(view: ResultsHeaderView,
                                     color1: UIColor.init(displayP3Red: 25/255, green: 154/255, blue: 143/255, alpha: 1),
                                     color2: UIColor.init(displayP3Red: 35/255, green: 188/255, blue: 121/255, alpha: 1))
        
        // Set icons in buttons to fit
        OkButton.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        OkButton.imageView?.contentMode = .scaleAspectFit
        EraseButton.imageView?.contentMode = .scaleAspectFit
        
        // Set input label to become empty
        InputLabel.text = ""
        
        // Add tags to buttons and apply target
        Number1.tag = 1
        Number1.addTarget(self,action:#selector(KeyboardPressed(sender:)),for:.touchUpInside)
        Number2.tag = 2
        Number2.addTarget(self,action:#selector(KeyboardPressed(sender:)),for:.touchUpInside)
        Number3.tag = 3
        Number3.addTarget(self,action:#selector(KeyboardPressed(sender:)),for:.touchUpInside)
        Number4.tag = 4
        Number4.addTarget(self,action:#selector(KeyboardPressed(sender:)),for:.touchUpInside)
        Number5.tag = 5
        Number5.addTarget(self,action:#selector(KeyboardPressed(sender:)),for:.touchUpInside)
        Number6.tag = 6
        Number6.addTarget(self,action:#selector(KeyboardPressed(sender:)),for:.touchUpInside)
        Number7.tag = 7
        Number7.addTarget(self,action:#selector(KeyboardPressed(sender:)),for:.touchUpInside)
        Number8.tag = 8
        Number8.addTarget(self,action:#selector(KeyboardPressed(sender:)),for:.touchUpInside)
        Number9.tag = 9
        Number9.addTarget(self,action:#selector(KeyboardPressed(sender:)),for:.touchUpInside)
        Number0.tag = 0
        Number0.addTarget(self,action:#selector(KeyboardPressed(sender:)),for:.touchUpInside)
        OkButton.tag = 10
        OkButton.addTarget(self,action:#selector(KeyboardPressed(sender:)),for:.touchUpInside)
        EraseButton.tag = 11
        EraseButton.addTarget(self,action:#selector(KeyboardPressed(sender:)),for:.touchUpInside)
        
        // Set plates to array
        let plate1 = IshiharaPlate(imageName: "Plate1", result: 8, condition: ColorCondition.RedAndGreen)
        let plate2 = IshiharaPlate(imageName: "Plate2", result: 6, condition: ColorCondition.RedAndGreen)
        let plate3 = IshiharaPlate(imageName: "Plate3", result: 5, condition: ColorCondition.RedAndGreen)
        let plate4 = IshiharaPlate(imageName: "Plate4", result: 7, condition: ColorCondition.RedAndGreen)
        let plate5 = IshiharaPlate(imageName: "Plate5", result: 74, condition: ColorCondition.RedAndGreen)
        let plate6 = IshiharaPlate(imageName: "Plate6", result: 5, condition: ColorCondition.RedAndGreen)
        let plate7 = IshiharaPlate(imageName: "Plate7", result: 96, condition: ColorCondition.RedAndGreen)
        let plate8 = IshiharaPlate(imageName: "Plate8", result: 42, condition: ColorCondition.RedAndGreen)
        let plate9 = IshiharaPlate(imageName: "Plate9", result: 12, condition: ColorCondition.RedAndGreen)
        let plate10 = IshiharaPlate(imageName: "Plate10", result: 2, condition: ColorCondition.RedAndGreen)
        let plate11 = IshiharaPlate(imageName: "Plate11", result: 6, condition: ColorCondition.RedAndGreen)
        let plate12 = IshiharaPlate(imageName: "Plate12", result: 45, condition: ColorCondition.RedAndGreen)
        let plate13 = IshiharaPlate(imageName: "Plate13", result: 29, condition: ColorCondition.RedAndGreen)
        
        IsiharaPlates += [plate1, plate2, plate3, plate4, plate5, plate6, plate7, plate8, plate9, plate10, plate11, plate12, plate13].shuffled()
        
        // Set the first plate
        let image: UIImage = UIImage(named: IsiharaPlates[0].ImageName)!
        PlateImage.image = image

    }
    
    
    
    // Hides status bar once test opened
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    ///////////////////////  Tipkovnica
    @objc func KeyboardPressed(sender: UIButton!){
        
        // Allow just 3 characters on input and append on keybaerd typing
        if(InputLabel.text?.count ?? 0 < 3){
            if (sender.tag == 1){
                InputLabel.text?.append("1")
            }
            if (sender.tag == 2){
                InputLabel.text?.append("2")
            }
            if (sender.tag == 3){
                InputLabel.text?.append("3")
            }
            if (sender.tag == 4){
                InputLabel.text?.append("4")
            }
            if (sender.tag == 5){
                InputLabel.text?.append("5")
            }
            if (sender.tag == 6){
                InputLabel.text?.append("6")
            }
            if (sender.tag == 7){
                InputLabel.text?.append("7")
            }
            if (sender.tag == 8){
                InputLabel.text?.append("8")
            }
            if (sender.tag == 9){
                InputLabel.text?.append("9")
            }
            if (sender.tag == 0){
                InputLabel.text?.append("0")
            }
        }
        
        // Forward and erase
        if (sender.tag == 10){
            MoveForward()
        }
        if (sender.tag == 11){
            InputLabel.text = ""
        }
        
    }
    
    // Present next image and check the answer
    func MoveForward(){
        
        // Get numberr from input
        let input: Int = Int(InputLabel.text!) ?? 0
        
        // Check if answer is correct
        if(input == IsiharaPlates[currentStep].Result && input != 0){
            correctAnswers += 1
        }
        else{
            // Add wrong answer to array
            WrongAnswers.append(IsiharaPlates[currentStep])
            WrongAnswersEntered.append(input)
            
            // Increase group fail counter
            if(IsiharaPlates[currentStep].ImageName == "Plate1" || IsiharaPlates[currentStep].ImageName == "Plate2"){
                firstGroup += 1
            }
            if(IsiharaPlates[currentStep].ImageName == "Plate3" || IsiharaPlates[currentStep].ImageName == "Plate4"){
                secondGroup += 1
            }
            if(IsiharaPlates[currentStep].ImageName == "Plate5" || IsiharaPlates[currentStep].ImageName == "Plate6"){
                thirdGroup += 1
            }
            if(IsiharaPlates[currentStep].ImageName == "Plate10" || IsiharaPlates[currentStep].ImageName == "Plate11"){
                fifthGroup += 1
            }
            if(IsiharaPlates[currentStep].ImageName == "Plate12" || IsiharaPlates[currentStep].ImageName == "Plate13"){
                sixthGroup += 1
            }
            
            if(IsiharaPlates[currentStep].ImageName == "Plate7"){
                if(input == 6){
                   protanopia = true
                }
                else if (input == 9){
                   deutranopia = true
                }
                else{
                   fourthGroup += 1
                }
            }
            else if(IsiharaPlates[currentStep].ImageName == "Plate8"){
                if(input == 2){
                    protanopia = true
                }
                else if (input == 4){
                    deutranopia = true
                }
                else{
                  fourthGroup += 1
                }
            }
            print("Answer is wrong!")
        }
        
        // Until the end of array of plates
        if(currentStep < IsiharaPlates.count-1){
        
            // Increase counter
            currentStep += 1
            
            // Change Image
            UIView.animate(withDuration: 0.2, animations: {
                self.PlateImage.alpha = 0;
            },completion: {(finished:Bool) in
                
                let image: UIImage = UIImage(named: self.IsiharaPlates[self.currentStep].ImageName)!
                self.PlateImage.image = image
                
                UIView.animate(withDuration: 0.2, animations: {
                    self.PlateImage.alpha = 1
                },completion: {(finished:Bool) in
                })
            })
            
            // Remove text from label
            InputLabel.text = ""
        }
        else{
            // Apply score to results label
            ScoreLabel.text = String(describing: self.correctAnswers) + "/13"
            // Interpretate results
            if(self.correctAnswers == 13){
                DeficeincyLabel.text = "no_colour_deficiency".localized
            }
            else{
                // If there were 2 or more mistakes
                if(firstGroup == 2 || secondGroup == 2 || thirdGroup == 2 || fourthGroup == 2 || fifthGroup == 2 || sixthGroup == 2)
                {
                    redAndGreen = true;
                }
                
                var result: String = ""
                if(redAndGreen)
                {
                    result = result + "red_green".localized + " | "
                }
                if(protanopia){
                    result = result + "protanopia".localized + " | "
                }
                if(deutranopia){
                    result = result + "deuteranopia".localized
                }
                
                DeficeincyLabel.text = result
            }
            
            // Show wrong palte if some answer was wrong
            if(correctAnswers < 13){
                WrongPlateImage.alpha = 1
                LeftButton.alpha = 0
                RightButton.alpha = 1
                WrongPlateLabel.alpha = 1
                
                // Set the first wrong plate
                let image: UIImage = UIImage(named: WrongAnswers[0].ImageName)!
                WrongPlateImage.image = image
                
                // Set first wrong answer label
                var result = "your_first_upper".localized + " - "
                result = result + String(WrongAnswersEntered[0])
                result = result + " | "
                result = result + String(WrongAnswers[0].Result)
                result = result + " - " + "correct_answer_first_upper".localized
                WrongPlateLabel.text = result
                
                
            }
            // Show results view
            UIView.animate(withDuration: 0.2, animations: {
                self.ResultsView.alpha = 1;
            },completion: {(finished:Bool) in
                
            })
        }
        
        
    }
    
    func RestartTest(){
        
        // Hide wrong answres
        WrongPlateImage.alpha = 0
        LeftButton.alpha = 0
        RightButton.alpha = 0
        WrongPlateLabel.alpha = 0
        
        // Reset the group fails
        firstGroup = 0;
        secondGroup = 0;
        thirdGroup = 0;
        fourthGroup = 0;
        fifthGroup = 0;
        sixthGroup = 0;
        
        // Empty array
        WrongAnswers.removeAll()
        WrongAnswersEntered.removeAll()
        
        // Shuffle the array of plates
        IsiharaPlates.shuffle()
        
        // Reset counters
        currentStep = 0
        correctAnswers = 0
        
        // Reset input label
        InputLabel.text = ""
        
        // Set the first plate
        let image: UIImage = UIImage(named: IsiharaPlates[0].ImageName)!
        PlateImage.image = image
        
        // Show test view
        UIView.animate(withDuration: 0.2, animations: {
            self.ResultsView.alpha = 0;
        })
    }
    
    func ShowWrongAnswer(index: Int){
        // Set the first wrong plate
        let image: UIImage = UIImage(named: WrongAnswers[index].ImageName)!
        WrongPlateImage.image = image
        
        // Set first wrong answer label
        var result = "your_first_upper".localized + " - "
        result = result + String(WrongAnswersEntered[index])
        result = result + " | "
        result = result + String(WrongAnswers[index].Result)
        result = result + " - " + "correct_answer_first_upper".localized
        WrongPlateLabel.text = result
    }
    
    // -----------------> Button Actions
    
    // Exit the test
    @IBAction func ExitTestAction(_ sender: Any) {
        UIScreen.main.brightness = br
        NotificationCenter.default.post(name: Notification.Name(rawValue: "MainMenuDefaultCheck"), object: nil)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func PrevWrong(_ sender: Any) {
        if(wrongCounter > 0){
            wrongCounter -= 1
            ShowWrongAnswer(index: wrongCounter)
        }
        if(wrongCounter == 0){
            LeftButton.alpha = 0
        }
        if(wrongCounter <= WrongAnswersEntered.count-2){
            RightButton.alpha = 1
        }
        
    }
    
    @IBAction func NextWrong(_ sender: Any) {
        if(wrongCounter < WrongAnswersEntered.count-1){
            wrongCounter += 1
            ShowWrongAnswer(index: wrongCounter)
        }
        if(wrongCounter > 0){
            LeftButton.alpha = 1
        }
        if(wrongCounter == WrongAnswersEntered.count-1){
            RightButton.alpha = 0
        }
    }
    
    @IBAction func RepeatTest(_ sender: Any) {
        RestartTest()
    }
    
    // Allow rotation
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
}
