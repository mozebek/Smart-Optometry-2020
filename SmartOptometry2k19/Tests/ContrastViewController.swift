//
//  ContrastViewController.swift
//  SmartOptometry2k19
//
//  Created by Matic Ozebek on 10/07/2019.
//  Copyright © 2019 Matic Ozebek. All rights reserved.
//

import UIKit

class ContrastViewController: UIViewController {
    
    @IBOutlet weak var ToolbarView: UIStackView!
    
    // Results view
    @IBOutlet weak var ResultsView: UIView!
    @IBOutlet weak var ResultCharacter: UILabel!
    @IBOutlet weak var ResultValue: UILabel!
    @IBOutlet weak var ResultsHeaderView: UIView! // Uper view of results
    @IBOutlet weak var ResultsInterpretationView: UIView! // Lower view of results
    @IBOutlet weak var ContrastValueLabel: UILabel!
    @IBOutlet weak var ResultsLabel: UILabel!
    
    // Brightness
    var br: CGFloat = 1
    
    @IBOutlet weak var LabelsView: UIView!
    var valuesTable = [[Float]](repeating: [Float](repeating: 0, count: 3), count: 48)
    var charactersTable = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Save current brightnes
        br = UIScreen.main.brightness
        UIScreen.main.brightness = CGFloat(1)
        
        // Set orientation to portrait
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        
        // Set transaltes
        ResultsLabel.text = "results".localized
        ContrastValueLabel.text = "contrast_value".localized
    
        // Get values for alpha value of characters
        getAlphaValues(objectsNumber: 48)
        // Fill the screen
        FillWithCharacters()
        

        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        FillWithCharacters()
        
        // Set results interpretation rounded corners
        ResultsInterpretationView.layer.cornerRadius = 30
        Helper.app.addGradientToView(view: ResultsHeaderView,
                                     color1: UIColor.init(displayP3Red: 25/255, green: 154/255, blue: 143/255, alpha: 1),
                                     color2: UIColor.init(displayP3Red: 35/255, green: 188/255, blue: 121/255, alpha: 1))        // Set results interpretation rounded corners
        ResultsInterpretationView.layer.cornerRadius = 30
        Helper.app.addGradientToView(view: ResultsHeaderView,
                                     color1: UIColor.init(displayP3Red: 25/255, green: 154/255, blue: 143/255, alpha: 1),
                                     color2: UIColor.init(displayP3Red: 35/255, green: 188/255, blue: 121/255, alpha: 1))
    }
    
    // Fill the screen
    func FillWithCharacters(){
        
        // Empty characters if already filled
        charactersTable.removeAll()
        for view in LabelsView.subviews{
            view.removeFromSuperview()
        }
        
        // Hide results view and toolbar if opened and show labels
        UIView.animate(withDuration: 0.2, animations: {
            self.ResultsView.alpha = 0;
            self.ToolbarView.alpha = 0;
            self.LabelsView.alpha = 1;
        })
        
        // Get width and height for each label 8x6
        let labelWidth: Int = Int(LabelsView.frame.width/6)
        let labelHeight: Int = Int(LabelsView.frame.height/8)
        
        // Initial positions
        var posX = 0
        var posY = 0
        
        var labelTag = 0
        
        // Fill the labels
        for _ in 1...8 {
            
            for _ in 1...6 {
                let lbl = UILabel(frame: CGRect(x: posX, y: posY, width: labelWidth, height: labelHeight))
                lbl.textAlignment = .center
                lbl.baselineAdjustment = .alignCenters
                lbl.text = randomString(length: 1)
                lbl.textColor = UIColor.init(red: CGFloat(valuesTable[labelTag][0]/255), green: CGFloat(valuesTable[labelTag][0]/255), blue: CGFloat(valuesTable[labelTag][0]/255), alpha: CGFloat(valuesTable[labelTag][1]/255))
                
                // Set font size depending on device
                let deviceIdiom = UIScreen.main.traitCollection.userInterfaceIdiom
                if(deviceIdiom == .pad){
                  lbl.font = UIFont.systemFont(ofSize: 120)
                }
                else{
                    lbl.font = UIFont.systemFont(ofSize: 60)
                }
                
                lbl.isUserInteractionEnabled = true
                lbl.adjustsFontSizeToFitWidth = true
                lbl.minimumScaleFactor = 0.5
                lbl.tag = labelTag
                self.LabelsView.addSubview(lbl)
                charactersTable.append(lbl.text ?? "A")
                
                // Tap geasture init
                let tapEvent = UITapGestureRecognizer(target: self, action: #selector(tappGeastureAction(sender:)))
                lbl.addGestureRecognizer(tapEvent)

                
                // Move location x
                posX = posX + labelWidth
                
                // Label tag increase
                labelTag = labelTag+1
            }
            
            // Set location
            posX = 0;
            posY = posY + labelHeight;
        }

        
    }
    
    // Get random character
    func randomString(length: Int) -> String {
        let letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        return String((0..<length).map{ _ in letters.randomElement()! })
    }
    
    // Set contrast values by Blaz Grah
    func getAlphaValues(objectsNumber: Int)
    {
        var color: Int
        var alpha: Int
        let rr: Float = Float(objectsNumber)
        var contrast: Float
        
        // Color parameters
        let max: Float = 255.0   // Max value for alpha and color
        let coef3: Float = 4.0   // Color reduction cooeficient
        let coef2: Float = 2.0
        let coef1: Float = 1.0
        
        // Space points whit some color reduction in coordinates
        let pt3: [Float] = [rr/2.0, rr/2.0]                    // Last section starting point - coef3
        let pt2: [Float] = [rr/8.0, (max-pt3[1])/3.0 + pt3[1]];  // Middle section starting point - coef2
        let pt1: [Float] = [0.0, max];                            // First section starting point - coef1
        
        // Linear function coeficients
        let k3: Float = -(pt3[1]/pt3[0]);
        let k2: Float = -(pt2[1] - pt3[1]/(coef3/coef2))/(pt3[0] - pt2[0]);
        let k1: Float = -(pt1[1] - pt2[1]/(coef2/coef1))/(pt2[0] - pt1[0]);

        for index in 0...objectsNumber-1 {
            if(index >= objectsNumber/2){       // Last segment
                color = Int(max) - Int(max)/Int(coef3);
                alpha = Int(k3)*index+Int(rr);
                contrast = Float(255-color)/max * Float(alpha)/max * 100.0
            }
            else if(index > objectsNumber/8){   // Middle segment
                color = Int(max) - Int(max)/Int(coef2);
                alpha = (Int(k2)*(index-Int(pt3[0]))) + (Int(pt3[1])/(Int(coef3)/Int(coef2)))    // Fit function between 2 points
                contrast = Float(255-color)/max * Float(alpha)/max * 100.0
            }
            else{               // First segment
                color = Int(max) - Int(max)/Int(coef1)
                alpha = Int(k1)*index+Int(max)
                contrast = Float(255-color)/max * Float(alpha)/max * 100.0
            }
            
            valuesTable[index][0] = Float(color)   // Store values to output buffer
            valuesTable[index][1] = Float(alpha)
            valuesTable[index][2] = contrast
        }
    }
    
    // Get object tapped
    @objc func tappGeastureAction(sender: AnyObject) {
        
        ResultCharacter.text = charactersTable[sender.view.tag]
 
        // Set result label
        ResultValue.text = String(format: "%.2f", valuesTable[sender.view.tag][2])
       
        // Show results view and
        UIView.animate(withDuration: 0.2, animations: {
            self.ResultsView.alpha = 1;
            self.ToolbarView.alpha = 1;
            self.LabelsView.alpha = 0;
        })
    }
    
    // -----------------> Button Actions
    
    // Exit the test
    @IBAction func ExitTestAction(_ sender: Any) {
        UIScreen.main.brightness = br
        NotificationCenter.default.post(name: Notification.Name(rawValue: "MainMenuDefaultCheck"), object: nil)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func RepeatTestAction(_ sender: Any) {
        FillWithCharacters()
    }
    
    // Allow rotation
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    // Hides status bar once test opened
    override var prefersStatusBarHidden: Bool {
        return true
    }

}



