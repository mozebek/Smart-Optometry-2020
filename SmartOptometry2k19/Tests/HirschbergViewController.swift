//
//  HirschbergViewController.swift
//  SmartOptometry2k19
//
//  Created by Matic Ozebek on 22/08/2019.
//  Copyright © 2019 Matic Ozebek. All rights reserved.
//

import UIKit
import AVKit

class HirschbergViewController: UIViewController {
    @IBOutlet weak var LeftSideView: UIView!
    @IBOutlet weak var LeftSideEyePlaceholder: UIView!
    @IBOutlet weak var Eye1Part1: UIView!
    @IBOutlet weak var Eye1Part2: UIView!
    @IBOutlet weak var Pupil1: UIView!
    @IBOutlet weak var Eye1Label: UILabel!
    @IBOutlet weak var Eye1ResultLabel: UILabel!
    
    @IBOutlet weak var RightSideView: UIView!
    @IBOutlet weak var RightSideEyePlaceholder: UIView!
    @IBOutlet weak var Eye2Part1: UIView!
    @IBOutlet weak var Eye2Part2: UIView!
    @IBOutlet weak var Pupil2: UIView!
    @IBOutlet weak var Eye2Label: UILabel!
    @IBOutlet weak var Eye2ResultLabel: UILabel!
    
    @IBOutlet weak var Eye1HorizontalConstraint: NSLayoutConstraint!
    @IBOutlet weak var Eye1VerticalConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var Eye2HorizontalConstraint: NSLayoutConstraint!
    @IBOutlet weak var Eye2VerticalConstraint: NSLayoutConstraint!
    
    var LorR: Bool = false
    
    var x: CGFloat = 0
    var y: CGFloat = 0
    
    var x2: CGFloat = 0
    var y2: CGFloat = 0
    
    var Eye1Angle: Int = 0
    var deltaX1: CGFloat = 0
    var deltaY1: CGFloat = 0
    
    var Eye2Angle: Int = 0
    var deltaX2: CGFloat = 0
    var deltaY2: CGFloat = 0
    
    // Brightness
    var br: CGFloat = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Save current brightnes
        br = UIScreen.main.brightness
        // Check for auto brightness
        if(Helper.app.CheckForBrightness()){
            UIScreen.main.brightness = CGFloat(1)
        }
        
        // Set orientation to portrait
        let value = UIInterfaceOrientation.landscapeRight.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
    
    }

    override func viewDidAppear(_ animated: Bool) {
        
        toggleTorch(on: true)

        // Set first eye
        Eye1Part1.layer.cornerRadius = Eye1Part1.frame.height/2
        Eye1Part2.layer.cornerRadius = Eye1Part2.frame.height/2
        Pupil1.layer.cornerRadius = Pupil1.frame.height/2
        Helper.app.SetBorder(CView: Eye1Part1, borderWidth: 5, borderColor: UIColor.darkGray)
        Helper.app.SetBorder(CView: Pupil1, borderWidth: 1, borderColor: UIColor.black)
        Eye1Part1.clipsToBounds = true
        Eye1Part2.clipsToBounds = true
        Pupil1.clipsToBounds = true
        
        // Set second eye
        Eye2Part1.layer.cornerRadius = Eye2Part1.frame.height/2
        Eye2Part2.layer.cornerRadius = Eye2Part2.frame.height/2
        Pupil2.layer.cornerRadius = Pupil2.frame.height/2
        Helper.app.SetBorder(CView: Eye2Part1, borderWidth: 5, borderColor: UIColor.darkGray)
        Helper.app.SetBorder(CView: Pupil2, borderWidth: 1, borderColor: UIColor.black)
        Eye2Part1.clipsToBounds = true
        Eye2Part2.clipsToBounds = true
        Pupil2.clipsToBounds = true
        
        // Set translates
        Eye1ResultLabel.text = "no_deviation".localized
        Eye2ResultLabel.text = "no_deviation".localized
        Eye2Label.text = "left_eye".localized.uppercased()
        Eye1Label.text = "right_eye".localized.uppercased()
    }
    
    // Open flashlight
    func toggleTorch(on: Bool) {
        guard let device = AVCaptureDevice.default(for: AVMediaType.video)
            else {return}
        
        if device.hasTorch {
            do {
                try device.lockForConfiguration()
                
                if on == true {
                    device.torchMode = .on
                } else {
                    device.torchMode = .off
                }
                
                device.unlockForConfiguration()
            } catch {
                print("Torch could not be used")
            }
        } else {
            print("Torch is not available")
        }
    }
    
    
    // Touch controls
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first!
        let location = touch.location(in: self.view)
        
        
        
        if(LeftSideView.frame.contains(location)){
           x = Pupil1.center.x - location.x
            y = Pupil1.center.y - location.y
            LorR = false;
        }
        
        if(RightSideView.frame.contains(location)){
            x2 = Pupil2.center.x - location.x
            y2 = Pupil2.center.y - location.y
            LorR = true;
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first!
        let location = touch.location(in: self.view)
        
        // eye 1
        if(LeftSideView.frame.contains(location) && !LorR){
            
            Pupil1.center = CGPoint(x: location.x + x, y: location.y + y)
            deltaY1 = Pupil1.center.y -  Eye1Part1.center.y
            deltaX1 = Pupil1.center.x -  Eye1Part1.center.x
            Eye1Angle =  getAngle(deltaX: Float(deltaX1), deltaY: Float(deltaY1)) //računanje kota
        }

        if(!LeftSideView.frame.contains(Pupil1.center) && !LorR){
           // Pupil1.center = CGPoint(x: LeftSideEyePlaceholder.center.x, y: LeftSideEyePlaceholder.center.y)
            x = Pupil1.center.x - location.x
            y = Pupil1.center.y - location.y
        }
        
        // eye 2
        if(RightSideView.frame.contains(location) && LorR){
            Pupil2.center = CGPoint(x: location.x + x2, y: location.y + y2)
            deltaY2 = Pupil2.center.y - Eye2Part1.center.y;
            deltaX2 = Pupil2.center.x - Eye2Part1.center.x;
            Eye2Angle =  getAngle(deltaX: Float(deltaX2), deltaY: Float(deltaY2)) //računanje kota
        }
        
        if(!RightSideView.frame.contains(Pupil2.center) && LorR){
           //Pupil2.center = CGPoint(x: Eye2Part1.center.x, y: Eye2Part1.center.y)
            x2 = Pupil2.center.x - location.x
            y2 = Pupil2.center.y - location.y
        }

    }
    
     override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        // Update constraints
        Eye1HorizontalConstraint.constant = (Pupil1.center.x - Eye1Part1.center.x)
        Eye1VerticalConstraint.constant = (Pupil1.center.y - Eye2Part1.center.y)
        Eye2HorizontalConstraint.constant = (Pupil2.center.x - Eye2Part1.center.x)
        Eye2VerticalConstraint.constant = (Pupil2.center.y - Eye2Part1.center.y)
        
        var p1 = LeftSideEyePlaceholder.convert(Pupil1.frame, to: self.view)
        var p2 = RightSideEyePlaceholder.convert(Pupil2.frame, to: self.view)
        

        if(!LeftSideView.frame.contains(p1)){
           
             Eye1HorizontalConstraint.constant = 0
             Eye1VerticalConstraint.constant = 0
             Pupil1.center = Eye1Part1.center
        }
        
        if(!RightSideView.frame.contains(p2)){
            
            Eye2HorizontalConstraint.constant = 0
            Eye2VerticalConstraint.constant = 0
            Pupil2.center = Eye2Part1.center
        }
        
        
        self.view.layoutIfNeeded()
        
        if(distance(Pupil1.center, Eye1Part1.center) < 5){
            Eye1ResultLabel.text = "no_deviation".localized
        }
        else if(Eye1Angle >= 260 && Eye1Angle <= 280)
        {
           Eye1ResultLabel.text = "hypotropia".localized
        }
        else if(Eye1Angle >= 80 && Eye1Angle <= 100)
        {
            Eye1ResultLabel.text = "hypertropia".localized
        }
        else if(Eye1Angle >= 350 || Eye1Angle <= 10)
        {
            Eye1ResultLabel.text = "exotropia".localized
        }
            
        else if(Eye1Angle >= 170 && Eye1Angle <= 190)
        {
           Eye1ResultLabel.text = "esotropia".localized
        }
        else if(Eye1Angle > 183 && Eye1Angle < 267)
        {
            Eye1ResultLabel.text = "hypo_and_eso".localized
        }
        else if(Eye1Angle > 273 && Eye1Angle < 357)
        {
            Eye1ResultLabel.text = "hypo_and_exo".localized
        }
        else if(Eye1Angle > 3 && Eye1Angle < 87)
        {
            Eye1ResultLabel.text = "hyper_and_exo".localized
        }
        else if(Eye1Angle > 93 && Eye1Angle < 177)
        {
            Eye1ResultLabel.text = "hyper_and_eso".localized
        }
        

        if(distance(Pupil2.center, Eye1Part1.center) < 5){
            Eye2ResultLabel.text = "no_deviation".localized
        }
        else if(Eye2Angle >= 260 && Eye2Angle <= 280)
        {
            Eye2ResultLabel.text = "hypotropia".localized
        }
        else if(Eye2Angle >= 80 && Eye2Angle <= 100)
        {
            Eye2ResultLabel.text = "hypertropia".localized
        }
        else if(Eye2Angle >= 350 || Eye2Angle <= 10)
        {
            Eye2ResultLabel.text = "esotropia".localized
        }
            
        else if(Eye2Angle >= 170 && Eye2Angle <= 190)
        {
            Eye2ResultLabel.text = "exotropia".localized
        }
        else if(Eye2Angle > 183 && Eye2Angle < 267)
        {
            Eye2ResultLabel.text = "hypo_and_exo".localized
        }
        else if(Eye2Angle > 273 && Eye2Angle < 357)
        {
            Eye2ResultLabel.text = "hypo_and_eso".localized
        }
        else if(Eye2Angle > 3 && Eye2Angle < 87)
        {
            Eye2ResultLabel.text = "hyper_and_eso".localized
        }
        else if(Eye2Angle > 93 && Eye2Angle < 177)
        {
            Eye2ResultLabel.text = "hyper_and_exo".localized
        }
        
        print(Pupil2.center)
        
        
        func distance(_ a: CGPoint, _ b: CGPoint) -> CGFloat {
            let xDist = a.x - b.x
            let yDist = a.y - b.y
            return CGFloat(sqrt(xDist * xDist + yDist * yDist))
        }
        
        
    }
    
    // Calculate the angle
    func getAngle(deltaX: Float, deltaY: Float) -> Int{
        var angle = atan2f(deltaY, deltaX) * 180 / 3.14
        angle = (angle > 0.0 ? angle : (360.0 + angle))
        return Int(angle)
    }
    
    func distance(_ a: CGPoint, _ b: CGPoint) -> CGFloat {
        let xDist = a.x - b.x
        let yDist = a.y - b.y
       // print(CGFloat(sqrt(xDist * xDist + yDist * yDist)))
        return CGFloat(sqrt(xDist * xDist + yDist * yDist))
    }

    
    
    // Exit the test
    @IBAction func ExitTestAction(_ sender: Any) {
        UIScreen.main.brightness = br
        NotificationCenter.default.post(name: Notification.Name(rawValue: "MainMenuDefaultCheck"), object: nil)
        toggleTorch(on: false)
        self.dismiss(animated: true, completion: nil)
    }
    
    // Allow rotation
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .landscapeRight
    }
    
    // Hide top bar
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var shouldAutorotate: Bool {
        return true
    }

}
