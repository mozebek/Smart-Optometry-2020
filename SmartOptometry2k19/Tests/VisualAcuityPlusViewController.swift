//
//  VisualAcuityPlusViewController.swift
//  SmartOptometry2k19
//
//  Created by Matic Ozebek on 03/10/2019.
//  Copyright © 2019 Matic Ozebek. All rights reserved.
//

import UIKit

class VisualAcuityPlusViewController: UIViewController {

    // VA labels
    @IBOutlet weak var Label1: UILabel!
    @IBOutlet weak var Label2: UILabel!
    @IBOutlet weak var Label3: UILabel!
    @IBOutlet weak var Label4: UILabel!
    @IBOutlet weak var Label5: UILabel!
    var labelsArray = [UILabel]()
    
    // Elements
    @IBOutlet weak var MarkerView: UIView! // Marker to see where are you positioned
    @IBOutlet weak var OptotypesView: UIView! // Where optotypes are presented
    @IBOutlet weak var swipeView: UIView! // View for geastures
    @IBOutlet weak var OptotypeSelectView: UIView!
    @IBOutlet weak var ResultsView: UIView! // Results container
    @IBOutlet weak var ResultsHeaderView: UIView! // Uper view of results
    @IBOutlet weak var ResultsInterpretationView: UIView! // Lower view of results
    @IBOutlet weak var EButton: UIButton! // Button to select E
    @IBOutlet weak var CBuutton: UIButton! // Button to select C
    @IBOutlet weak var VisusLabel: UILabel! // Label displaying final result
    @IBOutlet weak var ResultsLabel: UILabel! // Results label
    @IBOutlet weak var VAValueLabel: UILabel! // Results label
    @IBOutlet weak var SwipeAreaLabel: UILabel! // Swipe area label
    @IBOutlet weak var SelectOptotypeLabel: UILabel! // Swipe area label
    
    var swipeAllowed:Bool = false // True when test is running false if in menu
    var VATypeNum:Int = 0 // Type 0 C, 1 E
    
    // Sizes of optotypes characters array
    var CSizes:[CGFloat] = [64,34,30,24,17,12,11,9,8]
    var ESizes:[CGFloat] = [53,27,24,18,13,10,9,7,6]
    var VAValues:[String] = ["0.1","0.2","0.25","0.32","0.4","0.5","0.63","0.8","1.0"]

    var charactersArray = [String]()
    var currentStep:Int = 0
    var currentStepFontSize:CGFloat = 64
    var currentSectionVisus:Int = 0
    var swipedCharacter:String = String()
    var wrongCounter:Int = 0
    
    var swipeStart:CGPoint = CGPoint()
    
    // Brightness
    var br: CGFloat = 1
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Save current brightnes
        br = UIScreen.main.brightness
        // Check for auto brightness
        if(Helper.app.CheckForBrightness()){
            UIScreen.main.brightness = CGFloat(1)
        }
        
        
        // Set orientation to portrait
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        
        // Set transaltes
        ResultsLabel.text = "results".localized
        VAValueLabel.text = "va_value".localized
        VAValueLabel.text = "va_value".localized
        SwipeAreaLabel.text = "swipe_area".localized
        SelectOptotypeLabel.text = "select_optotype".localized.uppercased()
        
        // Set buttons corner radius
        EButton.layer.cornerRadius = 5
        CBuutton.layer.cornerRadius = 5
        
        
        
        // Hide results view
        ResultsView.alpha = 0
    }
    
    override func viewDidAppear(_ animated: Bool) {
       
        // Set labels positions on screen
        SetUpLabels()
        // Set results interpretation rounded corners
        ResultsInterpretationView.layer.cornerRadius = 30
        Helper.app.addGradientToView(view: ResultsHeaderView,
                                     color1: UIColor.init(displayP3Red: 25/255, green: 154/255, blue: 143/255, alpha: 1),
                                     color2: UIColor.init(displayP3Red: 35/255, green: 188/255, blue: 121/255, alpha: 1))
    }
    
    // Set the positions of labels;
    func SetUpLabels(){
        
        let wid = Label3.frame.size.width + 10
        
        Label3.center = OptotypesView.center
        Label2.center = CGPoint(x: Label3.center.x-wid, y: Label3.center.y)
        Label1.center = CGPoint(x: Label2.center.x-wid, y: Label3.center.y)
        Label4.center = CGPoint(x: Label3.center.x+wid, y: Label3.center.y)
        Label5.center = CGPoint(x: Label4.center.x+wid, y: Label3.center.y)
        MarkerView.center = CGPoint(x: Label1.center.x, y: Label1.center.y + wid/2 + 5)
        
    }
    
    // Method to fill label with C characters
    func FillLabelWithContent(){
        
        Label1.font = UIFont(name:"Opto", size: currentStepFontSize)
        Label2.font = UIFont(name:"Opto", size: currentStepFontSize)
        Label3.font = UIFont(name:"Opto", size: currentStepFontSize)
        Label4.font = UIFont(name:"Opto", size: currentStepFontSize)
        Label5.font = UIFont(name:"Opto", size: currentStepFontSize)
    
        var VALetters  = "abcdefgh"
        
        // Determine which letters to show C or E
        if(VATypeNum == 0){
            VALetters = "abcdefgh"
        }
        else if(VATypeNum == 1){
            VALetters = "ijkl"
        }
        
        charactersArray.removeAll()
        // Show C or E letters or show statement
        for _ in 0...5{
            charactersArray.append(randomString(length: 1, letters: VALetters))
        }
        
        Label1.text = charactersArray[0]
        Label2.text = charactersArray[1]
        Label3.text = charactersArray[2]
        Label4.text = charactersArray[3]
        Label5.text = charactersArray[4]
        
        labelsArray.removeAll()
        
        labelsArray.append(Label1)
        labelsArray.append(Label2)
        labelsArray.append(Label3)
        labelsArray.append(Label4)
        labelsArray.append(Label5)
        
    }
    
    // Get random character
    func randomString(length: Int, letters: String) -> String {
        return String((0..<length).map{ _ in letters.randomElement()!})
    }
    
    // Touch controls
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first!
        swipeStart = touch.location(in: self.view)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first!
        let location = touch.location(in: self.view)
        
        // Get the angle from the swipe
        let deltaY1 = location.y -  swipeStart.y
        let deltaX1 = location.x -  swipeStart.x
        let angle =  getAngle(deltaX: Float(deltaX1), deltaY: Float(deltaY1)) //računanje kota
        
        if(swipeAllowed && swipeView.frame.contains(location)){
        // Če so C-ji
        if(VATypeNum == 0){
            if(angle > 157.5 && angle < 202.5)
            {
                print("LEVO: A")
                swipedCharacter = "a"
            }
            else if(angle < 22.5 || angle > 337.5){
                print("DESNO: B")
                swipedCharacter = "b"
            }
            else if(angle > 67.5 && angle < 112.5)
            {
                print("DOL: C")
                swipedCharacter = "c"
            }
            else if(angle > 247.5 && angle < 292.5)
            {
                print("GOR: D")
                swipedCharacter = "d"
            }
            else if(angle > 22.5 && angle < 67.5)
            {
                print("DESNO DOL G")
                swipedCharacter = "g"
            }
            else if(angle > 202.5 && angle < 247.5)
            {
                print("LEVO GOR F")
                swipedCharacter = "f"
            }
            else if(angle > 112.5 && angle < 157.5)
            {
                print("LEVO DOL: H")
                swipedCharacter = "h"
            }
            else if(angle > 292.5 && angle < 337.5)
            {
                print("DESNO GOR: E")
                swipedCharacter = "e"
            }
        }
        
        else if(VATypeNum == 1){
            if(angle < 45 || angle > 315)
            {
                print("DESNO: I")
                swipedCharacter = "i"
            }
            
            if(angle > 45 && angle < 135)
            {
                print("DOL: L")
                swipedCharacter = "l"
            }
            
            if(angle > 135 && angle < 225)
            {
                print("LEVO: J")
                swipedCharacter = "j"
            }
            
            if(angle > 225 && angle < 315)
            {
                print("GOR: K")
                swipedCharacter = "k"
            }
        }
        
        let stp = currentStep
        
        // Animate scale of label
        UIView.animate(withDuration: 0.1, animations: {
            self.labelsArray[stp].transform=CGAffineTransform(scaleX: 1.2,y: 1.2);
        }, completion: {(finished:Bool) in
            UIView.animate(withDuration: 0.1, animations: {
                self.labelsArray[stp].transform=CGAffineTransform(scaleX: 1,y: 1);
            })
        })
        
        // Check if swipe direction and character in array match
        if(swipedCharacter == charactersArray[currentStep]){
            labelsArray[currentStep].textColor = UIColor.green
        }
        else{
            labelsArray[currentStep].textColor = UIColor.red
            wrongCounter = wrongCounter + 1;
        }
        
        if(currentStep < 4){
          currentStep = currentStep+1
            
            // Animate move of indicator
            UIView.animate(withDuration: 0.2, animations: {
                self.MarkerView.center = CGPoint(x: self.labelsArray[self.currentStep].center.x, y: self.MarkerView.center.y)
            })
        }
        else // Set the new visus
        {
            currentStep = 0
            VisusLabel.text = VAValues[currentSectionVisus]
            
            // If wrong more than twom times in a row
            if(wrongCounter>2){
                swipeAllowed = false
                UIView.animate(withDuration: 0.3, animations: {
                    self.ResultsView.alpha = 1
                })
            }
            
            wrongCounter = 0
            currentSectionVisus = currentSectionVisus + 1
            
            if(VATypeNum == 0){
                if(currentSectionVisus<CSizes.count){
                    currentStepFontSize = CSizes[currentSectionVisus]
                }
                else{
                    swipeAllowed = false
                    UIView.animate(withDuration: 0.3, animations: {
                        self.ResultsView.alpha = 1
                    })
                }
            }
            else{
                if(currentSectionVisus<ESizes.count){
                    currentStepFontSize = ESizes[currentSectionVisus]
                }
                else{
                    swipeAllowed = false
                    UIView.animate(withDuration: 0.3, animations: {
                        self.ResultsView.alpha = 1
                    })
                }
            }
            
            // Reset test with new values
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                // Make all labels black again
                for lab in self.labelsArray  {
                    lab.textColor = UIColor.black
                }
                // Reset labels
                self.FillLabelWithContent()
                
                // Animate move of indicator
                UIView.animate(withDuration: 0.2, animations: {
                    self.MarkerView.center = CGPoint(x: self.labelsArray[self.currentStep].center.x, y: self.MarkerView.center.y)
                })
            })
            
            
        }
        }
      
    }
    

    
    // Calculate the angle
    func getAngle(deltaX: Float, deltaY: Float) -> Double{
        var angle = atan2f(deltaY, deltaX) * 180 / 3.14
        angle = (angle > 0.0 ? angle : (360.0 + angle))
        return Double(angle)
    }
    
    // Select C optotypes button
    @IBAction func SelectOptotypeC(_ sender: Any) {
        VATypeNum = 0
        currentStepFontSize = 64
        FillLabelWithContent()
        UIView.animate(withDuration: 0.3, animations: {
            self.OptotypeSelectView.alpha = 0
        })
        swipeAllowed = true
    }
    
    // Select E optotypes button
    @IBAction func SelectOptotypeE(_ sender: Any) {
        VATypeNum = 1
        currentStepFontSize = 53
        FillLabelWithContent()
        UIView.animate(withDuration: 0.3, animations: {
            self.OptotypeSelectView.alpha = 0
        })
        swipeAllowed = true
    }
    
    // Repeat the test - show select optotpes button
    @IBAction func RepeatTest(_ sender: Any) {
        currentStep = 0
        currentSectionVisus = 0
        swipeAllowed = false
        wrongCounter = 0
        UIView.animate(withDuration: 0.3, animations: {
            self.OptotypeSelectView.alpha = 1
            self.ResultsView.alpha = 0
        })
        
        // Make all labels black again
        for lab in self.labelsArray  {
            lab.textColor = UIColor.black
        }
        
        // Animate move of indicator
        UIView.animate(withDuration: 0.2, animations: {
            self.MarkerView.center = CGPoint(x: self.Label1.center.x, y: self.MarkerView.center.y)
        })
    }
    
    
    // Exit the test
    @IBAction func ExitTestAction(_ sender: Any) {
        UIScreen.main.brightness = br
        NotificationCenter.default.post(name: Notification.Name(rawValue: "MainMenuDefaultCheck"), object: nil)
        self.dismiss(animated: true, completion: nil)
    }
    
    // Allow rotation
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    // Hide top bar
    override var prefersStatusBarHidden: Bool {
        return true
    }
}
