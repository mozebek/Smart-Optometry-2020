//
//  TestsViewController.swift
//  SmartOptometry2k19
//
//  Created by Matic Ozebek on 17/05/2019.
//  Copyright © 2019 Matic Ozebek. All rights reserved.
//

import UIKit




class TestsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, TestInfoProtocol {
    

    @IBOutlet weak var TestsList: UIView!
    @IBOutlet weak var TestsTableView: UITableView!
    @IBOutlet weak var SelectTestLabel: UILabel!
    @IBOutlet weak var AllTestsLabel: UILabel!
    
    
    var SOTests = [SOTest]();
    
    override func viewWillAppear(_ animated: Bool) {
        //Translates
        AllTestsLabel.text = "all_tests".localized
        SelectTestLabel.text = "select_test".localized
        TestsTableView.reloadData()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        TestsList.layer.cornerRadius = 5
        TestsList.layer.shadowColor = UIColor.darkGray.cgColor
        TestsList.layer.shadowOffset = CGSize(width: 5.0, height: 5.0)
        TestsList.layer.shadowRadius = 25.0
        TestsList.layer.shadowOpacity = 0.2
        TestsList.backgroundColor = UIColor.white;
        
        // Load tests
        let test1 = SOTest(testName: "worth_four_dot_upper_case",
                           testColor:  UIColor.clear ,
                           testImageName: "WorthFourDotIcon",
                           testVCName: "WorthFourDot",
                           testNumber: 0,
                           testLink: "https://www.smart-optometry.com/worth-four-dot/")
        
        let test2 = SOTest(testName: "schober_upper_case",
                           testColor:  UIColor.clear,
                           testImageName: "SchoberIcon",
                           testVCName: "Schober",
                           testNumber: 1,
                           testLink: "https://www.smart-optometry.com/schober/")
        
        let test3 = SOTest(testName: "colour_vision_upper_case",
                           testColor:  UIColor.clear,
                           testImageName: "ColorVision",
                           testVCName: "ColorVision",
                           testNumber: 2,
                           testLink: "https://www.smart-optometry.com/colour-vision/")
        
        let test4 = SOTest(testName: "visual_acuity_upper_case",
                           testColor:  UIColor.clear,
                           testImageName: "VisualAcuityIcon",
                           testVCName: "VisualAcuity",
                           testNumber: 3,
                           testLink: "https://www.smart-optometry.com/visual-acuity/")
        
        let test5 = SOTest(testName: "okn_stripes_upper_case",
                           testColor:  UIColor.clear,
                           testImageName: "OknStripesIcon",
                           testVCName: "OknStripes",
                           testNumber: 4,
                           testLink: "https://www.smart-optometry.com/okn-stripes/")
        
        let test6 = SOTest(testName: "flourescin_light_upper_case",
                           testColor:  UIColor.clear,
                           testImageName: "FlourescinIcon",
                           testVCName: "FlourescinLight",
                           testNumber: 5,
                           testLink: "https://www.smart-optometry.com/fluorescin-light/")
        
        let test7 = SOTest(testName: "amsler_grid_upper_case",
                           testColor:  UIColor.clear,
                           testImageName: "AmslerIcon",
                           testVCName: "AmslerGrid",
                           testNumber: 6,
                           testLink: "https://www.smart-optometry.com/amsler-grid/")
        
        let test8 = SOTest(testName: "duochrome_upper_case",
                           testColor:  UIColor.clear,
                           testImageName: "DuochromeIcon",
                           testVCName: "Duochrome",
                           testNumber: 7,
                           testLink: "https://www.smart-optometry.com/duochrome/")
        
        let test9 = SOTest(testName: "contrast_upper_case",
                           testColor:  UIColor.clear,
                           testImageName: "ContrastIcon",
                           testVCName: "Contrast",
                           testNumber: 8,
                           testLink: "https://www.smart-optometry.com/contrast/")
        
        let test10 = SOTest(testName: "accommodation_upper_case",
                           testColor:  UIColor.clear,
                           testImageName: "AccommodationIcon",
                           testVCName: "Accommodation",
                           testNumber: 9,
                           testLink: "https://www.smart-optometry.com/accommodation/")
        
        let test11 = SOTest(testName: "aniseikonia_upper_case",
                            testColor:  UIColor.clear,
                            testImageName: "AniseikoniaIcon",
                            testVCName: "Aniseikonia",
                            testNumber: 10,
                            testLink: "https://www.smart-optometry.com/aniseikonia/")
        
        let test12 = SOTest(testName: "mem_retinoscopy_upper_case",
                            testColor:  UIColor.clear,
                            testImageName: "MemIcon",
                            testVCName: "MemRetinoscopy",
                            testNumber: 11,
                            testLink: "https://www.smart-optometry.com/mem-retinoscopy/")
        
        let test13 = SOTest(testName: "red_desaturation_upper_case",
                            testColor:  UIColor.clear,
                            testImageName: "RedDesaturationIcon",
                            testVCName: "RedDesaturation",
                            testNumber: 12,
                            testLink: "https://www.smart-optometry.com/red-desaturation/")
        
        let test14 = SOTest(testName: "hirschberg_upper_case",
                            testColor:  UIColor.clear,
                            testImageName: "HirschbergIcon",
                            testVCName: "Hirschberg",
                            testNumber: 13,
                            testLink: "https://www.smart-optometry.com/hirschberg/")
        
        let test15 = SOTest(testName: "visual_acuity_plus",
                            testColor:  UIColor.clear,
                            testImageName: "VAPlusIcon",
                            testVCName: "VAPlus",
                            testNumber: 14,
                            testLink: "https://www.smart-optometry.com/visual-acuity-plus/")
        
        SOTests += [test1, test2, test3, test4, test5, test6, test7, test8, test9, test10, test11, test12, test13, test14, test15]
        
        TestsTableView.delegate = self
        TestsTableView.dataSource = self
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return SOTests.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Table view cells are reused and should be dequeued using a cell identifier.
        let cellIdentifier = "cell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? TestCellTableViewCell  else {
            fatalError("The dequeued cell is not an instance of MealTableViewCell.")
        }
        
        // Fetches the appropriate meal for the data source layout.
        let test = SOTests[indexPath.row]
        
        cell.TestImageHolder.backgroundColor = test.TestColor
        cell.TestName.text = test.TestName.localized
        let image = UIImage(named: test.TestImageName)
        cell.TestImage.image = image;
        cell.InfoButton.tag = test.TestNumber
        cell.InfoButton.setTitle("info".localized, for: .normal)
        cell.TypeLabel.text = "test".localized
        cell.delegate = self
        cell.selectionStyle = .none
        
        return cell
    }

    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        // Increase ads counter
        var adsCounter = UserDefaults.standard.integer(forKey: "interstitialCounter")
        adsCounter += 1
        UserDefaults.standard.set(adsCounter, forKey: "interstitialCounter")
        
        // Increase ads counter
        var rwCounter = UserDefaults.standard.integer(forKey: "ReviewsCounter")
        rwCounter += 1
        UserDefaults.standard.set(adsCounter, forKey: "ReviewsCounter")
        
        // Open new test with transition manager class (slide left animation)
        let testViewController = storyboard!.instantiateViewController(withIdentifier: SOTests[indexPath.row].TestVCName)
        testViewController.transitioningDelegate = self as? UIViewControllerTransitioningDelegate
        testViewController.modalPresentationStyle = .fullScreen
        present(testViewController, animated: false, completion: nil)
        
    }

    
    func ShowTestInfo(tag: Int) {
        print(tag)
        let displayVC : TestInfoViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TestInfo") as! TestInfoViewController
        displayVC.view.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.7)
        displayVC.modalPresentationStyle = .overFullScreen
        displayVC.modalTransitionStyle = .crossDissolve
        self.present(displayVC, animated: true, completion: nil )
        displayVC.SetTestInfo(TestNumber: tag, TestLink: SOTests[tag].TestLink, tName: SOTests[tag].TestName)
    }
    

    
    


}


