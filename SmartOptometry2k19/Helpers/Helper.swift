//
//  Helper.swift
//  SmartOptometry2k19
//
//  Created by Matic Ozebek on 15/05/2019.
//  Copyright © 2019 Matic Ozebek. All rights reserved.
//

import UIKit

class Helper {
    
    static var app: Helper = {
        return Helper()
    }()

    public func addGradientToView(view: UIView, color1: UIColor, color2: UIColor)
    {
        //gradient layer
        let gradientLayer = CAGradientLayer()
        //define colors
        gradientLayer.colors = [color1.cgColor,color2.cgColor]
        //define locations of colors as NSNumbers in range from 0.0 to 1.0
        //if locations not provided the colors will spread evenly
        gradientLayer.locations = [0,1]
        gradientLayer.startPoint = CGPoint(x: 0, y: 1)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0)
        gradientLayer.contentsScale = 150;
        //define frame
        gradientLayer.frame = view.bounds
        //insert the gradient layer to the view layer
        view.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    //Method to make circle shape view
    public func SetCircle(CView: UIView, Color: UIColor){
        CView.layer.cornerRadius = CView.frame.size.height/2;
        CView.layer.masksToBounds = true;
        CView.backgroundColor = Color;
        
    }
    
    // Add border to view
    public func SetBorder(CView: UIView, borderWidth: Float, borderColor: UIColor){
        CView.layer.borderColor = borderColor.cgColor
        CView.layer.borderWidth = CGFloat(borderWidth)
    }
    
    public func CheckForBrightness() -> Bool{
        let autoBrightnessOn = UserDefaults.standard.bool(forKey: "AutoBrightnessOn")
        if(autoBrightnessOn ==  true)
        {
            UIScreen.main.brightness = CGFloat(1)
        }
        return autoBrightnessOn
    }

    
    
}


extension UIView {
    
    func addShadow(offset: CGSize, color: UIColor, radius: CGFloat, opacity: Float) {
        layer.masksToBounds = true
        layer.shadowOffset = offset
        layer.shadowColor = color.cgColor
        layer.shadowRadius = radius
        layer.shadowOpacity = opacity
        
        let backgroundCGColor = backgroundColor?.cgColor
        backgroundColor = nil
        layer.backgroundColor =  backgroundCGColor
    }
}

extension UIFont {
    
    func withTraits(traits:UIFontDescriptor.SymbolicTraits...) -> UIFont {
        let descriptor = self.fontDescriptor
            .withSymbolicTraits(UIFontDescriptor.SymbolicTraits(traits))
        return UIFont(descriptor: descriptor!, size: 0)
    }
    
    func bold() -> UIFont {
        return withTraits(traits: .traitBold)
    }
    
    func removeBold()-> UIFont
    {

        var symTraits = fontDescriptor.symbolicTraits
        symTraits.remove([.traitBold])
        let fontDescriptorVar = fontDescriptor.withSymbolicTraits(symTraits)
        return UIFont(descriptor: fontDescriptorVar!, size: 0)

    }
}


