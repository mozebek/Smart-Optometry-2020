//
//  SubscriptionsViewController.swift
//  SmartOptometry2k19
//
//  Created by Matic Ozebek on 07/04/2020.
//  Copyright © 2020 Matic Ozebek. All rights reserved.
//

import UIKit
import StoreKit

class SubscriptionsViewController: UIViewController, SKProductsRequestDelegate {

    
    @IBOutlet weak var PurchaseButton: UIButton!
    
    var productIdentifier = "SoYearlySub" //Get it from iTunes connect
    var productID = "SoYearlySub"
    var productsRequest = SKProductsRequest()
    var iapProducts = [SKProduct]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        fetchAvailableProducts()
        
        
    }
    
    func fetchAvailableProducts() {

        // Put here your IAP Products ID's
        let productIdentifiers = NSSet(objects: productIdentifier)
        guard let identifier = productIdentifiers as? Set<String> else { return }
        productsRequest = SKProductsRequest(productIdentifiers: identifier)
        productsRequest.delegate = self
        productsRequest.start()
    }
    
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        if response.products.count > 0 {
        iapProducts = response.products
            if iapProducts.count > 0 {
                let purchasingProduct = response.products[0] as SKProduct
                // Get its price from iTunes Connect
                DispatchQueue.main.async {
                    // Set Button Price
                    self.PurchaseButton.setTitle(purchasingProduct.localizedPrice, for: .normal)
                }
                
            }
        }
    }
    
    func canMakePurchases() -> Bool {
     return SKPaymentQueue.canMakePayments()
    }
    
    func purchaseProduct(product: SKProduct) {
        if self.canMakePurchases() {
            let payment = SKPayment(product: product)
            SKPaymentQueue.default().add(self)
            SKPaymentQueue.default().add(payment)
            productID = product.productIdentifier //also show loader
        }
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            guard let productType = ProductType(rawValue: transaction.payment.productIdentifier) else {fatalError()}
            switch transaction.transactionState {
                case .purchasing:
                print("purchasing")
                case .purchased:
                print("purchased")
                case .failed:
                print("failed")
                case .restored:
                print("restored")
                case .deferred:
                print("deferred")
            }
        }
    }




}

extension SKProduct {
    var localizedPrice: String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = priceLocale
        return formatter.string(from: price)!
    }
}
