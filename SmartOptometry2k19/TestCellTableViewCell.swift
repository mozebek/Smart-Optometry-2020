//
//  TestCellTableViewCell.swift
//  SmartOptometry2k19
//
//  Created by Matic Ozebek on 15/05/2019.
//  Copyright © 2019 Matic Ozebek. All rights reserved.
//

import UIKit

protocol TestInfoProtocol {
    func ShowTestInfo(tag: Int)
}

class TestCellTableViewCell: UITableViewCell {

    @IBOutlet var TestImage: UIImageView!
    @IBOutlet weak var TestName: UILabel!
    @IBOutlet weak var TestImageHolder: UIView!
    @IBOutlet weak var InfoButton: UIButton!
    @IBOutlet weak var TypeLabel: UILabel!
    
    var delegate: TestInfoProtocol!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    override func layoutSubviews() {
        super.layoutSubviews()

        // Set image holder to circle
        TestImageHolder.layer.cornerRadius = TestImageHolder.frame.size.height/2
        TestImageHolder.layer.masksToBounds = true;
        TestImageHolder.backgroundColor = UIColor.init(red: 229/255, green: 229/255, blue: 229/255, alpha: 1    )
        
        InfoButton.layer.cornerRadius = InfoButton.frame.size.height/2
        
        //Helper.app.addGradientToView(view: TestImageHolder,
          //                           color1: UIColor.init(displayP3Red: 25/255, green: 154/255, blue: 143/255, alpha: 0.8),
            //                         color2: UIColor.init(displayP3Red: 35/255, green: 188/255, blue: 121/255, alpha: 0.8));
        
        
        let margins = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
        contentView.frame = contentView.frame.inset(by: margins)
    }
    
    @IBAction func ShowTestInfo(sender: AnyObject) {
        self.delegate.ShowTestInfo(tag: sender.tag)
    }


}
