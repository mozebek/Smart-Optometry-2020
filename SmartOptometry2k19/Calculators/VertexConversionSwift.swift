//
//  VertexConversionSwift.swift
//  Smart Optometry
//
//  Created by Matic Ozebek on 24/11/16.
//  Copyright © 2016 Matic Ozebek. All rights reserved.
//

import UIKit

class VertexConversionSwift: UIViewController,  UIPickerViewDataSource, UIPickerViewDelegate {

    // Variables, outlets...
    var firstArray:[String] = []
    var valuesArray:[Float] = []
    var secondArray:[String] = []
    var swapBool: Bool = false;
    var distance: Float = 12
    
    @IBOutlet weak var pickerView1: UIPickerView!
    @IBOutlet weak var distanceSegment: UISegmentedControl!
    
    @IBOutlet weak var vertexDistanceLabel: UILabel!
    @IBOutlet weak var leftLabel: UILabel!
    @IBOutlet weak var rightLabel: UILabel!
    @IBOutlet weak var swapButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set orientation to portrait
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
 
        // Set buttons corner radius
        swapButton.layer.cornerRadius = 5
        leftLabel.layer.masksToBounds = true
        leftLabel.layer.cornerRadius = 5
        
        // Picker view delegate
        pickerView1!.dataSource = self
        pickerView1!.delegate = self
        
        // Fill arrays with glasses and contacts values and present glasses -> contacts conversion
        self.fillFirstArray()
        self.fillSecondArray(distance:12, direction:-1)
        
        // Set picker view to default position 0.0
        pickerView1.selectRow(valuesArray.count/2, inComponent: 0, animated: true)
        pickerView1.selectRow(valuesArray.count/2, inComponent: 1, animated: true)
        
        //Translate
        leftLabel.text = "glasses_power".localized.uppercased()
        rightLabel.text = "contacts_power".localized.uppercased()
        vertexDistanceLabel.text = "vertex_distance".localized.uppercased()
        swapButton.setTitle("swap".localized.uppercased(), for: .normal)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(component == 0){
           return firstArray[row]
        }
        else{
            return secondArray[row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return firstArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        pickerView1.selectRow(row, inComponent: 0, animated: true)
        pickerView1.selectRow(row, inComponent: 1, animated: true)
    }
    
    
    
    // Fill left picker view values
    func fillFirstArray(){
       
        firstArray.removeAll()
        valuesArray.removeAll()
        
        var value: String = ""
        
        for index in stride(from: -30, to: 30.25, by: 0.250){
            
            var currentIndex: Float = 0.0;
            
            if(index > 0){
                currentIndex = round(100*Float(index))/100 //round to 2 decimals
                value = ("+" + String(currentIndex))
            }
            else{
                currentIndex = round(100*Float(index))/100 //round to 2 decimals
                value = (String(index))
            }
            
            firstArray.append(value)
            valuesArray.append(Float(index))
        }
    }
    
    // Fill right picker view values
    func fillSecondArray(distance: Float, direction: Float){
       
        secondArray.removeAll()
        
        var floatValue: Float = 0.0
        
        for index in stride(from: 0, to: valuesArray.count, by: 1){
            
            floatValue = 1.0/(1.0 / valuesArray[index] + (distance/1000.0) * direction)
            floatValue = round(100*floatValue)/100 //round to 2 decimals
            if(floatValue > 0){
               secondArray.append("+" + String(floatValue))
            }
            else{
               secondArray.append(String(floatValue))
            }
            
            
        }
    }


    // Change conversion direction glasses -> contacts or contact ->glasses
    @IBAction func swapPressed(sender: AnyObject){
        

        self.fillFirstArray()
        
        if(swapBool == false){
          self.fillSecondArray(distance: distance, direction: 1)
            leftLabel.text = "contacts_power".localized.uppercased()
            rightLabel.text = "glasses_power".localized.uppercased()
            swapBool = true
            pickerView1.reloadAllComponents()
           
        }
        else{
            self.fillSecondArray(distance: distance, direction: -1)
            leftLabel.text = "glasses_power".localized.uppercased()
            rightLabel.text = "contacts_power".localized.uppercased()
            swapBool = false
            pickerView1.reloadAllComponents()
        }
        
    }
    
    // Segment control milimeters distance
    @IBAction func changeDistance(sender:UISegmentedControl){
    

        
    switch distanceSegment.selectedSegmentIndex{
      case 0:
        self.fillFirstArray()
        distance = 8
        
        if(swapBool == false){
            self.fillSecondArray(distance: 8, direction: -1)
        }
        else{
            self.fillSecondArray(distance: 8, direction: 1)
        }
       
      case 1:
        distance = 10
        
        if(swapBool == false){
            self.fillSecondArray(distance: 10, direction: -1)
        }
        else{
            self.fillSecondArray(distance: 10, direction: 1)
        }
        
    case 2:
        distance = 12
        if(swapBool == false){
            self.fillSecondArray(distance: 12, direction: -1)
        }
        else{
            self.fillSecondArray(distance: 12, direction: 1)
        }
        
    case 3:
        distance = 14
        if(swapBool == false){
            self.fillSecondArray(distance: 14, direction: -1)
        }
        else{
            self.fillSecondArray(distance: 14, direction: 1)
        }
        
    case 4:
        distance = 16
        if(swapBool == false){
            self.fillSecondArray(distance: 16, direction: -1)
        }
        else{
            self.fillSecondArray(distance: 16, direction: 1)
        }
        
     default:
      break;
    }
        pickerView1.reloadAllComponents()
    }
    
    // Exit the test
    @IBAction func ExitTestAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // Allow rotation
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    

}
