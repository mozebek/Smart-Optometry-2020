//
//  VisualAcuitySwift.swift
//  Smart Optometry
//
//  Created by Matic Ozebek on 25/11/16.
//  Copyright © 2016 Matic Ozebek. All rights reserved.
//

import UIKit

class VisualAcuitySwift: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {

    // Variables outlets...
    @IBOutlet weak var UnitSelectorView: UIView!
    
    var leftArray:[String] = []
    var rightArray:[String] = []
    
    @IBOutlet weak var leftDropDownLabel: UILabel!
    @IBOutlet weak var rightDropDownlabel: UILabel!
    @IBOutlet weak var ChangeUnitButton: UIButton!
    @IBOutlet weak var DoneButton: UIButton!
    @IBOutlet weak var VAUnitLabel: UILabel!
    @IBOutlet weak var SelectUnit: UILabel!
    
    var feetArray:[String] = []
    var meterArray:[String] = []
    var decimalArray:[String] = []
    var angleArray:[String] = []
    var lineArray:[String] = []
    var logMarArray:[String] = []
    var spatialArray:[String] = []
    var visualEfficiencyArray:[String] = []
    var inchesArray:[String] = []
    var centimetersArray:[String] = []
    var americanPointarray:[String] = []
    var MNotationArray:[String] = []
    var jeagerArray:[String] = []
    
    
    var secondArray:[String] = []
    var swapBool: Bool = false;
    
    var currentSelectedLeftNumber: Int = 1
    var currentSelectedRightNumber: Int = 2
    
    var pickerData: [String] = []
    var allArrays:[[String]] = [[]]
    
    
    @IBOutlet weak var pickerView1: UIPickerView!
    @IBOutlet weak var pickerView2: UIPickerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set orientation to portrait
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        
        // Set transaltes
        VAUnitLabel.text = "va_unit_con".localized.uppercased()
        SelectUnit.text = "select_unit".localized.uppercased()
        ChangeUnitButton.setTitle("change".localized.uppercased(), for: .normal)
        DoneButton.setTitle("done".localized.uppercased(), for: .normal)

        
        // Set buttons corner radius
        rightDropDownlabel.layer.cornerRadius = 5
        rightDropDownlabel.layer.masksToBounds = true
        leftDropDownLabel.layer.cornerRadius = 5
        leftDropDownLabel.layer.masksToBounds = true
        ChangeUnitButton.layer.cornerRadius = 5
        DoneButton.layer.cornerRadius = 5
        
        // Hide unit selectr
        UnitSelectorView.alpha = 0;
        self.view.bringSubviewToFront(UnitSelectorView)
        
        pickerData = ["Feet 20/", "Meter 6/", "Decimal", "Visual Angle", "Line Number", "LogMAR", "Spatial Frequency (cyc/deg)", "Cen. Visual Efficiency", "Jaeger", "Inches 14/", "Centimeters 35/", "American Point-Type", "M Notation"]
        
        // Hardcode array values
        feetArray = [ "10", "12.5", "16", "20", "25", "30", "32", "40", "50", "60", "63", "70", "80", "100", "114", "125", "150", "160", "200", "250", "300", "320", "400", "800", "2000", "20000"]
        meterArray = ["3.0", "3.8", "4.8", "6.0", "7.5", "9.0", "9.6", "12.0", "15.0", "18.0", "18.9", "21.0", "24.0", "30.0", "34.2", "37.5", "45.0", "48.0", "60.0", "75.0", "90.0", "96.0", "120.0", "250.0", "600.0", "6000.0"]
        decimalArray = ["2.0","1.6","1.25","1.0","0.8","0.67","0.63","0.5","0.4","0.33","0.32","0.29","0.25","0.2","0.18","0.16","0.13","0.13","0.1", "0.08", "0.07", "0.06", "0.05", "0.03", "0.01", "0.001"]
        angleArray = ["0.5", "0.63", "0.8", "1.0", "1.25", "1.5", "1.6", "2.0", "2.5", "3.0", "3.15", "3.5", "4.0", "5.0", "5.7", "6.25", "7.5", "8.0", "10.0", "12.5", "15.0", "16.0", "20.0", "40.0", "100.0", "1000.0"]
        lineArray = ["-3", "-2", "-1", "0", "1", "-", "2", "3", "4", "-", "5", "-", "6", "7", "-", "8", "-", "9", "10", "11", "-", "12", "13", "16", "20", "30"	]
        logMarArray = ["-0.3", "-0.2", "-0.1", "0.0", "0.1", "0.18", "0.2", "0.3", "0.4", "0.48", "0.5", "0.54", "0.6", "0.7", "0.76", "0.8", "0.88", "0.9", "1.0", "1.1", "1.18", "1.2", "1.3", "1.6", "2.0", "3.0"]
        spatialArray = ["60.0", "48.0", "37.5", "30.0", "24.0", "20.0", "18.75", "15.0", "12.0", "10.0", "9.52", "8.57", "7.5", "6.0", "5.26", "4.8", "4.0", "3.75", "3.0", "2.4", "2.0", "1.88", "1.5", "0.75", "0.3", "0.03"]
        visualEfficiencyArray = ["100", "100", "100", "100", "95", "91", "90", "85", "75", "67", "65", "63", "60", "50", "44", "40", "32", "30", "20", "17", "16", "15", "10", "5", "0", "0"]
        jeagerArray = ["-", "-", "-", "1", "2", "3", "4", "5", "6", "7", "8", "-", "9", "10", "11", "12", "-", "13", "14", "-", "-", "-", "-", "-", "-", "-"]
        inchesArray = ["7.0", "8.8", "11.2", "14.0", "17.5", "21.0", "22.4", "28.0", "35.0", "42.0", "44.1", "49.0", "56.0", "70.0", "79.8", "87.5", "105.0", "112.0", "140.0", "175.0", "210.0", "224.0", "280.0", "560.0", "1400.0", "14000.0"]
        centimetersArray = ["17.5", "21.9", "28.0", "35.0", "43.8", "52.5", "56.0", "70.0", "87.5", "105.0", "110.3", "122.5", "140.0", "175.0", "199.5", "218.8", "262.5", "280.0", "350.0", "437.5", "525.0", "560.0", "700.0", "1400.0", "3500.0", "35000.0"]
        americanPointarray = ["-", "-", "-", "3", "4", "5", "6", "7", "8", "9", "10", "-", "11", "12", "13", "14", "-", "21", "23", "-", "-", "-", "-", "-", "-", "-"]
        MNotationArray = ["0.2", "0.25", "0.32", "0.4", "0.5", "0.6", "0.64", "0.8", "1.0", "1.2", "1.3", "1.4", "1.6", "2.0", "2.3", "2.5", "3.0", "3.2", "4.0", "5.0", "6.0", "6.4", "8.0", "16.0", "40.0", "400.0"]

        // Array with all arrays of converts
        allArrays = [feetArray,meterArray,decimalArray,angleArray,lineArray,logMarArray,spatialArray,visualEfficiencyArray,jeagerArray,inchesArray,centimetersArray,americanPointarray,MNotationArray]
        
        // Defaault arrays
        leftArray = feetArray
        rightArray = meterArray
        
        // Picker view delegate
        pickerView1!.dataSource = self
        pickerView1!.delegate = self
        
        pickerView2!.dataSource = self
        pickerView2!.delegate = self
        
        // Set picker view to default position 0.0
        pickerView1.selectRow(meterArray.count/2, inComponent: 0, animated: true)
        pickerView1.selectRow(meterArray.count/2, inComponent: 1, animated: true)
    
    }
    
    @IBOutlet weak var vaUnitLabel: UILabel!
    @IBOutlet weak var tableSorceLabel: UILabel!
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        print("title")
        if(component == 0 && pickerView == pickerView1){
            return leftArray[row]
            
        }
        
        if(component == 1 && pickerView == pickerView1){
            return rightArray[row]
            
        }
        
        else{
            return pickerData[row]
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(pickerView == pickerView1){
            return leftArray.count
        }
            else{
             return pickerData.count
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if(pickerView == pickerView1){
            pickerView1.selectRow(row, inComponent: 0, animated: true)
            pickerView1.selectRow(row, inComponent: 1, animated: true)
        }

        if(pickerView == pickerView2 && component == 0){
            self.leftArray =  allArrays[row]
            self.leftDropDownLabel.text = pickerData[row]
            self.pickerView1.reloadAllComponents()
        }
        
        else if(pickerView == pickerView2 && component == 1){
            self.rightArray =  allArrays[row]
            self.pickerView1.reloadAllComponents()
            self.rightDropDownlabel.text = pickerData[row]
        }
        
    }
    
    // Set text color to white in picker view
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString?{
        if(pickerView == pickerView2 ){
            return NSAttributedString(string: pickerData[row], attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
        }
        else if(pickerView == pickerView1 && component == 0 ){
            return NSAttributedString(string: leftArray[row], attributes: [NSAttributedString.Key.foregroundColor : UIColor.black])
        }
        else{
            return NSAttributedString(string: rightArray[row], attributes: [NSAttributedString.Key.foregroundColor : UIColor.black])
        }
        

    }
    
    @IBAction func HideUnitSelectorView(_ sender: Any) {
        UIView.animate(withDuration: 0.3, animations: {
            self.UnitSelectorView.alpha = 0
        })
    }
    
    @IBAction func ShowUnitSelectorView(_ sender: Any) {
        UIView.animate(withDuration: 0.3, animations: {
            self.UnitSelectorView.alpha = 1
        })
    }
    
    // Exit the test
    @IBAction func ExitTestAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func ResetCalculator(_ sender: Any){
        
        // Set picker view to default position 0.0
        leftArray =  feetArray
        rightArray = meterArray
        leftDropDownLabel.text = "Feet 20/"
        rightDropDownlabel.text = "Meter 6/"
        
        pickerView2.selectRow(1, inComponent: 0, animated: true)
        pickerView2.selectRow(2, inComponent: 1, animated: true)
        
        pickerView1.selectRow(meterArray.count/2, inComponent: 0, animated: true)
        pickerView1.selectRow(meterArray.count/2, inComponent: 1, animated: true)
        
        pickerView1.reloadAllComponents()
        pickerView2.reloadAllComponents()
    }
    
    // Allow rotation
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    


}
