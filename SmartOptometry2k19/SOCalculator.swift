//
//  SOCalculator.swift
//  SmartOptometry2k19
//
//  Created by Matic Ozebek on 08/10/2019.
//  Copyright © 2019 Matic Ozebek. All rights reserved.
//

import UIKit

class SOCalculator: NSObject {

    var CalculatorName: String = ""
    var CalculatorColor: UIColor = UIColor.black
    var CalculatorImageName: String = ""
    var CalculatorVCName: String = ""
    
    init(calculatorName: String, calculatorColor: UIColor, calculatorImageName: String, calculatorVCName: String) {
        self.CalculatorName = calculatorName
        self.CalculatorColor = calculatorColor
        self.CalculatorImageName = calculatorImageName
        self.CalculatorVCName = calculatorVCName
        super.init()
    }
}
