//
//  TestInfoViewController.swift
//  SmartOptometry2k19
//
//  Created by Matic Ozebek on 28/10/2019.
//  Copyright © 2019 Matic Ozebek. All rights reserved.
//

import UIKit

class TestInfoViewController: UIViewController {
    
    

    @IBOutlet weak var  instructionsView: UIView!
    @IBOutlet weak var  instructionsScroll: UIScrollView!
    @IBOutlet weak var MoreButton: UIButton!
    @IBOutlet weak var InfoSmallView: UIView!
    @IBOutlet weak var TestName: UILabel!
    @IBOutlet weak var LinkButton: UIButton!
    
    var link: String! = ""
    
    
    var testInstructionsText: [Array<Any>] = []
    var testInstructionsDesign: [Array<Any>] = []
    
    
    func SetTestInfo(TestNumber: Int, TestLink: String, tName: String) {
        self.addCurrentSelectedTestInstructions(InstructionText: testInstructionsText[TestNumber] as! Array<String>, InstuctionDesign: testInstructionsDesign[TestNumber] as! Array<Bool>)
        TestName.text = tName.localized
        link = TestLink;
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Add text to arrays 
        addTestsInstructions()
        
        //
        instructionsView.layer.cornerRadius = 10
        MoreButton.layer.cornerRadius = 5
        MoreButton.clipsToBounds = true;
        InfoSmallView.layer.cornerRadius = InfoSmallView.frame.size.height/2
        InfoSmallView.clipsToBounds = true;
        
        // Trasnlates
        LinkButton.setTitle("or_read_more_online".localized, for: .normal)
        MoreButton.setTitle("close".localized, for: .normal)
        
        // Add green gradient to small info view
        Helper.app.addGradientToView(view: InfoSmallView,
                                     color1: UIColor.init(displayP3Red: 25/255, green: 154/255, blue: 143/255, alpha: 1),
                                     color2: UIColor.init(displayP3Red: 35/255, green: 188/255, blue: 121/255, alpha: 1))
        
        Helper.app.addGradientToView(view: MoreButton,
                                     color1: UIColor.init(displayP3Red: 25/255, green: 154/255, blue: 143/255, alpha: 1),
                                     color2: UIColor.init(displayP3Red: 35/255, green: 188/255, blue: 121/255, alpha: 1))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        instructionsView.layer.cornerRadius = 10
        MoreButton.layer.cornerRadius = 5
        MoreButton.clipsToBounds = true;
        InfoSmallView.layer.cornerRadius = InfoSmallView.frame.size.height/2
        InfoSmallView.clipsToBounds = true;
        
        // Add green gradient to small info view
        Helper.app.addGradientToView(view: InfoSmallView,
                                     color1: UIColor.init(displayP3Red: 25/255, green: 154/255, blue: 143/255, alpha: 1),
                                     color2: UIColor.init(displayP3Red: 35/255, green: 188/255, blue: 121/255, alpha: 1))
        
        Helper.app.addGradientToView(view: MoreButton,
                                     color1: UIColor.init(displayP3Red: 25/255, green: 154/255, blue: 143/255, alpha: 1),
                                     color2: UIColor.init(displayP3Red: 35/255, green: 188/255, blue: 121/255, alpha: 1))
    }
    
    // Method that loads selected test localized instructions when user select one
    func addCurrentSelectedTestInstructions(InstructionText: Array<String>, InstuctionDesign: Array<Bool>){
        
        // Determine font size for iPad and iPhone
        var fontSize: CGFloat = 16
        //var labelHeight: Int = 22
        if(UIDevice.current.userInterfaceIdiom == .phone){
            fontSize = 16
            //labelHeight = 22
        }
        else{
            fontSize = 23
            //labelHeight = 30
        }
        
        // Remove all instructions data from instructions scroll view
        let subViews = self.instructionsScroll.subviews
        for subview in subViews{
            subview.removeFromSuperview()
        }
        
        var yPositionOfLabel = 0
        
        // Filling the instuctions scroll view with selected test instructions
        for index in 0...(InstructionText.count-1){
            
            // Variable to store last label height that was added to scroll view
            var lastLabelHeight = 0
            
            // If line is some of headers make it BOLD
            if(InstuctionDesign[index]){
                
                // Create label with header BOLD text
                let label = UILabel(frame: CGRect(x: 10, y: yPositionOfLabel, width: Int(instructionsScroll.frame.size.width-30), height: 22))
                label.text = InstructionText[index].localized
                label.textAlignment = .left
                label.font = UIFont.systemFont(ofSize: fontSize, weight: UIFont.Weight.bold)
                label.textColor = UIColor(red: 0.368, green: 0.368, blue: 0.368, alpha: 1)
                label.numberOfLines = 0
                label.sizeToFit()
                self.instructionsScroll.addSubview(label)
                
                lastLabelHeight = Int(label.frame.size.height)
                
                // Add aditional space between header and first regular label
                yPositionOfLabel = yPositionOfLabel+10
                let emptyLabel = UILabel(frame: CGRect(x: 0, y: yPositionOfLabel, width: Int(instructionsScroll.frame.size.width), height: 22))
                emptyLabel.text = "   "
                self.instructionsScroll.addSubview(emptyLabel)
            }
                
                // Else just make casual label with text
            else{
                
                let dotLabel = UILabel(frame: CGRect(x: 15, y: yPositionOfLabel, width: Int(instructionsScroll.frame.size.width), height: 22))
                dotLabel.text =  "•"
                dotLabel.textAlignment = .left
                dotLabel.font = UIFont.systemFont(ofSize: fontSize, weight: UIFont.Weight.medium)
                dotLabel.textColor = UIColor(red: 0.368, green: 0.368, blue: 0.368, alpha: 1)
                dotLabel.numberOfLines = 0
                dotLabel.sizeToFit()
                self.instructionsScroll.addSubview(dotLabel)
                
                
                // Create casual label
                let label = UILabel(frame: CGRect(x: Int(dotLabel.frame.size.width) + 20, y: yPositionOfLabel, width: Int(instructionsScroll.frame.size.width-40), height: 22))
                label.text = InstructionText[index].localized
                label.textAlignment = .left
                label.font = UIFont.systemFont(ofSize: fontSize, weight: UIFont.Weight.medium)
                label.textColor = UIColor(red: 0.368, green: 0.368, blue: 0.368, alpha: 1)
                label.numberOfLines = 0
                label.sizeToFit()
                self.instructionsScroll.addSubview(label)
                
                lastLabelHeight = Int(label.frame.size.height)
                
            }
            
            // Move lower to add another line of text
            yPositionOfLabel = yPositionOfLabel + lastLabelHeight + 5
        }
        
        // Make scroll view that will fit labels added
        let instScrollSize = CGSize(width: self.instructionsScroll.frame.size.width, height: CGFloat(yPositionOfLabel))
        instructionsScroll.isScrollEnabled = true
        instructionsScroll.contentSize = instScrollSize
    }
    
    // Add available tests instructions text by strings -> ading bool valu for recognizing which string should be bolded in instructions view
    func addTestsInstructions(){
        
        // Add each test instructions
        let test0InstrtuctionsArray = ["equipment_and_procedure","fourty_cm_instruction","wfd_line2","wfd_line3","perform_on_light_or_dark"]
        let test0InstructionsTextDesign = [true,false,false,false,false]
        
        let test1InstrtuctionsArray = ["equipment_and_procedure","fourty_cm_instruction","sch_line2","perform_on_light_or_dark"]
        let test1InstructionsTextDesign = [true,false,false,false]
        
        let test2InstrtuctionsArray = ["equipment_and_procedure","fourty_cm_instruction","cvision_line1","cvision_line2","cvision_line3"]
        let test2InstructionsTextDesign = [true,false,false,false,false]
        
        let test3InstrtuctionsArray = ["equipment_and_procedure","fourty_cm_instruction","device_sidewise","va_line1","va_line2"]
        let test3InstructionsTextDesign = [true,false,false,false,false]
        
        let test4InstrtuctionsArray = ["equipment_and_procedure","okn_line1","okn_line2","okn_line3","okn_line4","okn_line5","okn_line6"]
        let test4InstructionsTextDesign = [true,false,false,false,false,false,false]
        
        let test5InstrtuctionsArray = ["equipment_and_procedure","flou_line1","flou_line1"]
        let test5InstructionsTextDesign = [true,false,false]
        
        let test6InstrtuctionsArray = ["equipment_and_procedure","fourty_cm_instruction","amsler_line1","amsler_line2","amsler_line3","amsler_line4","amsler_line5","amsler_line6","amsler_line7","amsler_line8"]
        let test6InstructionsTextDesign = [true,false,false,false,false,false,false,false,false,false]
        
        let test7InstrtuctionsArray = ["equipment_and_procedure","fourty_cm_instruction","dc_line1","redesa_line2","dc_line3","dc_line4","dc_line5","dc_line6","dc_line7","dc_line8","dc_line9","dc_line10","dc_line11","dc_line12"]
        let test7InstructionsTextDesign = [true,false,false,false,false,false,false,false,false,false,false,false,false,false]
        
        let test8InstrtuctionsArray = ["equipment_and_procedure","fourty_cm_instruction","contrast_line1"]
        let test8InstructionsTextDesign = [true,false,false]
        
        let test9InstrtuctionsArray = ["equipment_and_procedure","fourty_cm_instruction","acc_line1","acc_line2","acc_line3","acc_line4"]
        let test9InstructionsTextDesign = [true,false,false,false,false,false]
        
        let test10InstrtuctionsArray = ["equipment_and_procedure","fourty_cm_instruction","anise_line1","anise_line2","anise_line3","anise_line4"]
        let test10InstructionsTextDesign = [true,false,false,false,false,false]
        
        let test11InstrtuctionsArray = ["equipment_and_procedure","fourty_cm_instruction","device_sidewise","mem_line1","mem_line2","mem_line3","mem_line4"]
        let test11InstructionsTextDesign = [true,false,false,false,false,false,false]
        
        let test12InstrtuctionsArray = ["equipment_and_procedure","fourty_cm_instruction","device_sidewise","redesa_line1","redesa_line2","redesa_line3","redesa_line4","redesa_line5","redesa_line6"]
        let test12InstructionsTextDesign = [true,false,false,false,false,false,false,false,false]
        
        let test13InstrtuctionsArray = ["equipment_and_procedure","device_sidewise","hirs_line1","hirs_line2","hirs_line3"]
        let test13InstructionsTextDesign = [true,false,false,false,false]
        
        let test14InstrtuctionsArray = ["equipment_and_procedure","fourty_cm_instruction","vaplus_line1","vaplus_line2","vaplus_line3"]
        let test14InstructionsTextDesign = [true,false,false,false,false]
        
        
        
        // Add all instructions array to global array for picking up when user click on spscific test
        testInstructionsText = [test0InstrtuctionsArray,test1InstrtuctionsArray ,test2InstrtuctionsArray, test3InstrtuctionsArray, test4InstrtuctionsArray, test5InstrtuctionsArray, test6InstrtuctionsArray, test7InstrtuctionsArray, test8InstrtuctionsArray, test9InstrtuctionsArray, test10InstrtuctionsArray, test11InstrtuctionsArray, test12InstrtuctionsArray, test13InstrtuctionsArray, test14InstrtuctionsArray]
        
        testInstructionsDesign = [test0InstructionsTextDesign,test1InstructionsTextDesign, test2InstructionsTextDesign, test3InstructionsTextDesign, test4InstructionsTextDesign,test5InstructionsTextDesign,test6InstructionsTextDesign,test7InstructionsTextDesign, test8InstructionsTextDesign, test9InstructionsTextDesign, test10InstructionsTextDesign, test11InstructionsTextDesign, test12InstructionsTextDesign, test13InstructionsTextDesign, test14InstructionsTextDesign]
        
    }
    
    @IBAction func ExitAdView(sender: AnyObject){
        self.dismiss(animated: true, completion: nil)
    }


    @IBAction func OpenOnWebsite(_ sender: Any) {
        if let url = URL(string: link) {
            UIApplication.shared.open(url, options: [:])
        }
    }
    

}
