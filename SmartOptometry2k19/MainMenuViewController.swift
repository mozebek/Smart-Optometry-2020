//
//  MainMenuViewController.swift
//  SmartOptometry2k19
//
//  Created by Matic Ozebek on 15/05/2019.
//  Copyright © 2019 Matic Ozebek. All rights reserved.
//

import UIKit
import Firebase
import StoreKit
import GoogleMobileAds

protocol HashTagPickerDelegate: class {
    func TriggerAds()
}

class MainMenuViewController: UIViewController, GADInterstitialDelegate {
   
    

    @IBOutlet weak var DashboardView: UIView!
    @IBOutlet weak var TestsList: UIView!
    @IBOutlet weak var Underline: UIView!
    @IBOutlet weak var TestsButton: UIButton!
    @IBOutlet weak var CalculatorsButton: UIButton!
    @IBOutlet weak var CalculatorsList: UIView!
    @IBOutlet weak var HelloThereLabel: UILabel!
    
    @IBOutlet weak var LeadingConstraintUnderline: NSLayoutConstraint!
    @IBOutlet weak var CenterConstraintCalcList: NSLayoutConstraint!
    @IBOutlet weak var CenterConstraintTestsList: NSLayoutConstraint!
    @IBOutlet weak var TestListBottom: NSLayoutConstraint!
    
    @IBOutlet weak var bannerView :BannerView!
    var bannerViewGads: GADBannerView!
    var interstitialGads: GADInterstitial!
    var SOAdsAvailable = false;
    
    var countryHasInterstitials = false;
    var ref: DatabaseReference!
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Google Ads Interstitial create
        interstitialGads = createAndLoadInterstitial()
        
        UITabBar.appearance().barTintColor = UIColor.white // your color
        UITabBar.appearance().backgroundColor = UIColor.white
        
        // Get users data from Firebase Database
        ref = Database.database().reference()

        // Set up main view on start
        SetUpOnStart()
        bannerView.LoadBanner();
        CheckForSOBanners()
        
        
        // Set titles
        self.tabBarController?.tabBar.items?[2].title = "settings".localized
        self.tabBarController?.tabBar.items?[0].title = "news".localized
        
        // translates
        HelloThereLabel.text = "hello_there".localized
        TestsButton.setTitle("tests".localized, for: .normal)
        CalculatorsButton.setTitle("calculators".localized, for: .normal)
        
        NotificationCenter.default.addObserver(self, selector: #selector(CheckCounters(_:)), name: Notification.Name(rawValue: "MainMenuDefaultCheck"), object: nil)
        
        self.view.layoutIfNeeded()
        
        
      
        
    }
    
    override func viewDidLayoutSubviews() {
       // TestListBottom.constant = 10
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        // translates
        HelloThereLabel.text = "hello_there".localized
        TestsButton.setTitle("tests".localized, for: .normal)
        CalculatorsButton.setTitle("calculators".localized, for: .normal)

       // TriggerAds()
        
    }
    
    // Check if ad or review controller needs to be showed after test closes
    @objc func CheckCounters(_ notification: Notification){
        // Check for review after 5 tests
        print("Preverjam review counter")
        let adsCounter = UserDefaults.standard.integer(forKey: "interstitialCounter")
        let rwCounter = UserDefaults.standard.integer(forKey: "ReviewsCounter")
        if(rwCounter % 10 == 0 && adsCounter % 6 != 0){
            SKStoreReviewController.requestReview()
        }
        
        TriggerAds()
    }

    func CheckForSOBanners(){
        let countryCode = Locale.current.regionCode
        ref.child("banners").child("countries2").child(countryCode!.lowercased()).observe(DataEventType.value, with: { snapshot in
            if(!snapshot.hasChildren()){
                self.AddGoogleBanner()
                self.SOAdsAvailable = false
            }
            else{
                self.SOAdsAvailable = true
            }
        })
    }
    
    // Create Google Ads interstitial
    func createAndLoadInterstitial() -> GADInterstitial {
      let interstitial = GADInterstitial(adUnitID: "ca-app-pub-3940256099942544/4411468910")
      interstitial.delegate = self
      interstitial.load(GADRequest())
      return interstitial
    }

    // Reloade Google Ads Interstitial after use
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
      interstitialGads = createAndLoadInterstitial()
    }
    
    func AddGoogleBanner(){
        bannerViewGads = GADBannerView(adSize: kGADAdSizeBanner)
        GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers =
        [ "dd824005b35c6f4a0ff620d09b243a5f" ] // Sample device ID
        bannerViewGads.adUnitID = "ca-app-pub-5440359851821242/8708323413/6499/example/banner"
        bannerViewGads.rootViewController = self
        bannerViewGads.load(GADRequest())
        bannerViewGads.backgroundColor = UIColor.black
        bannerViewGads.frame = CGRect.init(x: 0, y: 0, width: bannerView.frame.width, height: bannerView.frame.height)
        self.bannerView.addSubview(bannerViewGads)
    }
    
    public func TriggerAds(){
        
        // Check for ads
        let countryCode = Locale.current.regionCode?.lowercased()
        
        ref.child("banners").child("interstitials").observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.hasChild(countryCode!){
                self.countryHasInterstitials = true
                self.TryShowingAd()
            }
            else{
                self.TryShowingAd()}
        })
    }
    

    
    
    func SetUpOnStart(){
        
        //gradient layer
        let gradientLayer = CAGradientLayer()
        //define colors
        gradientLayer.colors = [UIColor.init(displayP3Red: 25/255, green: 154/255, blue: 143/255, alpha: 1).cgColor,UIColor.init(displayP3Red: 35/255, green: 188/255, blue: 121/255, alpha: 1).cgColor]
        //define locations of colors as NSNumbers in range from 0.0 to 1.0
        gradientLayer.locations = [0,1]
        gradientLayer.startPoint = CGPoint(x: 0, y: 1)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0)
        gradientLayer.contentsScale = 150;
        //define frame
        gradientLayer.frame = view.bounds
        //insert the gradient layer to the view layer
        DashboardView.layer.insertSublayer(gradientLayer, at: 0)
        
        // Set tests view controller rounded and apply shadow
        TestsList.layer.cornerRadius = 5
        TestsList.layer.shadowColor = UIColor.darkGray.cgColor
        TestsList.layer.shadowOffset = CGSize(width: 5.0, height: 5.0)
        TestsList.layer.shadowRadius = 25.0
        TestsList.layer.shadowOpacity = 0.2
        TestsList.backgroundColor = UIColor.white;
        
        // Set calculators view controller rounded and apply shadow. Move  it to right side of the screen presented
        CalculatorsList.layer.cornerRadius = 5
        CalculatorsList.layer.shadowColor = UIColor.darkGray.cgColor
        CalculatorsList.layer.shadowOffset = CGSize(width: 5.0, height: 5.0)
        CalculatorsList.layer.shadowRadius = 25.0
        CalculatorsList.layer.shadowOpacity = 0.2
        self.CenterConstraintCalcList.constant = self.view.frame.size.width
        self.view.layoutIfNeeded()
    }

    // --------------> Button Actions
    
    // Select calculators view
    @IBAction func SelectCalculators(_ sender: Any) {
        CalculatorsButton.titleLabel?.font = CalculatorsButton.titleLabel?.font.bold()
        TestsButton.titleLabel?.font = TestsButton.titleLabel?.font.removeBold()
        self.LeadingConstraintUnderline.constant = self.view.frame.size.width/2
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
        
        self.CenterConstraintCalcList.constant = 0
        self.CenterConstraintTestsList.constant = -self.view.frame.size.width
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }

    // Select tests view
    @IBAction func SetTests(_ sender: Any) {
        TestsButton.titleLabel?.font = TestsButton.titleLabel?.font.bold()
        CalculatorsButton.titleLabel?.font = CalculatorsButton.titleLabel?.font.removeBold()
        self.LeadingConstraintUnderline.constant = 0
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
        
        self.CenterConstraintCalcList.constant = self.view.frame.size.width
        self.CenterConstraintTestsList.constant = 0
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }

    func TryShowingAd(){
        
        // Present interstital every N times
        let adsCounter = UserDefaults.standard.integer(forKey: "interstitialCounter")
        print("number ads", adsCounter)
        if(adsCounter % 6 == 0 && SOAdsAvailable){
            if(UserDefaults.standard.bool(forKey: "adsRemoved") == false){
                
                if self.countryHasInterstitials{
                    let vc = storyboard!.instantiateViewController(withIdentifier: "SoInterstitital") as! InterstitialAdViewController
                    vc.view.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.7)
                    vc.modalPresentationStyle = .overFullScreen
                    vc.modalTransitionStyle = .crossDissolve
                    self.present(vc, animated: true, completion: nil)
                }
            }
        }
        
        // Google ads if no SO ads
        if(adsCounter % 3 == 0 && !SOAdsAvailable){
            if(UserDefaults.standard.bool(forKey: "adsRemoved") == false){
                print("ŠOOOOV INTERWSTIŠL")
                if interstitialGads.isReady {
                  interstitialGads.present(fromRootViewController: self)
                }
                else {
                print("Ad wasn't ready")
                }
            }
        }
    }

    // Check if user defaults value exists
    func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    
}

extension UIImageView {
    override open func awakeFromNib() {
        super.awakeFromNib()
        tintColorDidChange()
    }
}



