//
//  TermsViewController.swift
//  SmartOptometry2k19
//
//  Created by Matic Ozebek on 07/11/2019.
//  Copyright © 2019 Matic Ozebek. All rights reserved.
//

import UIKit

class TermsViewController: UIViewController {

    @IBOutlet weak var AgreeButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set orientation to portrait
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        
        Helper.app.addGradientToView(view: AgreeButton,
                                     color1: UIColor.init(displayP3Red: 25/255, green: 154/255, blue: 143/255, alpha: 1),
                                     color2: UIColor.init(displayP3Red: 35/255, green: 188/255, blue: 121/255, alpha: 1))
        AgreeButton.layer.cornerRadius = 5
        AgreeButton.clipsToBounds = true
    }
    
    override func viewWillLayoutSubviews() {
        Helper.app.addGradientToView(view: AgreeButton,
                                     color1: UIColor.init(displayP3Red: 25/255, green: 154/255, blue: 143/255, alpha: 1),
                                     color2: UIColor.init(displayP3Red: 35/255, green: 188/255, blue: 121/255, alpha: 1))
        AgreeButton.layer.cornerRadius = 5
        AgreeButton.clipsToBounds = true
    }
    
    @IBAction func AgreePressed(_ sender: Any) {
        UserDefaults.standard.set(true, forKey: "Terms")
        NotificationCenter.default.post(name: Notification.Name(rawValue: "showProfileUpdate"), object: nil)
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func DontAgreePressed(_ sender: Any) {
    }
    
    // Allow rotation
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
}
