//
//  ParentTabBarViewController.swift
//  SmartOptometry2k19
//
//  Created by Matic Ozebek on 18/05/2019.
//  Copyright © 2019 Matic Ozebek. All rights reserved.
//

import UIKit

class ParentTabBarViewController: UITabBarController {

    var buttonShadow:UIView = UIView()
    var menuButton:UIButton = UIButton()
    var bottomPadding: CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Add button to center of a tab and set default tab
        
        self.selectedIndex = 1;
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute: {
            self.CheckForProfileUpdate()
        })
        
        setupMiddleButton()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        
        
        // Check if terms aggreed
        var terms = isKeyPresentInUserDefaults(key: "Terms")
        if(terms){
            terms = UserDefaults.standard.bool(forKey: "Terms")
        }
        if(!terms){
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "TermsVC") as! TermsViewController
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    // Show profile update after terms closed
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(profileUpdateAfterTerms(_:)), name: Notification.Name(rawValue: "showProfileUpdate"), object: nil)

    }
    
    @objc func profileUpdateAfterTerms(_ notification: Notification) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
            self.CheckForProfileUpdate()
        })
    }
    
    // Cheking if user left email address
    func CheckForProfileUpdate(){
        // Check if email was added
        var profile = isKeyPresentInUserDefaults(key: "Subscribed")
        if(profile){
            profile = UserDefaults.standard.bool(forKey: "Subscribed")
        }
        if(!profile){
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileViewController
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    override func viewWillLayoutSubviews() {
        
        if #available(iOS 11.0, *) {
            bottomPadding = view.safeAreaInsets.bottom
        } else {
            bottomPadding = bottomLayoutGuide.length
        }
        
        if(bottomPadding == 0){
            var frame = buttonShadow.frame
            frame.origin.y = self.view.bounds.height - frame.height - 20
            buttonShadow.frame = frame
            menuButton.frame = frame
        }
        else{
            var frame = buttonShadow.frame
            frame.origin.y = self.view.bounds.height - frame.height - bottomPadding
            buttonShadow.frame = frame
            menuButton.frame = frame
        }
        print(bottomPadding)
    }
    
    

    // Add middle button to become highlited in center of tab bar
    func setupMiddleButton() {
        
        
        print(bottomPadding)
        
        // First set button shadow under
        buttonShadow = UIView(frame: CGRect(x: 0, y: 0, width: 64, height: 64))
        var frame = buttonShadow.frame
        frame.origin.y = self.view.bounds.height - frame.height - bottomPadding
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad ){
            frame.origin.y = self.view.bounds.height - frame.height*1.3
        }
        frame.origin.x = self.view.bounds.width/2 - frame.size.width/2
        buttonShadow.frame = frame
        buttonShadow.layer.cornerRadius = frame.height/2
        buttonShadow.layer.shadowColor = UIColor.darkGray.cgColor
        buttonShadow.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        buttonShadow.layer.shadowRadius = 10.0
        buttonShadow.layer.shadowOpacity = 0.6
        buttonShadow.backgroundColor = UIColor.white
        
        self.view.addSubview(buttonShadow)
        
        // Set up button
        menuButton = UIButton(frame: CGRect(x: 0, y: 0, width: 64, height: 64))
        var menuButtonFrame = menuButton.frame
        menuButtonFrame.origin.y = self.view.bounds.height - menuButtonFrame.height - bottomPadding
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad ){
            menuButtonFrame.origin.y = self.view.bounds.height - frame.height*1.3
        }
        menuButtonFrame.origin.x = self.view.bounds.width/2 - menuButtonFrame.size.width/2
        menuButton.frame = menuButtonFrame
        menuButton.layer.cornerRadius = menuButtonFrame.height/2
        
        // Add action on click
        menuButton.addTarget(self, action: #selector(ParentTabBarViewController.menuButtonAction(sender:)), for: .touchUpInside)
        
        // Set gradient bg
        Helper.app.addGradientToView(view: menuButton,
                                     color1: UIColor.init(displayP3Red: 25/255, green: 154/255, blue: 143/255, alpha: 1),
                                     color2: UIColor.init(displayP3Red: 35/255, green: 188/255, blue: 121/255, alpha: 1));
        menuButton.clipsToBounds = true;
        
        // Set button icon
        menuButton.setImage(UIImage(named: "Home"), for: UIControl.State.normal)
        menuButton.bringSubviewToFront(menuButton.imageView!)
        menuButton.imageView?.tintColor = UIColor.white;
        menuButton.imageEdgeInsets = UIEdgeInsets(top: 18 , left: 18, bottom: 18, right: 18);
        
        
        menuButton.addShadow(offset: CGSize.init(width: 0, height: 3), color: UIColor.black, radius: 2, opacity: 0.35)

        self.view.addSubview(menuButton)
        self.view.layoutIfNeeded()
    }
    
    
    @objc func menuButtonAction(sender: UIButton) {
        self.selectedIndex = 1
    }
    
    // Allow just portraint orientation inside main menu
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    // Check if user defaults value exists
    func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }

}



