//
//  BannerView.swift
//  
//
//  Created by Matic Ozebek on 17/11/16.
//
//

import UIKit
import Firebase
import Kingfisher


class BannerView: UIView {

    var ref: DatabaseReference! // database reference
    var bannersArray: [BannerObject] = [] // array of available banners
    var bannersTimer = Timer()
    var currentBannerNumber: Int = 0
    var bannerImageView = UIImageView()
    var bannerDefaultExist: Bool = false
    var imageLoader = UIImageView()
    var bannerViewGads: GADBannerView!
    
    public func GetBannerInfo() -> Bool{
        
        var bannerExist = false
        let countryCode = Locale.current.regionCode
        ref = Database.database().reference()
        ref.child("banners").child("countries2").child(countryCode!.lowercased()).observe(DataEventType.value, with: { snapshot in
            if(!snapshot.hasChildren()){
                bannerExist = false
            }
            else{
                bannerExist = true
            }
        })
        return bannerExist
    }
    
    public func LoadBanner(){
        // Remove previous views if exist
        self.lf_removeAllSubviews()
        // Create banner image view
        bannerImageView  = UIImageView(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height))
        bannerImageView.tag = 1
        self.addSubview(bannerImageView)
        
        // Get user country code
        let countryCode = Locale.current.regionCode
        print(countryCode!)
        
        // Go directly into country data
        ref = Database.database().reference()
        ref.child("banners").child("countries2").child(countryCode!.lowercased()).observe(DataEventType.value, with: { snapshot in
            
            // Empty old banners array
            self.bannersArray.removeAll()
            
            // Get all banners URLs to images and cliks
            let enumerator = snapshot.children
            while let rest = enumerator.nextObject() as? DataSnapshot {
                
                // New instance of class banner to fil it with data of banner loaded
                let currentBannerObject = BannerObject()
                
                //
                print(snapshot.childrenCount)
                
                // Get all banners for current country
                let value = rest.value as? NSDictionary
                let imageUrl = value?["imageName"] as! String
                let clickUrl = value?["clickUrl"] as! String
                
                // If snapshot has occupation value
                if value?["occupation"] != nil{
                    
                    //Store occupation data co current banner
                    let occupation = value?["occupation"] as! String
                    currentBannerObject.bannerOccupation = occupation // banner occupation information
                }
                // if snapshot has default value
                if value?["default"] != nil{
                    
                    // Store defaault data to current banner
                    let defBValue = value?["default"] as! Bool
                    currentBannerObject.bannerDefault = defBValue // banner default information
                    
                    // Add bool info that default banner exist online
                    if(defBValue == true){ self.bannerDefaultExist = true }
                    
                }
                
                // Add new banner object with data to array of banners
                currentBannerObject.bannerImageURL = imageUrl // banner image url
                currentBannerObject.bannerLinkURL = clickUrl // banner url
                currentBannerObject.bannerID = rest.key  // banner key
                
                print("BANNER OCCUPATION   " + currentBannerObject.bannerOccupation)
                print("BANNER DEFAULT   ", currentBannerObject.bannerDefault)
                
                // Add current banner to array
                self.bannersArray.append(currentBannerObject)
                self.runBannersLoop()
            }
        })
    }
    
    // Run banners timer loop
    func runBannersLoop (){
        // Check if array is not empty and start with banners rotation
        if(bannersArray.isEmpty == false && UserDefaults.standard.bool(forKey: "adsRemoved") == false){
            
            // Run banners to start looping
            print("RUNNING BANNERS")
            self.bannersTimer.invalidate()
            self.bannersTimer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(self.startCirculation), userInfo: nil, repeats: true)
        }

    }

    // Method that runs banners to repeat in circle
    @objc func startCirculation(){

        // Load new banner to image view
        self.loadBanners(bannerNumber: currentBannerNumber)
        
        // Move to next banner in array
        if(currentBannerNumber < bannersArray.count){
            currentBannerNumber = currentBannerNumber + 1
        }
        
        // If conter is at end of array move it to start position
        if(currentBannerNumber >= bannersArray.count) {currentBannerNumber = 0}
        
    }
    

    // Load single banner to image view and show it
    func loadBanners(bannerNumber: Int){
        
        // Get screen width
        let screenBounds = UIScreen.main.bounds
        let width: Int = Int(screenBounds.width)*2
        
        // If there is no online default banner as last present app default banner
        if(bannerNumber == bannersArray.count-1 && bannerDefaultExist == false){
            bannerImageView.image = UIImage(named: "defaultBanner")
        }
        // Else present banner by current number
        else{
            // Change banner image view image
            let fullImageUrl = URL(string: "https://storage.googleapis.com/so-banner/" + (String(describing: width)) + "/" + (bannersArray[bannerNumber].bannerImageURL))
            bannerImageView.kf.setImage(with: fullImageUrl,  options: [.transition(ImageTransition.fade(1))])
        }

        // Set banner link number
        bannerLinkNumber = bannerNumber
        
    }
    
    // Method that stops banners circulation if user moves to next view or sth...
    func stopBannersLoop(){
        // Run banners to start looping
        self.bannersTimer.invalidate()
    }
    
    var bannerLinkNumber: Int = 0
    
    // Open banner link connection in Safari
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        // If user touches bammer
        if touches.first != nil {
            if(bannersArray.isEmpty == false){
                UIApplication.shared.open(NSURL(string: bannersArray[bannerLinkNumber].bannerLinkURL)! as URL, options: [:], completionHandler: nil)
            }
            
        }
        super.touchesBegan(touches, with: event)

    }
    
    func AdsAreRemoved(){
        stopBannersLoop()
        bannerImageView.image = UIImage(named: "defaultBanner")
    }
    

}

extension UIView {
    func lf_removeAllSubviews() {
        for view in self.subviews {
            if(view.tag == 1){
                view.removeFromSuperview()
            }
        }
    }
}
