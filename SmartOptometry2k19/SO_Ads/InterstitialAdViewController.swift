//
//  InterstitialAdViewController.swift
//  SmartOptometry2k19
//
//  Created by Matic Ozebek on 28/10/2019.
//  Copyright © 2019 Matic Ozebek. All rights reserved.
//

import UIKit

class InterstitialAdViewController: UIViewController {

    @IBOutlet weak var AdView: SOInterstitialView!
    @IBOutlet weak var ExitButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Set rounded corners
        AdView.layer.cornerRadius = 10
        AdView.clipsToBounds = true
        AdView.layer.borderWidth = 1;
        AdView.layer.borderColor = UIColor.white.cgColor;
        
        ExitButton.layer.borderWidth = 2;
        ExitButton.layer.borderColor = UIColor.white.cgColor;
        ExitButton.layer.cornerRadius = (ExitButton.frame.size.height) / 2;
        ExitButton.clipsToBounds = true
        ExitButton.layer.masksToBounds = true;
        
    }
    
    override func viewWillLayoutSubviews() {
        ExitButton.layer.borderWidth = 2;
        ExitButton.layer.borderColor = UIColor.white.cgColor;
        ExitButton.layer.cornerRadius = (ExitButton.frame.size.height) / 2;
        ExitButton.clipsToBounds = true
        ExitButton.layer.masksToBounds = true;
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
    }

    @IBAction func ExitAdView(sender: AnyObject){
        
        //delegate.DismissDarkOverlay();
        self.dismiss(animated: true, completion: nil)
    }

}
