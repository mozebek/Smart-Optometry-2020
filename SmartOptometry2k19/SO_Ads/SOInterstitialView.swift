//
//  SOInterstitialView.swift
//  SmartOptometry
//
//  Created by Matic Ozebek on 16/01/2019.
//  Copyright © 2019 Google. All rights reserved.
//

import UIKit
import Firebase
import Kingfisher

class SOInterstitialView: UIView {
    
    // Trenutno vzema samo previ interstitial iz arraya.. Popravi v primeru večih oglasov!
    
    var ref: DatabaseReference! // database reference
    var intersArray: [InterstitialObject] = [] // array of available interstitials
    var IntersImage: UIImageView!
    
    override func draw(_ rect: CGRect) {
    
        
        IntersImage  = UIImageView(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height))
        self.addSubview(IntersImage)
        
        ref = Database.database().reference()
        let countryCode = Locale.current.regionCode
        
         ref.child("banners").child("interstitials").child(countryCode!.lowercased()).observe(DataEventType.value, with: { snapshot in
            
            let enumerator = snapshot.children
            while let rest = enumerator.nextObject() as? DataSnapshot {
                
                // New instance of class banner to fil it with data of banner loaded
                let currentIntersObject = InterstitialObject()
                
                // Get all banners for current country
                let value = rest.value as? NSDictionary
                currentIntersObject.intImageName = value?["imageName"] as! String
                currentIntersObject.intLinkURL = value?["clickUrl"] as! String
                currentIntersObject.intID = rest.key
                
                self.intersArray.append(currentIntersObject)
                
                print(self.intersArray[0].intImageName)
                print(self.intersArray[0].intLinkURL)
                
                self.SetInterstitial();
            }

         })
        
    }
    
    func SetInterstitial(){
        if intersArray.isEmpty == false
        {
            let fullImageUrl = URL(string: "https://storage.googleapis.com/so-banner/1080/" + intersArray[0].intImageName)
            self.IntersImage.kf.setImage(with: fullImageUrl,  options: [.transition(ImageTransition.fade(0.1))])
        }

    }
    
    // If user clicks interstitial add
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        // If user touches bammer
        if touches.first != nil {
            if intersArray.isEmpty == false {
                UIApplication.shared.open(NSURL(string: intersArray[0].intLinkURL)! as URL, options: [:], completionHandler: nil)
            }
            
        }
        super.touchesBegan(touches, with: event)
        
    }
    

}
