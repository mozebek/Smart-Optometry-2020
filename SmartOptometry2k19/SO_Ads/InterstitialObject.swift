//
//  InterstitialObject.swift
//  SmartOptometry
//
//  Created by Matic Ozebek on 16/01/2019.
//  Copyright © 2019 Google. All rights reserved.
//

import UIKit

class InterstitialObject: NSObject {
    
    var intImageName: String = ""
    var intLinkURL: String = ""
    var intID: String = ""

}
