//
//  BannerObject.swift
//  Smart Optometry
//
//  Created by Matic Ozebek on 17/11/16.
//  Copyright © 2016 Matic Ozebek. All rights reserved.
//

import UIKit

class BannerObject: NSObject {

    var bannerImageURL: String = ""
    var bannerLinkURL: String = ""
    var bannerID: String = ""
    var bannerDefault: Bool = false
    var bannerOccupation: String = ""
}
