//
//  SettingsTableViewController.swift
//  Smart Optometry
//
//  Created by Matic Ozebek on 14/12/16.
//  Copyright © 2016 Matic Ozebek. All rights reserved.
//

import UIKit
import StoreKit

class SettingsTableViewController: UITableViewController {

    
    @IBOutlet weak var LanguageLabel: UILabel!
    @IBOutlet weak var SocialLabel: UILabel!
    @IBOutlet weak var AboutLabel: UILabel!
    @IBOutlet weak var ContactLabel: UILabel!
    @IBOutlet weak var RateLabel: UILabel!
    @IBOutlet weak var BrightnessLabel: UILabel!
    @IBOutlet weak var BrightSwitch: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        
        // Increase ads counter
        var adsCounter = UserDefaults.standard.integer(forKey: "interstitialCounter")
        adsCounter += 1
        UserDefaults.standard.set(adsCounter, forKey: "interstitialCounter")
        
        _ = Locale.current.regionCode?.lowercased()

        // Set brightnes switch on or off depends on user defaults
        let autoBrightnessOn = UserDefaults.standard.bool(forKey: "AutoBrightnessOn")
        if(autoBrightnessOn ==  false)
        {
           BrightSwitch.setOn(false, animated: false)
        }
        
        translate()
        
        tableView.reloadData();

    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if(indexPath.row == 3 && indexPath.section == 1){
            SKStoreReviewController.requestReview()
        }
        
    }
    
    func translate(){
        LanguageLabel.text = "language_first_upper".localized
        SocialLabel.text = "social_first_upper".localized
        AboutLabel.text = "about_app".localized
        ContactLabel.text = "contact_us_first_upper".localized
        RateLabel.text = "rate_our_app".localized
        BrightnessLabel.text = "auto_brightness".localized
        self.title = "settings".localized
        self.tabBarController?.tabBar.items?[2].title = "settings".localized
        self.tabBarController?.tabBar.items?[0].title = "news".localized
    
    }
    

    @IBAction func SwitchValueChange(sender: UISwitch) {
        if sender.isOn {
            UserDefaults.standard.setValue(true, forKey: "AutoBrightnessOn")
        } else {
            UserDefaults.standard.setValue(false, forKey: "AutoBrightnessOn")
        }
    }
    
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        switch section
        {
        case 0:
            return "user".localized.uppercased()
        case 1:
            return "other".localized.uppercased()
        default:
            return ""
        }
    }
}
