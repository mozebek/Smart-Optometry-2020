//
//  AboutAppViewController.swift
//  SmartOptometry2k19
//
//  Created by Matic Ozebek on 10/10/2019.
//  Copyright © 2019 Matic Ozebek. All rights reserved.
//

import UIKit

class AboutAppViewController: UIViewController {

    @IBOutlet weak var AppVersionLabel: UILabel!
    @IBOutlet weak var AppVersionHeading: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        AppVersionHeading.text = "app_version".localized
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Set small title
        self.navigationController?.navigationBar.prefersLargeTitles = false
        
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        AppVersionLabel.text = appVersion
    }
    

    // Set large title again
    override func willMove(toParent parent: UIViewController?) {
        navigationItem.largeTitleDisplayMode = .always
        navigationController?.navigationBar.prefersLargeTitles = true
    }

}
