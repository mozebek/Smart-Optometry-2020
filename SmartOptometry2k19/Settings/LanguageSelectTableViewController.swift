//
//  LanguageSelectTableViewController.swift
//  SmartOptometry2k19
//
//  Created by Matic Ozebek on 10/10/2019.
//  Copyright © 2019 Matic Ozebek. All rights reserved.
//

import UIKit

class LanguageSelectTableViewController: UITableViewController {

    var items: [String] = ["English", "Spanish", "German", "French", "Italian", "Polish", "Norwegian", "Croatian", "Slovenian", "Russian", "Czech"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        tableView.delegate = self
        tableView.dataSource = self
        
        self.navigationController?.navigationBar.prefersLargeTitles = false
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count;
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)

        cell.textLabel?.text = self.items[indexPath.row]
        cell.selectionStyle = .none


        return cell
    }
    
    
   
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
        
        
        if(indexPath.row == 0){
            print("English")
            UserDefaults.standard.set("en", forKey: "i18n_language")
            UserDefaults.standard.synchronize()
        }
        
        if(indexPath.row == 1){
            print("Spanish")
            UserDefaults.standard.set("es", forKey: "i18n_language")
            UserDefaults.standard.synchronize()
        }
        
        if(indexPath.row == 2){
            print("German")
            UserDefaults.standard.set("de", forKey: "i18n_language")
            UserDefaults.standard.synchronize()
        }
        
        if(indexPath.row == 3){
            print("France")
            UserDefaults.standard.set("fr", forKey: "i18n_language")
            UserDefaults.standard.synchronize()
        }
        
        if(indexPath.row == 4){
            print("Italian")
            UserDefaults.standard.set("it", forKey: "i18n_language")
            UserDefaults.standard.synchronize()
        }
        
        if(indexPath.row == 5){
            print("Polish")
            UserDefaults.standard.set("pl", forKey: "i18n_language")
            UserDefaults.standard.synchronize()
        }
        
        if(indexPath.row == 6){
            print("Norwegian")
            UserDefaults.standard.set("nb", forKey: "i18n_language")
            UserDefaults.standard.synchronize()
        }
        
        if(indexPath.row == 7){
            print("Croatian")
            UserDefaults.standard.set("hr", forKey: "i18n_language")
            UserDefaults.standard.synchronize()
        }
        
        if(indexPath.row == 8){
            print("Slovenian")
            UserDefaults.standard.set("sl-SI", forKey: "i18n_language")
            UserDefaults.standard.synchronize()
        }
        
        if(indexPath.row == 9){
            print("Russian")
            UserDefaults.standard.set("ru", forKey: "i18n_language")
            UserDefaults.standard.synchronize()
        }
        
        if(indexPath.row == 10){
            print("Czech")
            UserDefaults.standard.set("cs", forKey: "i18n_language")
            UserDefaults.standard.synchronize()
        }
        

    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.accessoryType = .none
    }
    
    // Set large title again
    override func willMove(toParent parent: UIViewController?) {
        navigationItem.largeTitleDisplayMode = .always
        navigationController?.navigationBar.prefersLargeTitles = true
    }

}


extension String {
    var localized: String {
        if let _ = UserDefaults.standard.string(forKey: "i18n_language") {} else {
            // we set a default, just in case
            UserDefaults.standard.set("en", forKey: "i18n_language")
            UserDefaults.standard.synchronize()
        }
        
        let lang = UserDefaults.standard.string(forKey: "i18n_language")
        
        let path = Bundle.main.path(forResource: lang, ofType: "lproj")
        let bundle = Bundle(path: path!)
        
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    }
}
