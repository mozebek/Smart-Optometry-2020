//
//  ContactUsViewController.swift
//  SmartOptometry2k19
//
//  Created by Matic Ozebek on 10/10/2019.
//  Copyright © 2019 Matic Ozebek. All rights reserved.
//

import UIKit

class ContactUsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Set small title
        self.navigationController?.navigationBar.prefersLargeTitles = false
    }
    

    // Set large title again
    override func willMove(toParent parent: UIViewController?) {
        navigationItem.largeTitleDisplayMode = .always
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    @IBAction func ContactUsAction(_ sender: Any) {
        let appURL = URL(string: "mailto:info@smart-optometry.com")!
        UIApplication.shared.open(appURL as URL, options: [:], completionHandler: nil)
        


    }
    
}
