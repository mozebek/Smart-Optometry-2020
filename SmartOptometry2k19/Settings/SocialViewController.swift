//
//  SocialViewController.swift
//  SmartOptometry2k19
//
//  Created by Matic Ozebek on 10/10/2019.
//  Copyright © 2019 Matic Ozebek. All rights reserved.
//

import UIKit

class SocialViewController: UIViewController {

    @IBOutlet weak var FollowLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        FollowLabel.text = "follow_us".localized
        
        // Set small title
        self.navigationController?.navigationBar.prefersLargeTitles = false    }
    

    // Set large title again
    override func willMove(toParent parent: UIViewController?) {
        navigationItem.largeTitleDisplayMode = .always
        navigationController?.navigationBar.prefersLargeTitles = true
    }

    @IBAction func TwitterConnect(_ sender: Any) {
        if let url = URL(string: "https://twitter.com/smartoptometry") {
            UIApplication.shared.open(url, options: [:])
        }
    }
    @IBAction func InstagramConnect(_ sender: Any) {
        if let url = URL(string: "https://www.instagram.com/smartoptometry/") {
            UIApplication.shared.open(url, options: [:])
        }
    }
    @IBAction func FacebookConnect(_ sender: Any) {
        if let url = URL(string: "https://www.facebook.com/SmartOptometry/") {
            UIApplication.shared.open(url, options: [:])
        }
    }
    @IBAction func LinkedInConnect(_ sender: Any) {
        if let url = URL(string: "https://www.linkedin.com/company/10097815/") {
            UIApplication.shared.open(url, options: [:])
        }
    }
}
