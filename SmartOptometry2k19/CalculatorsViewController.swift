//
//  CalculatorsViewController.swift
//  SmartOptometry2k19
//
//  Created by Matic Ozebek on 08/10/2019.
//  Copyright © 2019 Matic Ozebek. All rights reserved.
//

import UIKit

class CalculatorsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var CalculatorList: UIView!
    @IBOutlet weak var CalculatorTableView: UITableView!
    @IBOutlet weak var SelectCalculatorLabel: UILabel!
    @IBOutlet weak var AllCalculatorsLabel: UILabel!
    
    var SOCalculators = [SOCalculator]();
    
    override func viewWillAppear(_ animated: Bool) {
        //Translates
        AllCalculatorsLabel.text = "all_calculators".localized
        SelectCalculatorLabel.text = "select_calculator".localized
        CalculatorTableView.reloadData()  
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Hide ampty cells
        CalculatorTableView.tableFooterView = UIView()
        
        CalculatorList.layer.cornerRadius = 5
        CalculatorList.layer.shadowColor = UIColor.darkGray.cgColor
        CalculatorList.layer.shadowOffset = CGSize(width: 5.0, height: 5.0)
        CalculatorList.layer.shadowRadius = 25.0
        CalculatorList.layer.shadowOpacity = 0.2
        CalculatorList.backgroundColor = UIColor.white;
        
        // Load calculators
        let calc1 = SOCalculator(calculatorName: "vertex_calculator",
                           calculatorColor:  UIColor.clear ,
                           calculatorImageName: "VertexCalculatorIcon",
                           calculatorVCName: "VertexCalculator")
        
        let calc2 = SOCalculator(calculatorName: "visual_acuity_calculator",
                           calculatorColor:  UIColor.clear ,
                           calculatorImageName: "VisualAcuityCalculatorIcon",
                           calculatorVCName: "VisualAcuityCalculator")
        
        SOCalculators += [calc1, calc2]
        
        CalculatorTableView.delegate = self
        CalculatorTableView.dataSource = self
        
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return SOCalculators.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Table view cells are reused and should be dequeued using a cell identifier.
        let cellIdentifier = "cell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? TestCellTableViewCell  else {
            fatalError("The dequeued cell is not an instance of MealTableViewCell.")
        }
        
        // Fetches the appropriate meal for the data source layout.
        let test = SOCalculators[indexPath.row]
        
        cell.TestImageHolder.backgroundColor = test.CalculatorColor
        cell.TestName.text = test.CalculatorName.localized
        let image = UIImage(named: test.CalculatorImageName)
        cell.TestImage.image = image;
        cell.InfoButton.setTitle("info".localized, for: .normal)
        cell.TypeLabel.text = "calculator".localized
        cell.selectionStyle = .none
        
        return cell
    }
    
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        // Open new test with transition manager class (slide left animation)
        let testViewController = storyboard!.instantiateViewController(withIdentifier: SOCalculators[indexPath.row].CalculatorVCName)
        testViewController.transitioningDelegate = self as? UIViewControllerTransitioningDelegate
        testViewController.modalPresentationStyle = .fullScreen
        present(testViewController, animated: false, completion: nil)
        
    }

}
