//
//  RSSREaderViewController.swift
//  SmartOptometry2k19
//
//  Created by Matic Ozebek on 03/12/2019.
//  Copyright © 2019 Matic Ozebek. All rights reserved.
//

import UIKit
import Alamofire
import SWXMLHash

class RSSREaderViewController: UIViewController {
var a: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        /*let feedURL = URL(string: "http://images.apple.com/main/rss/hotnews/hotnews.rss")!
        let parser = FeedParser(URL: feedURL)
        
        let result = parser.parse()*/
        
        /*let url = "https://www.aoa.org/news/news-rss-feed"
        let parameters: Parameters = ["foo": "bar"]
        let headers: HTTPHeaders = [
            "Authorization": "Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ==",
            "Accept": "application/json"
        ]
        
        
        
        AF.request(url, method: .get, encoding: URLEncoding.default , headers: headers)
            .responseString { response in
                print(" - API url: \(String(describing: response.request!))")   // original url request
                var statusCode = response.response?.statusCode
                
                switch response.result {
                case .success(let value):
  
                    print("status code is: \(String(describing: statusCode))")
                    //print("XML: \(value)")
                    
                    
                case .failure(let error):
                    statusCode = error._code // statusCode private
                    print("status code is: \(String(describing: statusCode))")
                    print(error)
                }
        }*/
        

   
        // Do any additional setup after loading the view.
        
        let url = "https://www.aoa.org/news/news-rss-feed"
        let parameters: Parameters = ["foo": "bar"]
        let headers: HTTPHeaders = [
            "Authorization": "Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ==",
            "Accept": "application/json"
        ]
        
        AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.default , headers: headers)
            .responseString { response in
                print(" - API url: \(String(describing: response.request!))")   // original url request
                var statusCode = response.response?.statusCode
                
                
                
                
                switch response.result {
                case .success(let value):
                    print("status code is: \(String(describing: statusCode))")
                    let xml = SWXMLHash.parse(value)
                        print("XML: \(xml["rss"]["channel"]["item"][1]["title"].element?.text)")
                    
                case .failure(let error):
                    statusCode = error._code // statusCode private
                    print("status code is: \(String(describing: statusCode))")
                    print(error)
                }
        }
    }
    

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
