/* 
  Localizable.strings
  SmartOptometry2k19

  Created by Matic Ozebek on 19/08/2019.
  Copyright © 2019 Matic Ozebek. All rights reserved.
*/

// General
"results" = "Results";

// Color vision
"score" = "Score";
"color_deficiency"= "Color deficiency";
"protanopia" = "protanopia";
"deuteranopia" = "deuteranopia";
"red_green" = "red-green";
"your_first_upper" = "Your";
"correct_answer_first_upper" = "Correct";
"no_colour_deficiency" = "No color deficiency detected";

// Schober
"center" = "Center";
"up" = "Up %@";
"down" = "Down %@";
"temporal" = "Temporal";
"nasal" = "Nasal";

// Worth Four Dot
"right_dominant" =  "Your dominant eye is right eye";
"left_dominant" = "Your dominant eye is left eye";
"no_dominant" = "Your don't have dominant eye";

// Visual Acuity statements
"statmentVA1" = "Ljubljana is the capital city of Slovenia.";
"statmentVA2" = "Fascinating Lake Bled with its island, where you can ring the bell at the church and make a wish.";
"statmentVA3" = "The unique Karst landscape stretches over approximately 500 square kilometres.";
"statmentVA4" = "The famous veduta of Piran, the medieval pearl, gives the memorable seaside view.";
"statmentVA5" = "The magical Kočevje virgin forest, preserved natural heritage.";
"statmentVA6" = "Numerous thermal-water springs reaching the surface at 32-73 degress celsius are one of the greatest Slovenian treasures.";
"statmentVA7" = "The oldest vine in the world, the 400-year-old vine in Maribor.";
"statmentVA8" = "The Avsenik brothers established a new folk music by combining piano accordion, trumpet, clarinet, bass and guitar.";
"statmentVA9" = "Slovenia has the tallest cliff on the Adriatic coast. On the Strunjan Peninsula, an almost vertical flysch wall rises up from the sea to a height of 80 metres.";

// Contrast
"contrast_value" = "Contrast value";

// Accomodation 100 random short words
"bread" = "bread";
"people" = "people";
"after" = "after";
"because" = "because";
"person" = "person";
"large" = "large";
"think" = "think";
"child" = "child";
"great" = "great";
"different" = "different";
"number" = "number";
"work" = "work";
"problem" = "problem";
"other" = "other";
"hand" = "hand";
"leave" = "leave";
"point" = "point";
"fact" = "fact";
"young" = "young";
"history" = "history";
"world" = "world";
"family" = "family";
"park" = "park";
"meat" = "meat";
"year" = "year";
"music" = "music";
"power" = "power";
"internet" = "internet";
"control" = "control";
"science" = "science";
"product" = "product";
"activity" = "activity";
"language" = "language";
"player" = "player";
"week" = "week";
"quality" = "quality";
"oven" = "oven";
"moon" = "moon";
"space" = "space";
"ability" = "ability";
"fire" = "fire";
"movie" = "movie";
"direction" = "direction";
"camera" = "camera";
"paper" = "paper";
"mouth" = "mouth";
"strategy" = "strategy";
"analysis" = "analysis";
"army" = "army";
"goal" = "goal";
"fishing" = "fishing";
"university" = "university";
"college" = "college";
"article" = "article";
"income" = "income";
"user" = "user";
"marriage" = "marriage";
"teacher" = "teacher";
"road" = "road";
"moment" = "moment";
"paint" = "paint";
"wood" = "wood";
"office" = "office";
"student" = "student";
"event" = "event";
"politics" = "politics";
"driver" = "driver";
"flight" = "flight";
"river" = "river";
"scene" = "scene";
"member" = "member";
"woman" = "woman";
"water" = "water";
"health" = "health";
"advice" = "advice";
"effort" = "effort";
"reality" = "reality";
"skill" = "skill";
"city" = "city";
"heart" = "heart";
"photo" = "photo";
"director" = "director";
"alcohol" = "alcohol";
"union" = "union";
"banana" = "banana";
"apple" = "apple";
"orange" = "orange";
"drama" = "drama";
"golf" = "golf";
"tennis" = "tennis";
"football" = "football";
"virus" = "virus";
"sample" = "sample";
"editor" = "editor";
"guitar" = "guitar";
"basket" = "basket";
"lesson" = "lesson";
"hair" = "hair";
"police" = "police";

//Amsler
"thickness" = "Thickness";
"color" = "Color";
"grid_color" = "Grid color";

// Accommodation
"acc_line1" = "Swipe to the left to start the test";
"first_thirty" = "Words in first 30 seconds";
"second_thirty" = "Words in second 30 seconds";


// Aniseikonia
"horizontal_meridian_upper_case" = "HORIZONTAL MERIDIAN";
"vertical_meridian_upper_case" = "VERTICAL MERIDIAN";
"both_eyes_same" = "Both eyes have same retinal image.";
"left_eye_has" = "Left eye has %@ bigger retinal image.";
"right_eye_has" = "Right eye has %@ bigger retinal image.";
"done" = "done";

// Red Desaturation
"lower" = "lower";
"upper" = "upper";
"temporal" = "temporal";
"nasal" = "nasal";

// Hirschberg
"no_deviation" = "Reflex is in the centre of the pupil (no deviation).";
"hypotropia" = "Slightly superior to the centre of the pupil (hypotropia).";
"hypertropia" = "Slightly anterior to the centre of the pupil (hypertropia).";
"exotropia" = "Slightly nasal to the centre of the pupil (exotropia).";
"esotropia" = "Slightly temporal to the centre of the pupil (esotropia).";
"hypo_and_eso" = "Slightly superior to the centre of the pupil (hypotropia) and slightly temporal to the centre of the pupil (esotropia).";
"hypo_and_exo" = "Slightly superior to the centre of the pupil (hypotropia) and slightly nasal to the centre of the pupil (exotropia).";
"hyper_and_exo" = "Slightly anterior to the centre of the pupil (hypertropia) and slightly nasal to the centre of the pupil (exotropia).";
"hyper_and_eso" = "Slightly anterior to the centre of the pupil (hypertropia) and slightly temporal to the centre of the pupil (esotropia).";
"right_eye" = "Right eye";
"left_eye" = "Left eye";

// VA Plus
"swipe_area" = "swipe area";
"va_value" = "Visual Acuity Value";
"select_optotype" = "SELECT OPTOTYPE";

// Vertex Calculator
"glasses_power" = "Glasses [D]";
"contacts_power" = "Contact lens [D]";
"swap" = "swap";
"vertex_distance" = "Vertex distance";

// VA Calculator
"va_unit_con" = "visual acuity unit converter";
"select_unit" = "Select unit";
"change" = "change";

// Settings
"social_first_upper" = "Social";
"about_us_first_upper" = "About us";
"contact_us_first_upper" = "Contact us";
"rate_our_app" = "Rate our app!";
"about_app" = "About app";
"auto_brightness" = "Auto Brightness";
"language_first_upper" = "Language";
"follow_us" = "Follow us";
"user" = "User";
"other" = "Other";
"app_version" = "App version";


// Instructions
"wfd_line2" = "State the colour of bottom circle";
"wfd_line3" = "Select the corresponding colour on the top";

"sch_line2" = "Move the red cross to the centre";

"okn_line1" = "Hold the test next to the patient\'s eyes";
"okn_line2" = "Click the screen for faster mode";
"okn_line3" = "Slide left or right to fixate dot";
"okn_line4" = "Slide up or down to change colour";
"okn_line5" = "Test horizontally then rotate the screen for 90 degrees and test vertical movements";
"okn_line6" = "Observe for any asymmetric OKN";

"cvision_line1" = "Type in the number you seen and click ✓";
"cvision_line2" = "Tap ✓ if you can\'t see the numbers and you want to continue to next table";
"cvision_line3" = "Tap ✘ if you entered wrong number and you want to correct your answer";

"flou_line1" = "A drop of saline solution is put on the fluorescein strip and colour the tear film";
"flou_line2" = "Shine the colour onto the corneal staining or for the RGP fitting pattern and analyse the surface";

"redesa_line1" = "Occlude one eye";
"redesa_line2" = "Focus on the central white dot";
"redesa_line3" = "Compare the brightness of the red circles";
"redesa_line4" = "Holding the circle will increase the intensity";
"redesa_line5" = "Pressing the circle again will decrease the intensity";
"redesa_line6" = "Press the central white dot when the colours are equally intense";

"hirs_line1" = "Shine the light into the patient\'s eyes from about 50 cm";
"hirs_line2" = "Instruct the patient to look at the light, and observe the corneal light reflex from the both eyes, looking directly from behind the pen";
"hirs_line3" = "Slide the white reflex on both eyes in the direction where the reflex is seen";

"acc_line1" = "Swipe to the left to start the test";
"acc_line2" = "You put flippers with plus lenses in front of patient\'s eyes and ask him to report the word written";
"acc_line3" = "After reading it, patient slides the screen to get another word on, meanwhile you flip the flipper to minus lenses";
"acc_line4" = "The test lasts 60 seconds";

"va_line1" = "By sliding up or down you can choose different symbols";
"va_line2" = "By sliding left or right you can change symbol sizes regarding visual acuity";

"dc_line3" = "Compare the two sides for its intensity";
"dc_line4" = "Add minus power lens for brighter red side";
"dc_line5" = "Add plus power if green side stands out more";
"dc_line6" = "Neutrality is reached when the letters on both backgrounds appear equally distinct";
"dc_line8" = "Put 6 prisms base down in front of the left eye to split images";
"dc_line9" = "Compare the two sides in each line for its intensity";
"dc_line10" = "OD - add minus power lens for brighter upper red side, add plus lens if green appears more intense";
"dc_line11" = "Do the same for bottom line with the left eye (OS)";
"dc_line12" = "Neutrality is reached when the letters on both backgrounds and both lines appear equally distinct";

"anise_line1" = "Compare red and green bracket sizes";
"anise_line2" = "Slide the point at the line horizontally in order to diminish or enlarge the bracket size";
"anise_line3" = "Click done when the brackets are aligned";
"anise_line4" = "Left or right side buttons are there to select horizontal or vertical position";

"amsler_line1" = "Focus on the central dot";
"amsler_line2" = "With the finger tip colour the parts that may appear wavy, broken, distorted or missing";
"amsler_line4" = "While looking to the centre dot, can you see the four corners?";
"amsler_line5" = "Are any of the lines missing?";
"amsler_line6" = "Do any have holes in them? If so, where?";
"amsler_line7" = "Are all the lines on the test straight? Are any of them wavy, if so, where?";
"amsler_line8" = "Are all the squares the same size? If some of them are bigger or smaller, which ones?";

"mem_line1" = "Orient the retinoscope beam vertically and hold it just behind the screen";
"mem_line2" = "Patient follows the animation on the screen";
"mem_line3" = "Quickly guide the streak across the patient\'s left and right eye and try to evaluate the reflex for “with or against motion” or neutrality";
"mem_line4" = "Estimate the dioptric value required to neutralise the observed motion";

"vaplus_line1" = "Choose a symbol that you want to use for test";
"vaplus_line2" = "Slide in the direction the symbol is facing";
"vaplus_line3" = "Application returns you the result on your visual acuity";

"contrast_line1" = "Say out loud the last letter you can distinguish and click on it";

"equipment_and_procedure" = "Equipment, set-up and the procedure:";
"dc_line1" = "Monocular:";
"dc_line7" = "Binocular:";
"amsler_line3" = "Questions:";
"device_sidewise" = "Device is held sidewise";
"perform_on_light_or_dark" = "Perform the test on either light or dark background";
"fourty_cm_instruction" = "Test is held at 40 cm";
"click_for_more_info" = "Click here to get more info";
"hold_device_near" = "Hold device near to your eye";
"red_and_green_glasses" = "Red and green filter glasses";


// Test Names
"worth_four_dot_upper_case" = "Worth Four Dot";
"schober_upper_case" = "Schober";
"colour_vision_upper_case" = "Color Vision";
"visual_acuity_upper_case" = "Visual Acuity";
"okn_stripes_upper_case" = "OKN Stripes";
"flourescin_light_upper_case" = "Fluorescein Light";
"amsler_grid_upper_case" = "Amsler Grid";
"aniseikonia_upper_case" = "Aniseikonia";
"mem_retinoscopy_upper_case" = "MEM Retinoscopy";
"red_desaturation_upper_case" = "Red Desaturation";
"hirschberg_upper_case" = "Hirschberg";
"visual_acuity_plus" = "Visual Acuity +";
"accommodation_upper_case" = "Accommodation";
"contrast_upper_case" = "Contrast";
"duochrome_upper_case" = "Duochrome";

//Calculators
"vertex_calculator" = "Vertex Conversion";
"visual_acuity_calculator" = "VA Conversion";

// Test info
"or_read_more_online" = "or read more online";
"close" = "Close";

// Main menu
"all_tests" = "All tests";
"select_test" = "Select Test";
"hello_there" = "Hello there!";
"select_calculator" = "Select calculator";
"all_calculators" = "All calculators";
"info" = "Info";
"test" = "Test";
"calculator" = "Calculator";
"tests" = "Tests";
"calculators" = "Calculators";
"settings" = "Settings";
"news" = "News";

// Profile
"profile_update" = "Update your profile";
"name" = "Name";
"email" = "E-main address";
"terms_agree" = "I agree to ToS, EULA and Privacy Policy";
"update" = "Update";
"error" = "Error!";
"check_connection" = "Check internet connection?";
"invalid_email" = "Invalid email address.";
"email_missing" = "Email address missing.";
"you_must_aggree" = "You must aggree with our terms.";


// Other
"coming_soon" = "Coming soon.";
